/* 

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Author:  Mohammed
 * Created: 25, 08, 2020
 */

ALTER TABLE `tbl_gallery`  ADD `file_extension` VARCHAR(250) NOT NULL  AFTER `is_active`;
ALTER TABLE `tbl_gallery` CHANGE `InsTerminal` `InsTerminal` VARCHAR(250) NULL DEFAULT NULL;
ALTER TABLE `tbl_gallery` CHANGE `InsDateTime` `InsDateTime` DATETIME NULL DEFAULT NULL;
ALTER TABLE `tbl_gallery` CHANGE `UpdTerminal` `UpdTerminal` VARCHAR(250) NULL DEFAULT NULL;
ALTER TABLE `tbl_gallery` CHANGE `UpdDateTime` `UpdDateTime` DATETIME NULL DEFAULT NULL;
ALTER TABLE `tbl_gallery` CHANGE `gallery_description` `gallery_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

CREATE TABLE `business_profile`.`tbl_map` ( `map_id` INT NOT NULL AUTO_INCREMENT , `ref_user_id` INT NOT NULL , `map` TEXT NOT NULL , `is_active` INT NOT NULL DEFAULT '1' , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` INT NOT NULL , PRIMARY KEY (`map_id`)) ENGINE = InnoDB;
ALTER TABLE `tbl_map` CHANGE `del_status` `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live';

CREATE TABLE `business_profile`.`tbl_user_contact_us` ( `contact_us_id` INT NOT NULL AUTO_INCREMENT , `ref_user_id` INT NOT NULL , `contact_us_name` VARCHAR(250) NOT NULL , `contact_us_email` TEXT NOT NULL , `contact_us_message` TEXT NOT NULL , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` VARCHAR(50) NOT NULL , PRIMARY KEY (`contact_us_id`)) ENGINE = InnoDB;


ALTER TABLE `tbl_user_profile` CHANGE `ref_theme_id` `theme_color` INT(11) NOT NULL;
ALTER TABLE `tbl_user_profile` CHANGE `theme_color` `theme_color` VARCHAR(20) NOT NULL;



/**
 * Author:  Mohammed
 * Created: 01, 09, 2020
 */

ALTER TABLE `tbl_user_profile` ADD `vcf_first_name` VARCHAR(250) NULL DEFAULT NULL AFTER `theme_color`, ADD `vcf_last_name` VARCHAR(250) NULL DEFAULT NULL AFTER `vcf_first_name`, ADD `vcf_date_of_birth` DATE NULL DEFAULT NULL AFTER `vcf_last_name`, ADD `vcf_email` TEXT NULL DEFAULT NULL AFTER `vcf_date_of_birth`, ADD `vcf_phone` VARCHAR(250) NULL DEFAULT NULL AFTER `vcf_email`, ADD `vcf_address` TEXT NULL DEFAULT NULL AFTER `vcf_phone`;
ALTER TABLE `tbl_user_profile`  ADD `vcf_country_id` INT NULL DEFAULT NULL  AFTER `vcf_email`;
ALTER TABLE `tbl_user_info`  ADD `is_created_short_link` INT NOT NULL DEFAULT '0'  AFTER `user_type`;
ALTER TABLE `tbl_user_info`  ADD `user_short_link` TEXT NULL DEFAULT NULL  AFTER `raw`;


/**
 * Author:  Mohammed
 * Created: 03, 09, 2020
 */

ALTER TABLE `tbl_user_client`  ADD `compressed_client_logo` TEXT NULL DEFAULT NULL  AFTER `client_logo`;
ALTER TABLE `tbl_user_profile`  ADD `profile_compressed_photo` TEXT NULL DEFAULT NULL  AFTER `profile_photo`;
ALTER TABLE `tbl_user_profile`  ADD `cover_compressed_photo` TEXT NULL DEFAULT NULL  AFTER `cover_photo`;



/**
 * Author:  Mohammed
 * Created: 04, 09, 2020
 */


CREATE TABLE `business_profile`.`tbl_menu_category` ( `category_id` INT NOT NULL AUTO_INCREMENT , `ref_user_id` INT NOT NULL , `category_name` VARCHAR(250) NOT NULL , `is_active` INT NOT NULL , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live' , PRIMARY KEY (`category_id`)) ENGINE = InnoDB;
CREATE TABLE `business_profile`.`tbl_menu_item` ( `item_id` INT NOT NULL AUTO_INCREMENT , `ref_user_id` INT NOT NULL , `ref_category_id` INT NOT NULL , `item_name` VARCHAR(300) NOT NULL , `item_description` TEXT NOT NULL , `item_price` DECIMAL(10,2) NOT NULL , `item_sale_price` DECIMAL(10,2) NULL DEFAULT NULL , `is_sale` INT NOT NULL DEFAULT '0' , `is_active` INT NOT NULL , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live' , PRIMARY KEY (`item_id`)) ENGINE = InnoDB;
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Menu', 'MENU', '#', NULL, 'fa-burger-soda', '3', 'customer', 'Live');
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Menu Category', 'MENU_CATEGORY', 'admin/customer/Menu', '22', NULL, '1', 'customer', 'Live');
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES (NULL, 'Menu Item', 'MENU_ITEM', 'admin/customer/Menu/item', '22', NULL, '2', 'customer', 'Live');
ALTER TABLE `tbl_menu_item` CHANGE `item_description` `item_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_plan` CHANGE `product_limit` `product_limit` INT(11) NULL DEFAULT '0';
ALTER TABLE `tbl_plan` CHANGE `address_limit` `address_limit` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `tbl_plan` CHANGE `service_limit` `service_limit` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `tbl_plan` CHANGE `payment_info_limit` `payment_info_limit` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `tbl_plan` CHANGE `item_limit` `item_limit` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `tbl_contact`  ADD `address_of` VARCHAR(250) NOT NULL  AFTER `ref_user_id`;
ALTER TABLE `tbl_contact`  ADD `map_link` TEXT NULL DEFAULT NULL  AFTER `contact_address`;



/**
 * Author:  Mohammed
 * Created: 07, 09, 2020
 */

ALTER TABLE `tbl_contact` CHANGE `contact_email` `contact_email` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_plan`  ADD `gallery_category_limit` INT NOT NULL DEFAULT '0'  AFTER `client_limit`;
ALTER TABLE `tbl_user_info`  ADD `country_id` INT NOT NULL  AFTER `user_name`;


/**
 * Author:  Mohammed
 * Created: 11, 09, 2020
 */

ALTER TABLE `tbl_user_payment_info`  ADD `is_active_qr_code` INT NOT NULL DEFAULT '1'  AFTER `qr_code`;