<!DOCTYPE html>
<html lang="en">
    <head>

        <!--
                Basic
        -->
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <title><?= isset($user_info->user_name) && !empty($user_info->user_name) ? $user_info->user_name : 'Weborative IT Consultancy LLP' ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Weborative IT Consultancy LLP" />
        <meta name="keywords" content="vcard, resposnive, resume, personal, card, cv, cards, portfolio" />
        <meta name="author" content="Weborative IT Consultancy LLP" />

        <!--
                Load Fonts
        -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/fontawesome_all.css">

        <!--
                Load CSS
        -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/basic.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/layout.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/blogs.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/ionicons.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/fancybox-master/dist/jquery.fancybox.min.css">

        <!--
                Background Gradient
        -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/gradient.css" />

        <!--
                Template New-Skin
        -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/new-skin/new-skin.css" />

        <!--
                Template Colors
        -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/template-colors/<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? $profile_data->theme_color : 'green' ?>.css" />

        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--
                Favicons
        -->
        <link rel="shortcut icon" href="<?= base_url() ?>assets/images/favicons/favicon.png">
        <style>
            .owl-nav{
                position: absolute;
                top: 45px;
                width: 100%;
                height: 50px;
            }
            .owl-prev{
                float: left;
            }
            .owl-next{
                float: right;
            }
            .owl-nav span{
                font-size: 32px;
            }
            .owl-nav button{
                background: #7f7f7f !important;
                color: #fff !important;
                width: 15px;
                text-align: center;
            }

            #google_translate_element select {
                border: 0;
                font-family: 'Poppins';
                text-transform: uppercase;
                font-size: 14px;
                padding: 10px;
                width:96%;
            }

            .add_for:after {
                height: 0px !important;
            }
        </style>

    </head>

    <body>
        <div class="page new-skin">

            <!-- preloader -->
            <div class="preloader">
                <div class="centrize full-width">
                    <div class="vertical-center">
                        <div class="spinner">
                            <div class="double-bounce1"></div>
                            <div class="double-bounce2"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- background -->
            <div class="background gradient">
                <ul class="bg-bubbles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>

            <!--
                    Container
            -->
            <div class="container opened" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">

                <!--
                        Header
                -->
                <header class="header">

                    <!-- menu -->
                    <div class="top-menu">
                        <ul>
                            <li class="active">
                                <a href="#about-card">
                                    <span class="icon ion-person"></span>
                                    <span class="link">About</span>
                                </a>
                            </li>
                            <?php
                            if ($this->profile_type == 1) {
                                if (isset($service_data) && !empty($service_data)) {
                                    ?>
                                    <li>
                                        <a href="#service-card">
                                            <span class="icon ion-android-list"></span>
                                            <span class="link">Service</span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            } else if ($this->profile_type == 2) {
                                if ((isset($experience_data) && !empty($experience_data)) || (isset($education_data) && !empty($education_data)) || (isset($skill_data) && !empty($skill_data)) || (isset($language_data) && !empty($language_data)) || (isset($additional_data) && !empty($additional_data))) {
                                    ?>
                                    <li>
                                        <a href="#resume-card">
                                            <span class="icon ion-android-list"></span>
                                            <span class="link">Resume</span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            if (isset($gallery_data_cnt) && !empty($gallery_data_cnt) && $gallery_data_cnt > 0) {
                                ?>
                                <li>
                                    <a href="#works-card">
                                        <span class="icon ion-paintbrush"></span>
                                        <span class="link">Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#gallery-card">
                                        <span class="icon ion-image"></span>
                                        <span class="link">Gallery</span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="#contacts-card">
                                    <span class="icon ion-at"></span>
                                    <span class="link">Contact</span>
                                </a>
                            </li>
                            <?php if (isset($payment_data) && !empty($payment_data)) { ?>
                                <li>
                                    <a href="#payment-card">
                                        <span class="icon ion-pound"></span>
                                        <span class="link">Payment</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                </header>

                <!--
                        Card - Started
                -->
                <div class="card-started" id="home-card">

                    <!--
                            Profile
                    -->
                    <div class="profile <?= (isset($profile_data->profile_compressed_photo) && !empty($profile_data->profile_compressed_photo) && $profile_data->is_active_profile_photo == 1 ? '' : 'no-photo' ) ?>">

                        <!-- profile image -->
                        <div class="slide" style="background-image: url(<?= base_url() . (isset($profile_data->cover_compressed_photo) && !empty($profile_data->cover_compressed_photo) ? $profile_data->cover_compressed_photo : 'assets/images/bg.jpg') ?>);"></div>

                        <div class="image">
                            <?php if (isset($profile_data->profile_compressed_photo) && !empty($profile_data->profile_compressed_photo) && $profile_data->is_active_profile_photo == 1) { ?>
                                <!-- profile photo -->
                                <img src="<?= base_url() . (isset($profile_data->profile_compressed_photo) && !empty($profile_data->profile_compressed_photo) ? $profile_data->profile_compressed_photo : 'assets/images/profile.png' ) ?>" alt="" />
                            <?php } ?>
                        </div>

                        <!-- profile titles -->
                        <div class="title"><?= isset($user_info->user_name) && !empty($user_info->user_name) ? $user_info->user_name : '' ?></div>
                        <div class="subtitle"><?= isset($profile_data->designation) && !empty($profile_data->designation) && $profile_data->is_active_designation == 1 ? $profile_data->designation : '' ?></div>

                        <!-- profile socials -->
                        <div class="social">
                            <?php
                            $total_link = 0;
                            if (isset($profile_data) && !empty($profile_data)) {
                                if ((isset($profile_data->website) && !empty($profile_data->website)) && (isset($profile_data->is_active_website) && !empty($profile_data->is_active_website) && $profile_data->is_active_website == 1)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->website ?>"><span class="fab fa-dribbble"></span></a>
                                    <?php
                                }
                                if ((isset($profile_data->instagram) && !empty($profile_data->instagram)) && (isset($profile_data->is_active_instagram) && !empty($profile_data->is_active_instagram) && $profile_data->is_active_instagram == 1)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->instagram ?>"><span class="fab fa-instagram"></span></a>
                                    <?php
                                }
                                if ((isset($profile_data->twitter) && !empty($profile_data->twitter)) && (isset($profile_data->is_active_twitter) && !empty($profile_data->is_active_twitter) && $profile_data->is_active_twitter == 1)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->twitter ?>"><span class="fab fa-twitter"></span></a>
                                    <?php
                                }
                                if ((isset($profile_data->facebook) && !empty($profile_data->facebook)) && (isset($profile_data->is_active_facebook) && !empty($profile_data->is_active_facebook) && $profile_data->is_active_facebook == 1)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->facebook ?>"><span class="fab fa-facebook"></span></a>
                                    <?php
                                }
                                if ((isset($profile_data->linkedin) && !empty($profile_data->linkedin)) && (isset($profile_data->is_active_linkedin) && !empty($profile_data->is_active_linkedin) && $profile_data->is_active_linkedin == 1)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->linkedin ?>"><span class="fab fa-linkedin"></span></a>
                                    <?php
                                }
                                if ((isset($profile_data->whatsapp) && !empty($profile_data->whatsapp)) && (isset($profile_data->is_active_whatsapp) && !empty($profile_data->is_active_whatsapp) && $profile_data->is_active_whatsapp == 1) && ($total_link < 6)) {
                                    $total_link += 1;
                                    ?>
                                    <a target="_blank" href="<?= $profile_data->whatsapp ?>"><span class="fab fa-whatsapp"></span></a>   
                                    <?php
                                }
                                for ($i = 1; $i < 6; $i++) {
                                    $cs_link = 'custom_link_' . $i;
                                    $cs_link_active = 'is_active_custom_link_' . $i;
                                    if ((isset($profile_data->$cs_link) && !empty($profile_data->$cs_link)) && (isset($profile_data->$cs_link_active) && !empty($profile_data->$cs_link_active) && $profile_data->$cs_link_active == 1) && ($total_link < 6)) {
                                        ?>
                                        <a target="_blank" href="<?= $profile_data->$cs_link ?>"><span class="fab fa-dribbble"></span></a>
                                        <?php
                                    }
                                }
                                ?>
                            <?php } ?>
                        </div>

                        <!-- profile buttons -->
                        <?php
                        $document_url = isset($profile_data->document) && !empty($profile_data->document) ? $profile_data->document : '';
                        ?>
                        <div class="lnks">
                            <?php if (isset($document_url) && !empty($document_url)) { ?>
                                <a href="<?= $document_url ?>" class="lnk" download>
                                    <span class="text">Download CV</span>
                                    <span class="ion ion-archive"></span>
                                </a>
                            <?php } ?>
                            <?php if ((isset($profile_data->vcf_phone) && !empty($profile_data->vcf_phone)) && (isset($profile_data->vcf_first_name) && !empty($profile_data->vcf_first_name)) && (isset($profile_data->vcf_last_name) && !empty($profile_data->vcf_last_name))) { ?>
                                <a href="<?= base_url() ?>Home/contactVcardExportService/<?= isset($card_user_id) && !empty($card_user_id) ? $card_user_id : '' ?>" download title="" class="lnk">
                                    <span class="text">Save Contact</span>
                                    <span class="ion ion-archive"></span>
                                </a>
                            <?php } ?>
                            <a href="javascript:void(0);" id="sharethis" class="lnk">
                                <span class="text">Share Profile</span>
                                <span class="arrow"></span>
                            </a>
                            <a href="javascript:void(0);"  class="lnk">
                                <div id="google_translate_element"></div>
                                <!--<span class="ion ion-archive"></span>-->
                            </a>
                        </div>

                    </div>

                </div>

                <!-- 
                        Card - About
                -->
                <div class="card-inner animated active" id="about-card">
                    <div class="card-wrap">

                        <?php if (isset($about_data) && !empty($about_data)) { ?>
                            <!-- 
                                    About 
                            -->
                            <div class="content about">

                                <!-- title -->
                                <div class="title"><?= isset($about_data->about_title) && !empty($about_data->about_title) ? $about_data->about_title : 'About Me' ?></div>

                                <!-- content -->
                                <div class="row">
                                    <div class="col col-d-12 col-t-12 col-m-12 border-line-v">
                                        <div class="text-box">
                                            <p>
                                                <?= isset($about_data->about_desc) && !empty($about_data->about_desc) ? $about_data->about_desc : '' ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>
                        <?php } ?>

                        <?php if (isset($client_data) && !empty($client_data)) { ?>
                            <!--
                                    Clients
                            -->
                            <div class="content clients">

                                <!-- title -->
                                <div class="title">Clients</div>

                                <!-- content -->
                                <div id="owl-slider" class="owl-carousel"> 
                                    <?php
                                    if (isset($client_data) && !empty($client_data)) {
                                        foreach ($client_data as $k2 => $v2) {
                                            $client_data_url = (isset($v2->compressed_client_logo) && !empty($v2->compressed_client_logo) && file_exists($v2->compressed_client_logo) ? $v2->compressed_client_logo : 'assets/admin/img/no_image.jpg');
                                            ?>
                                            <div>
                                                <img style="padding: 15px;" src="<?= isset($client_data_url) && !empty($client_data_url) ? base_url() . $client_data_url : '' ?>"/>
                                                <div class="textoverlay" style="text-align: center;">
                                                    <span class="text"><?= isset($v2->client_name) && !empty($v2->client_name) ? $v2->client_name : '' ?></span>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                        <?php } ?>
                    </div>
                </div>

                <?php if ($this->profile_type == 1) { ?>
                    <!--
                                                Card - Service
                    -->
                    <div class="card-inner" id="service-card">
                        <div class="card-wrap">
                            <?php if ((isset($service_data) && !empty($service_data))) { ?> 
                                <!--
                                 && (isset($service_data->service_item_data) && !empty($service_data->service_item_data))
                                    Services
                                -->
                                <div class="content services">

                                    <!-- title -->
                                    <div class="title"><?= isset($service_data->service_title) && !empty($service_data->service_title) ? $service_data->service_title : 'My Services' ?></div>

                                    <!-- content -->
                                    <div class="row service-items border-line-v">

                                        <!-- service item -->
                                        <?php
                                        if (isset($service_data->service_item_data) && !empty($service_data->service_item_data)) {
                                            foreach ($service_data->service_item_data as $k1 => $v1) {
                                                ?>
                                                <div class="col col-d-6 col-t-6 col-m-12 border-line-h">
                                                    <div class="service-item">
                                                        <div class="name"><?= isset($v1->service_item_title) && !empty($v1->service_item_title) ? $v1->service_item_title : '' ?></div>
                                                        <p><?= isset($v1->service_item_desc) && !empty($v1->service_item_desc) ? $v1->service_item_desc : '' ?></p>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>

                                        <div class="clear"></div>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                } else if ($this->profile_type == 2) {
                    if ((isset($experience_data) && !empty($experience_data)) || (isset($education_data) && !empty($education_data)) || (isset($skill_data) && !empty($skill_data)) || (isset($language_data) && !empty($language_data)) || (isset($additional_data) && !empty($additional_data))) {
                        ?>
                        <!--
                                Card - Resume
                        -->
                        <div class="card-inner" id="resume-card">
                            <div class="card-wrap">
                                <?php if ((isset($experience_data) && !empty($experience_data)) || (isset($education_data) && !empty($education_data))) { ?>
                                    <!--
                                            Resume
                                    -->
                                    <div class="content resume">

                                        <!-- title -->
                                        <div class="title">Resume</div>

                                        <!-- content -->
                                        <div class="row">

                                            <?php if (isset($experience_data) && !empty($experience_data)) { ?>
                                                <!-- experience -->
                                                <div class="col col-d-6 col-t-6 col-m-12 border-line-v">
                                                    <div class="resume-title border-line-h">
                                                        <div class="icon"><i class="ion ion-briefcase"></i></div>
                                                        <div class="name">Experience</div>
                                                    </div>
                                                    <div class="resume-items">
                                                        <?php
                                                        foreach ($experience_data as $k3 => $v3) {
                                                            ?>
                                                            <div class="resume-item border-line-h <?= isset($v3->current_status) && !empty($v3->current_status) && $v3->current_status == 1 ? 'active' : '' ?>">
                                                                <div class="date"><?= isset($v3->period_from) && !empty($v3->period_from) ? $v3->period_from . (isset($v3->current_status) && !empty($v3->current_status) && $v3->current_status == 1 ? ' - Present' : (isset($v3->period_to) && !empty($v3->period_to) ? ' - ' . $v3->period_to : '')) : '' ?></div>
                                                                <div class="name"><?= isset($v3->designation_course_name) && !empty($v3->designation_course_name) ? $v3->designation_course_name : '' ?></div>
                                                                <div class="company"><?= isset($v3->company_university_name) && !empty($v3->company_university_name) ? $v3->company_university_name : '' ?></div>
                                                                <p><?= isset($v3->description) && !empty($v3->description) ? $v3->description : '' ?></p>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            if (isset($education_data) && !empty($education_data)) {
                                                ?>
                                                <!-- education -->
                                                <div class="col col-d-6 col-t-6 col-m-12 border-line-v">
                                                    <div class="resume-title border-line-h">
                                                        <div class="icon"><i class="ion ion-university"></i></div>
                                                        <div class="name">Education</div>
                                                    </div>
                                                    <div class="resume-items">
                                                        <?php
                                                        foreach ($education_data as $k3 => $v3) {
                                                            ?>
                                                            <div class="resume-item border-line-h <?= isset($v3->current_status) && !empty($v3->current_status) && $v3->current_status == 1 ? 'active' : '' ?>">
                                                                <div class="date"><?= isset($v3->period_from) && !empty($v3->period_from) ? $v3->period_from . (isset($v3->current_status) && !empty($v3->current_status) && $v3->current_status == 1 ? ' - Present' : (isset($v3->period_to) && !empty($v3->period_to) ? ' - ' . $v3->period_to : '')) : '' ?></div>
                                                                <div class="name"><?= isset($v3->designation_course_name) && !empty($v3->designation_course_name) ? $v3->designation_course_name : '' ?></div>
                                                                <div class="company"><?= isset($v3->company_university_name) && !empty($v3->company_university_name) ? $v3->company_university_name : '' ?></div>
                                                                <p><?= isset($v3->description) && !empty($v3->description) ? $v3->description : '' ?></p>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div class="clear"></div>
                                        </div>


                                    </div>
                                    <?php
                                }

                                if ((isset($skill_data) && !empty($skill_data)) || (isset($language_data) && !empty($language_data)) || (isset($additional_data) && !empty($additional_data))) {
                                    ?>
                                    <!--
                                            Skills
                                    -->
                                    <div class="content skills">

                                        <!-- title -->
                                        <div class="title">My Skills</div>

                                        <!-- content -->
                                        <div class="row">

                                            <?php if (isset($skill_data) && !empty($skill_data)) { ?>
                                                <!-- skill item -->
                                                <div class="col col-d-6 col-t-6 col-m-12 border-line-v">
                                                    <div class="skills-list circles">
                                                        <div class="skill-title border-line-h">
                                                            <div class="icon"><i class="ion ion-code"></i></div>
                                                            <div class="name">Technical Skills</div>
                                                        </div>
                                                        <ul>
                                                            <?php
                                                            if (isset($skill_data) && !empty($skill_data)) {
                                                                foreach ($skill_data as $k5 => $v5) {
                                                                    ?>
                                                                    <li> 
                                                                        <div class="name"><?= isset($v5->skill_name) && !empty($v5->skill_name) ? $v5->skill_name : '' ?></div>
                                                                        <div class="progress p<?= isset($v5->skill_per) && !empty($v5->skill_per) ? $v5->skill_per : '' ?>">
                                                                            <span><?= isset($v5->skill_per) && !empty($v5->skill_per) ? $v5->skill_per : '' ?>%</span>
                                                                        </div>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <?php if (isset($language_data) && !empty($language_data)) { ?>
                                                <!-- skill item -->
                                                <div class="col col-d-6 col-t-6 col-m-12 border-line-v">
                                                    <div class="skills-list dotted">
                                                        <div class="skill-title border-line-h">
                                                            <div class="icon"><i class="ion ion-flag"></i></div>
                                                            <div class="name">Languages</div>
                                                        </div>
                                                        <ul>
                                                            <?php
                                                            if (isset($language_data) && !empty($language_data)) {
                                                                foreach ($language_data as $k6 => $v6) {
                                                                    ?>
                                                                    <li class="border-line-h"> 
                                                                        <div class="name"><?= isset($v6->language_name) && !empty($v6->language_name) ? $v6->language_name : '' ?></div>
                                                                        <div class="progress">
                                                                            <div class="percentage" style="width:<?= isset($v6->language_per) && !empty($v6->language_per) ? $v6->language_per : '0' ?>%;"></div>
                                                                        </div>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <?php
                                            if (isset($additional_data) && !empty($additional_data)) {
                                                foreach ($additional_data as $k7 => $v7) {
                                                    if (isset($v7->additional_description) && !empty($v7->additional_description)) {
                                                        ?>
                                                        <!-- skill item -->
                                                        <div class="col col-d-6 col-t-6 col-m-12 border-line-v">
                                                            <div class="skills-list list">
                                                                <div class="skill-title border-line-h">
                                                                    <div class="icon"><i class="ion ion-android-list"></i></div>
                                                                    <div class="name"><?= isset($v7->additional_title) && !empty($v7->additional_title) ? $v7->additional_title : '' ?></div>
                                                                </div>
                                                                <ul>
                                                                    <?php
                                                                    if (isset($v7->additional_description) && !empty($v7->additional_description)) {
                                                                        foreach ($v7->additional_description as $k => $v8) {
                                                                            ?>
                                                                            <li> 
                                                                                <div class="name"><?= isset($v8->additional_description) && !empty($v8->additional_description) ? $v8->additional_description : '' ?></div>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                            <div class="clear"></div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>


                <?php if (isset($gallery_data_cnt) && !empty($gallery_data_cnt) && $gallery_data_cnt > 0) { ?>
                    <!--
                            Card - Works
                    -->
                    <div class="card-inner" id="works-card">
                        <div class="card-wrap">

                            <!--
                                    Works
                            -->
                            <div class="content works">

                                <!-- title -->
                                <div class="title">Recent Works</div>

                                <!-- filters -->
                                <div class="filter-menu filter-button-group">
                                    <div class="f_btn active">
                                        <label><input type="radio" name="fl_radio" value="grid-item" />All</label>
                                    </div>
                                    <?php
                                    if (isset($gallery_data) && !empty($gallery_data)) {
                                        foreach ($gallery_data as $k10 => $v10) {
                                            if (isset($v10->gallery) && !empty($v10->gallery)) {
                                                ?>
                                                <div class="f_btn">
                                                    <label><input type="radio" name="fl_radio" value="<?= isset($v10->gallery_category_name) && !empty($v10->gallery_category_name) ? cleanString(str_replace(' ', '_', $v10->gallery_category_name)) : '' ?>" /><?= isset($v10->gallery_category_name) && !empty($v10->gallery_category_name) ? $v10->gallery_category_name : '' ?></label>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>

                                <!-- content -->
                                <div class="row grid-items border-line-v">
                                    <?php
                                    if (isset($gallery_data) && !empty($gallery_data)) {
                                        foreach ($gallery_data as $k11 => $v11) {
                                            if (isset($v11->gallery) && !empty($v11->gallery)) {
                                                foreach ($v11->gallery as $k12 => $v12) {
                                                    if (isset($v12->gallery_description) && !empty($v12->gallery_description)) {
                                                        ?>
                                                        <!-- work item photo -->
                                                        <div class="col col-d-6 col-t-6 col-m-12 grid-item <?= isset($v11->gallery_category_name) && !empty($v11->gallery_category_name) ? cleanString(str_replace(' ', '_', $v11->gallery_category_name)) : '' ?> border-line-h">
                                                            <div class="box-item">
                                                                <div class="image">
                                                                    <a href="#galler_<?= $v12->gallery_id ?>" class="has-popup-media">
                                                                        <img src="<?= base_url() . (isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : 'assets/admin/img/no_image.jpg') ?>" alt="" />
                                                                        <span class="info">
                                                                            <span class="ion ion-images"></span>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                                <div class="desc">
                                                                    <a href="#galler_<?= $v12->gallery_id ?>" class="name has-popup-media"><?= isset($v12->gallery_name) && !empty($v12->gallery_name) ? $v12->gallery_name : '' ?></a>
                                                                    <div class="category"><?= isset($v11->gallery_category_name) && !empty($v11->gallery_category_name) ? $v11->gallery_category_name : '' ?></div>
                                                                </div>
                                                                <div id="galler_<?= $v12->gallery_id ?>" class="popup-box mfp-fade mfp-hide">
                                                                    <div class="content">
                                                                        <div class="image">
                                                                            <img src="<?= base_url() . (isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : 'assets/admin/img/no_image.jpg') ?>" alt="">
                                                                        </div>
                                                                        <?php if (isset($v12->gallery_description) && !empty($v12->gallery_description)) { ?>
                                                                            <div class="desc">
                                                                                <a href="javascript:void(0);" class="button product_enqiry" data-category="<?= $v11->gallery_category_name ?>" data-product="<?= $v12->gallery_name ?>">
                                                                                    <span class="fab fa-whatsapp"></span>
                                                                                    <span class="text">Enquiry </span>
                                                                                </a>
                                                                                <div class="post-box">
                                                                                    <?= $v12->gallery_description ?>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="col col-d-6 col-t-6 col-m-12 grid-item <?= isset($v11->gallery_category_name) && !empty($v11->gallery_category_name) ? $v11->gallery_category_name : '' ?> border-line-h">
                                                            <div class="box-item">
                                                                <div class="image">
                                                                    <a href="<?= base_url() . (isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : 'assets/admin/img/no_image.jpg') ?>" class="has-popup-image">
                                                                        <img src="<?= base_url() . (isset($v12->gallery_image) && !empty($v12->gallery_image) ? $v12->gallery_image : 'assets/admin/img/no_image.jpg') ?>" alt="" />
                                                                        <span class="info">
                                                                            <span class="ion ion-image"></span>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                                <div class="desc">
                                                                    <a href="<?= base_url() ?><?= isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : '' ?>" class="name has-popup-image"><?= isset($v12->gallery_name) && !empty($v12->gallery_name) ? $v12->gallery_name : '' ?></a>
                                                                    <div class="category"><?= isset($v11->gallery_category_name) && !empty($v11->gallery_category_name) ? $v11->gallery_category_name : '' ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="clear"></div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="card-inner" id="gallery-card">
                        <div class="card-wrap">

                            <!--
                                    Works
                            -->
                            <div class="content works">

                                <!-- title -->
                                <div class="title">Gallery</div>

                                <!-- filters -->
                                <div class="filter-menu filter-button-group">
                                    <div class="f_btn active">
                                        <label><input type="radio" name="fl_radio" value="grid-item" />All</label>
                                    </div>
                                    <?php
                                    if (isset($gallery_data) && !empty($gallery_data)) {
                                        foreach ($gallery_data as $k10 => $v10) {
                                            if (isset($v10->gallery) && !empty($v10->gallery)) {
                                                ?>
                                                <div class="f_btn">
                                                    <label><input type="radio" name="fl_radio" value="<?= isset($v10->gallery_category_name) && !empty($v10->gallery_category_name) ? cleanString(str_replace(' ', '_', $v10->gallery_category_name)) : '' ?>" /><?= isset($v10->gallery_category_name) && !empty($v10->gallery_category_name) ? $v10->gallery_category_name : '' ?></label>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>

                                <!-- content -->
                                <div class="row grid-items border-line-v">
                                    <?php
                                    if (isset($gallery_data) && !empty($gallery_data)) {
                                        foreach ($gallery_data as $k11 => $v11) {
                                            if (isset($v11->gallery) && !empty($v11->gallery)) {
                                                foreach ($v11->gallery as $k12 => $v12) {
                                                    ?>
                                                    <div class="col col-d-6 col-t-6 col-m-12 grid-item <?= isset($v11->gallery_category_name) && !empty($v11->gallery_category_name) ? cleanString(str_replace(' ', '_', $v11->gallery_category_name)) : '' ?> border-line-h">
                                                        <div class="box-item">
                                                            <div class="image">
                                                                <a class="fancybox" rel="fancybox-button" href="<?= base_url() ?><?= isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : '' ?>" title="Colorful Feldberg II (STEFFEN EGLY)">
                                                                    <img src="<?= base_url() ?><?= isset($v12->gallery_image) && !empty($v12->gallery_image) && file_exists($v12->gallery_image) ? $v12->gallery_image : '' ?>" alt="" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="clear"></div>
                                </div>

                            </div>

                        </div>
                    </div>

                <?php } ?>

                <!--
                        Card - Contacts
                -->
                <div class="card-inner contacts" id="contacts-card">
                    <div class="card-wrap">

                        <?php if ((isset($map_data->map) && !empty($map_data->map)) || (isset($contact_data) && !empty($contact_data))) { ?>
                            <!--
                                    Conacts Info
                            -->
                            <div class="content contacts">

                                <!-- title -->
                                <div class="title">Get in Touch</div>

                                <!-- content -->
                                <div class="row">
                                    <div class="col col-d-12 col-t-12 col-m-12 border-line-v">
                                        <?php if (isset($map_data->map) && !empty($map_data->map)) { ?>
                                            <div class="map" id="map">
                                                <iframe src="<?= $map_data->map ?>" height="100%" width="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                            <?php
                                        }
                                        if (isset($contact_data) && !empty($contact_data)) {
                                            $cnt_contact_data = count($contact_data);
                                            foreach ($contact_data as $k9 => $v9) {
                                                $k9++;
                                                ?>
                                                <div class="info-list">
                                                    <div class="title add_for" style="min-height: 30px;padding: 0 0 15px 0;"><?= isset($v9->address_of) && !empty($v9->address_of) ? $v9->address_of : '' ?></div>
                                                    <ul>
                                                        <li><strong>Name</strong> <?= isset($v9->contact_name) && !empty($v9->contact_name) ? $v9->contact_name : '' ?></li>
                                                        <?php if (isset($v9->contact_address) && !empty($v9->contact_address)) { ?>
                                                            <li><strong>Address</strong> 
                                                                <?php if (isset($v9->map_link) && !empty($v9->map_link)) { ?>
                                                                    <a href="<?= $v9->map_link ?>" target="_blank" class="cntus">
                                                                        <span class="text"><?= $v9->contact_address ?></span>
                                                                    </a>
                                                                    <?php
                                                                } else {
                                                                    echo $v9->contact_address;
                                                                }
                                                                ?>
                                                            </li>
                                                        <?php } ?>
                                                        <?php if (isset($v9->contact_email) && !empty($v9->contact_email)) { ?><li><strong>Email</strong> <a class="cntus" href="mailto:<?= $v9->contact_email ?>"><?= $v9->contact_email ?></a></li><?php } ?>
                                                        <?php if (isset($v9->ref_country_id) && !empty($v9->ref_country_id)) { ?>
                                                            <li><strong>Phone</strong> 
                                                                <a class="cntus" href="tel:<?= isset($v9->ref_country_id) && !empty($v9->ref_country_id) ? '+' . getCountryCode($v9->ref_country_id) : '' ?><?= $v9->contact_phone ?>">
                                                                    <?= isset($v9->ref_country_id) && !empty($v9->ref_country_id) ? '+' . getCountryCode($v9->ref_country_id) : '' ?> <?= $v9->contact_phone ?>
                                                                </a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <?php
                                                if ($k9 < $cnt_contact_data) {
                                                    echo "<hr style='opacity: 0.3;'>";
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>
                        <?php } ?>
                        <!--
                                Contact Form
                        -->
                        <div class="content contacts">

                            <!-- title -->
                            <div class="title">Contact Form</div>

                            <!-- content -->
                            <div class="row">
                                <div class="col col-d-12 col-t-12 col-m-12 border-line-v">
                                    <div class="contact_form">
                                        <form id="cform" method="post">
                                            <div class="row">
                                                <div class="col col-d-12 col-t-12 col-m-12">
                                                    <div class="group-val">
                                                        <input type="text" name="name" placeholder="Full Name" />
                                                    </div>
                                                </div>
                                                <div class="col col-d-12 col-t-12 col-m-12">
                                                    <div class="group-val">
                                                        <input type="text" name="email" placeholder="Email Address" />
                                                    </div>
                                                </div>
                                                <div class="col col-d-12 col-t-12 col-m-12">
                                                    <div class="group-val">
                                                        <textarea name="message" placeholder="Your Message"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="align-left">
                                                <a href="#" class="button" onclick="$('#cform').submit(); return false;">
                                                    <span class="text">Send Message</span>
                                                    <span class="arrow"></span>
                                                </a>
                                            </div>
                                        </form>
                                        <div class="alert-success">
                                            <p style="color:green;">Thanks, your message is sent successfully.</p>
                                        </div>
                                        <div class="alert-error" style="display: none;">
                                            <p style="color:red;">Something went wrong. Try again.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>

                    </div>
                </div>


                <?php if (isset($payment_data) && !empty($payment_data)) { ?>
                    <!--
                            Card - Payment
                    -->
                    <div class="card-inner animated" id="payment-card">
                        <div class="card-wrap">

                            <!-- 
                                    Payment 
                            -->
                            <div class="content about">

                                <!-- title -->
                                <div class="title">Our Payment Information</div>

                                <?php
                                if (isset($payment_data) && !empty($payment_data)) {
                                    foreach ($payment_data as $k13 => $v13) {
                                        ?>
                                        <div class="row grid-items border-line-h">
                                            <!-- work item photo -->
                                            <?php if ((isset($v13->qr_code) && !empty($v13->qr_code)) && ($v13->is_active_qr_code == 1)) { ?>
                                                <div class="col col-d-6 col-t-12 col-m-12 grid-item photo">
                                                    <div class="box-item">
                                                        <div class="image">
                                                            <a href="<?= base_url() ?><?= isset($v13->qr_code) && !empty($v13->qr_code) && file_exists($v13->qr_code) ? $v13->qr_code : '' ?>" class="has-popup-image">
                                                                <img src="<?= base_url() ?><?= isset($v13->qr_code) && !empty($v13->qr_code) && file_exists($v13->qr_code) ? $v13->qr_code : '' ?>" alt="" />
                                                                <span class="info">
                                                                    <span class="ion ion-image"></span>
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div class="desc" style="padding: 10px 0 0 0;">
                                                            <a href="<?= base_url() ?><?= isset($v13->qr_code) && !empty($v13->qr_code) && file_exists($v13->qr_code) ? $v13->qr_code : '' ?>" class="name has-popup-image">Qr Code</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col col-t-12 col-m-12 grid-item photo border-line-h <?= isset($v13->qr_code) && !empty($v13->qr_code) && $v13->is_active_qr_code == 1 ? 'col-d-6' : 'col-d-12' ?>">
                                                <div class="info-list">
                                                    <ul>
                                                        <?php if (isset($v13->bank_name) && !empty($v13->bank_name)) { ?><li><strong>Bank Name </strong> <span id="bank_name_<?= $k13 ?>"><?= $v13->bank_name ?></span></li><?php } ?>
                                                        <?php if (isset($v13->acc_holder_name) && !empty($v13->acc_holder_name)) { ?><li><strong>Account Name</strong> <span id="acc_holder_name_<?= $k13 ?>"><?= $v13->acc_holder_name ?></span></li><?php } ?>
                                                        <?php if (isset($v13->account_number) && !empty($v13->account_number)) { ?><li><strong>Bank Account Number </strong> <span id="account_number_<?= $k13 ?>"><?= $v13->account_number ?></span></li><?php } ?>
                                                        <?php if (isset($v13->ifsc_code) && !empty($v13->ifsc_code)) { ?><li><strong>Bank IFSC Code</strong> <span id="ifsc_code_<?= $k13 ?>"><?= $v13->ifsc_code ?></span></li><?php } ?>
                                                        <?php if (isset($v13->branch_name) && !empty($v13->branch_name)) { ?><li><strong>Bank Branch</strong> <span id="branch_name_<?= $k13 ?>"><?= $v13->branch_name ?></span></li><?php } ?>
                                                    </ul>
                                                </div>
                                                <a href="javascript:void(0);" class="button copy_bank_name" data-bank_id="<?= $k13 ?>">
                                                    <span class="text">Copy Bank Detail</span>
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <!-- content -->


                            </div>

                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <!--
                jQuery Scripts
        -->
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/js/jquery.validate.js"></script>
        <script src="<?= base_url() ?>assets/js/jquery.magnific-popup.js"></script>
        <script src="<?= base_url() ?>assets/js/imagesloaded.pkgd.js"></script>
        <script src="<?= base_url() ?>assets/js/isotope.pkgd.js"></script>
        <script src="<?= base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?= base_url() ?>assets/js/owl.carousel.js"></script>
        <script src="<?= base_url() ?>assets/plugins/fancybox-master/dist/jquery.fancybox.min.js"></script>
        <script>
                                                    $(".fancybox").fancybox({
                                                        animationEffect: 'fade'
                                                    }).attr('data-fancybox', 'group1');

                                                    $("#owl-slider").owlCarousel({
                                                        autoPlay: false,
                                                        nav: true,
                                                        responsive: {
                                                            0: {
                                                                items: 2,
                                                                nav: true,
                                                                loop: true
                                                            },
                                                            600: {
                                                                items: 3,
                                                                nav: true,
                                                                loop: true
                                                            },
                                                            1000: {
                                                                items: 4,
                                                                nav: true,
                                                                loop: true
                                                            }
                                                        }
                                                    })

                                                    $(document).ready(function () {
                                                        $("#owl-slider").owlCarousel();

                                                    });
        </script>

        <!--
                Main Scripts
        -->
        <script src="<?= base_url() ?>assets/js/scripts.js"></script>

        <script type="text/javascript">
                                                    function googleTranslateElementInit() {
                                                        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                                                    }
        </script>

        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

        <script>
                                                    $("#sharethis").on("click", function () {
                                                        var link = '<?= isset($user_info->user_short_link) && !empty($user_info->user_short_link) ? $user_info->user_short_link : '' ?>';
                                                        var message = 'You%20can%20see%20my%20Digital%20Business%20Card%20and%20Save%20my%20Number%20from%20given%20below%20link%20' + link;
                                                        var person = prompt("Please Enter number to Share:", "");
                                                        if (person == null || person == "") {
                                                            alert("Please Enter Mobile Number.");
                                                        } else if (isNaN(person) || person.length != 10) {
                                                            alert("Please Enter Only 10 digit Mobile Number.");
                                                        } else {
                                                            window.open("https://api.whatsapp.com/send?text=" + message + "&phone=+91" + person, "_BLANK");
                                                        }
                                                    });

                                                    $(".product_enqiry").on("click", function () {
                                                        var contact_number = '<?= isset($user_info->user_number) && !empty($user_info->user_number) ? (isset($user_info->country_id) && !empty($user_info->country_id) ? getCountryCode($user_info->country_id) : '91') . $user_info->user_number : '' ?>';
                                                        var category_name = $(this).attr('data-category');
                                                        var product_name = $(this).attr('data-product');
//                                                        var message = 'Hi,%20I%20am%20interested%20in%20your%20product/service%20(' + category_name + '):%20Planning%20(' + product_name + ').%20Please%20provide%20more%20details.';
//                                                        var message = 'https://wa.me/918712709770?text=Hi,%20I%20am%20interested%20in%20your%20product/service:%20' + product_name + '.%20Please%20provide%20more%20details.';
                                                        var message = 'Hi,%20I%20am%20interested%20in%20your%20product/service:%20' + product_name + '.%20Please%20provide%20more%20details.';
                                                        window.open("https://api.whatsapp.com/send?text=" + message + "&phone=+" + contact_number, "_BLANK");
                                                    });

                                                    $(".copy_bank_name").on("click", function () {
                                                        var bank_id = $(this).attr('data-bank_id');

                                                        var bank_name = $('#bank_name_' + bank_id).html();
                                                        bank_name = bank_name != '' && bank_name != undefined ? ' Bank Name.: ' + bank_name : '';

                                                        var acc_holder_name = $('#acc_holder_name_' + bank_id).html();
                                                        acc_holder_name = acc_holder_name != '' && acc_holder_name != undefined ? ' Account Holder Name.: ' + acc_holder_name : '';

                                                        var account_number = $('#account_number_' + bank_id).html();
                                                        account_number = account_number != '' && account_number != undefined ? ' Account Number.: ' + account_number : '';

                                                        var ifsc_code = $('#ifsc_code_' + bank_id).html();
                                                        ifsc_code = ifsc_code != '' && ifsc_code != undefined ? ' IFSC Code.: ' + ifsc_code : '';

                                                        var branch_name = $('#branch_name_' + bank_id).html();
                                                        branch_name = branch_name != '' && branch_name != undefined ? ' Branch Name.: ' + branch_name : '';

                                                        var tempItem = document.createElement('input');

                                                        tempItem.setAttribute('type', 'text');
                                                        tempItem.setAttribute('display', 'none');

                                                        let content = bank_name + acc_holder_name + account_number + ifsc_code + branch_name;
                                                        if (content instanceof HTMLElement) {
                                                            content = content.innerHTML;
                                                        }

                                                        tempItem.setAttribute('value', content);
                                                        document.body.appendChild(tempItem);

                                                        tempItem.select();
                                                        document.execCommand('Copy');

                                                        tempItem.parentElement.removeChild(tempItem);
                                                    });
        </script>

    </body>
</html>