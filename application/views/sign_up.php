<!-- pricing -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-lg-6">
                <div class="p-sm-5 px-3 py-5 rounded text-center" style="border: 1px solid #012e5b;">
                    <h3 class="section-title section-title-border">Sign Up</h3>
                    <?php echo form_open(base_url() . 'Home/signUp', $arrayName = array('id' => 'businessProfile', 'class' => 'row')) ?>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Your Name" required>
                    </div>
                    <div class="col-lg-6">
                        <input type="email" class="form-control" name="email_address" id="email_address" placeholder="Email" required>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" class="form-control numbersonly" id="contact_number" name="contact_number" placeholder="Contact Number" required>
                    </div>
                    <div class="col-lg-6">
                        <select class="form-control" id="country_id" name="country_id">
                            <option value="">Country Code</option>
                            <?php
                            if (isset($country_data) && !empty($country_data)) {
                                foreach ($country_data as $k2 => $v2) {
                                    ?>
                                    <option class="form-control" value="<?= isset($v2->id) && !empty($v2->id) ? $v2->id : '' ?>"><?= isset($v2->name) && !empty($v2->name) ? $v2->name : '' ?><?= isset($v2->phonecode) && !empty($v2->phonecode) ? ' (+' . $v2->phonecode . ')' : '' ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <select class="form-control" id="plan_type" name="plan_type">
                            <option value="">Select Plan</option>
                            <?php
                            if (isset($plan_data) && !empty($plan_data)) {
                                foreach ($plan_data as $k1 => $v1) {
                                    ?>
                                    <option class="form-control" value="<?= isset($v1->plan_id) && !empty($v1->plan_id) ? $v1->plan_id : '' ?>" <?= isset($v1->plan_id) && !empty($v1->plan_id) ? set_selected($plan_type, $v1->plan_id) : '' ?>><?= isset($v1->plan_name) && !empty($v1->plan_name) ? $v1->plan_name : '' ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6" id="profile_type_div" style="display: <?= (isset($plan_type) && !empty($plan_type) && allowProfileType($plan_type) == 1) ? 'block' : 'none' ?>">
                        <select class="form-control" id="profile_type" name="profile_type">
                            <option value="">Select Profile Type</option>
                            <option class="form-control" value="1">Service</option>
                            <option class="form-control" value="2">Resume</option>
                        </select>
                    </div>
                    <div class="col-lg-12 text-center">
                        <button type="submit" value="send" class="btn btn-sm btn-primary">Sign Up</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- /pricing -->
