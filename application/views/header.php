<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo (isset($this->pg_nm) && !empty($this->pg_nm) ? $this->pg_nm . ' |' : ''); ?>  WEBORATIVE IT CONSULTANCY LLP</title>
        <!-- mobile responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta property="og:type" content="website">
        <meta property="og:title" content="WEBORATIVE IT CONSULTANCY LLP" />
        <meta property="og:description" content="Pioneer in Information Technology" />
        <meta property="og:image" content="<?= base_url('assets/pricing') ?>/images/pp.jpg" />
        <meta property="og:site_name" content="WEBORATIVE IT CONSULTANCY LLP" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/pricing') ?>/images/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/pricing') ?>/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/pricing') ?>/images/favicon-16x16.png">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/bootstrap/bootstrap.min.css">
        <!-- magnific popup -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/magnific-popup/magnific-popup.css">
        <!-- Slick Carousel -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/slick/slick.css">
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/slick/slick-theme.css">
        <!-- themify icon -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/themify-icons/themify-icons.css">
        <!-- animate -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/animate/animate.css">
        <!-- Adroit -->
        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/adroit/adroit.css">
        <!-- Stylesheets -->
        <link href="<?= base_url('assets/pricing') ?>/css/style.css" rel="stylesheet">
        <!--Favicon-->

        <link rel="stylesheet" href="<?= base_url('assets/pricing') ?>/plugins/sweetalert2/sweetalert2.min.css">

        <style>
            .mapouter{
                position:relative;
                height:500px;
                width:100%;
            }
            .gmap_canvas {
                overflow:hidden;
                background:none!important;
                height:500px;
                width:100%;
            }
        </style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171647174-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-171647174-1');
        </script>
    </head>

    <body>


        <!-- preloader start -->
        <div class="preloader">
            <img src="<?= base_url('assets/pricing') ?>/images/preloader.gif" alt="preloader">
        </div>
        <!-- preloader end -->

        <!-- navigation -->
        <header>
            <!-- top header -->
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline text-lg-right text-center">
                                <li class="list-inline-item">
                                    <a href="mailto:<?= $footer_data->email_address ?>"><?= $footer_data->email_address ?></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript:void(0);">Call Us Now:
                                        <span class="ml-2"><?= $footer_data->phone_number ?></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- nav bar -->
            <div class="navigation">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="<?= base_url('/') ?>">
                            <img src="<?= base_url('assets/pricing') ?>/images/logo.png" alt="logo" width="80%">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="dropdown mega-dropdown nav-item">
                                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        Company
                                    </a>				
                                    <ul class="dropdown-menu mega-dropdown-menu clearfix">
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li><a class="dropdown-item" href="<?= base_url('about') ?>">Know Who We Are</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('vision-mission') ?>">Vision and Mission</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li><a class="dropdown-item" href="<?= base_url('about') ?>#client-speak">Client Speaks</a></li>	
                                                <li><a class="dropdown-item" href="<?= base_url('career') ?>">Job Opportunities</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li><a class="dropdown-item" href="<?= base_url('development-process') ?>">Development Process</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('delivery-methodologies') ?>">Delivery Methodologies</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('pricing-methodologies') ?>">Pricing Methodologies</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li><a class="dropdown-item" href="<?= base_url('quality-policy') ?>">Quality Policy</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('confidentiality') ?>">Confidentiality</a></li>
                                            </ul>
                                        </li>
                                    </ul>				
                                </li>
                                <li class="nav-item mega-dropdown dropdown">
                                    <a class="nav-link" href="<?= base_url('service') ?>" role="button" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        Service
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu clearfix">
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="" href="<?= base_url('mobile-app-services') ?>">Mobile Apps</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-app-strategy') ?>">Strategy</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-app-design') ?>">Design</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-app-development') ?>">Development</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('app-maintenance-support') ?>">Support & Maintenance</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-app-marketing') ?>">Marketing</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="" href="<?= base_url('web-ecommerce-services') ?>">Web And E-Commerce</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('web-ecommerce-consulting') ?>">Consulting</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('web-ecommerce-design') ?>">Design</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('web-ecommerce-development') ?>">Development</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('web-maintenance-support') ?>">Support & Maintenance</a></li>
                                                <li class="d-none d-sm-block"><a class="dropdown-item" href="javascript:void(0);">&nbsp;</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="javascript:void(0);">Development Stack</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('iphone-app-development') ?>">IOS</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('android-app-development') ?>">Android</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('augmented-reality-app-development') ?>">Augmented Reality</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('wearable-device-app-development') ?>">Wearable</a></li>
                                                <li><a class="dropdown-item d-none d-sm-block" href="javascript:void(0);">&nbsp;</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header d-none d-sm-block"><a class="dropdown-item" href="javascript:void(0);">&nbsp;</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('cross-platform-app-development') ?>">Cross Platform</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('angularjs-development') ?>">Angular JS</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('nodejs-development') ?>">Node JS</a></li>
                                                <li class="d-none d-sm-block"><a class="dropdown-item" href="javascript:void(0);">&nbsp;</a></li>
                                                <li class="d-none d-sm-block"><a class="dropdown-item" href="javascript:void(0);">&nbsp;</a></li>
                                            </ul>
                                        </li>
                                        <!--break line and start menu from left-->
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="<?= base_url('big-data-services') ?>">Big Data</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('big-data-intelligence') ?>">Big Data Intelligence</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('big-data-development') ?>">Big Data Development</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('big-data-analytics') ?>">Big Data Analytics</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('big-data-maintenance-support') ?>">Big Data Maintenance</a></li>
                                            </ul>
                                        </li>
                                        <!--                                        <li class="clearfix"></li>-->
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="<?= base_url('sap-services') ?>">SAP Services</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-technical-services') ?>">SAP Technical Services</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-fiori-ui5') ?>">SAP Fiori & UI5</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-bi-bo-solution') ?>">SAP BI & BO Solution</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-implementation-service') ?>">SAP Implementation</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header d-none d-sm-block">&nbsp;</li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-functional-services') ?>">SAP Functional Services</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-s4-hana-services') ?>">SAP S4 HANA Service</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-hana-services') ?>">SAP HANA Service</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('sap-hcm-service') ?>">SAP HCM Service</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="<?= base_url('mobile-game-services') ?>">Mobile Game</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-game-design') ?>">Mobile Game Design</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-game-development') ?>">Game Development</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-game-support-maintenance') ?>">Support and Maintenance</a></li>
                                                <li class="d-none d-sm-block"><a class="dropdown-item" href="javascript:void(0);">&nbsp;</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="<?= base_url('testing-services') ?>">Testing & QA</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('quality-testing') ?>">Quality Assurance Testing</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('mobile-app-testing-services') ?>">Mobile Application Testing</a></li>
                                                <li><a class="dropdown-item" href="<?= base_url('web-ecommerce-testing') ?>">Web & eCommerce Testing</a></li>
                                            </ul>
                                        </li>
                                        <!--break line and start menu from left-->
                                        <li class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                            <ul>
                                                <li class="dropdown-header"><a class="dropdown-item" href="<?= base_url('blockchain-development') ?>">Blockchain Development</a></li>
                                                <li class=""><a style="font-weight: 600;" class="dropdown-item" href="<?= base_url('internet-of-things-app-development') ?>">iOT (Internet of Things)</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?= base_url('work') ?>" role="button" aria-haspopup="true"
                                       aria-expanded="false">
                                        Work
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?= base_url('career') ?>" role="button" aria-haspopup="true"
                                       aria-expanded="false">
                                        Career
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url('contact') ?>" role="button" aria-haspopup="true"
                                       aria-expanded="false">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-primary btn-sm" href="<?= base_url('quote') ?>">get a quote</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>

        <!-- Search Form -->
        <div class="search-form">
            <a href="javascript:void(0);" class="close" id="searchClose">
                <i class="ti-close"></i>
            </a>
            <div class="container">
                <form action="#" class="row">
                    <div class="col-lg-10 mx-auto">
                        <h3>Search Here</h3>
                        <div class="input-wrapper">
                            <input type="text" class="form-control" name="search" id="search" placeholder="Search" required>
                            <button>
                                <i class="ti-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /navigation -->
