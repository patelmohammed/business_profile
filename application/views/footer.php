<footer class="bg-secondary">
    <div class="pt-5 border-bottom" style="border-color: #454547 !important">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="mb-md-0 text-center text-md-left">
                        <!-- logo -->
                        <img class="" src="<?= base_url('assets/pricing') ?>/images/<?= $footer_data->footer_logo ?>" alt="logo" style="margin-top:-20px;">
                        <p class="text-white text-justify mb-30"><?= $footer_data->footer_logo_desc ?></p>

                    </div>
                </div>
                <!-- subscribe form -->
                <div class="col-lg-4 col-md-4">
                    <div class="mt-lg-0 text-center text-md-left">
                        <h4 class="mb-4 text-white">Subscribe Us</h4>
                        <p class="text-white mb-4">Keep update with Weborative </p>
                        <form id="subscribemail" class="position-relative">
                            <input type="email" class="form-control subscribe" required name="subscribe" id="Subscribe" placeholder="Enter Your Email">
                            <button class="btn-subscribe" type="submit" value="send">
                                <i class="ti-arrow-right"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <!-- footer links -->
                <div class="col-lg-4 col-md-4">
                    <div class="mt-lg-0 text-center text-md-left">
                        <h4 class="text-white mb-4">Quick Link</h4>
                        <p class="text-white mb-4">Know more about Weborative </p>
                        <!-- social icon -->
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="social-icon-outline" href="<?= $footer_data->footer_facebook ?>">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon-outline" href="<?= $footer_data->footer_twitter ?>">
                                    <i class="ti-twitter-alt"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon-outline" href="<?= $footer_data->footer_linkedin ?>">
                                    <i class="ti-linkedin"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon-outline" href="javascript:void(0);">
                                    <i class="ti-google"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- copyright -->
    <div class="pt-4 pb-3 position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-10 text-center">
                    <p class="text-white text-center">
                        <span class="text-primary">Weborative IT Consultancy LLP</span> &copy; 2020 All Right Reserved.&nbsp;<a href="terms-condition" style="color: #ffffff;" >Terms of Use</a> and <a style="color: #ffffff;" href="privacy-policy">Privacy Policy</a></p>
                </div>
            </div>
        </div>
        <!-- back to top -->
        <button class="back-to-top">
            <i class="ti-angle-up"></i>
        </button>
    </div>
</footer>
<!-- /footer --> 

<!-- jQuery -->
<script src="<?= base_url('assets/pricing') ?>/plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="<?= base_url('assets/pricing') ?>/plugins/bootstrap/bootstrap.min.js"></script>
<!-- magnific popup -->
<script src="<?= base_url('assets/pricing') ?>/plugins/magnific-popup/jquery.magnific.popup.min.js"></script>
<!-- slick slider -->
<script src="<?= base_url('assets/pricing') ?>/plugins/slick/slick.min.js"></script>
<!-- mixitup filter -->
<script src="<?= base_url('assets/pricing') ?>/plugins/mixitup/mixitup.min.js"></script>
<!-- Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA"></script>
<script  src="<?= base_url('assets/pricing') ?>/plugins/google-map/gmap.js"></script>
<!-- Syo Timer -->
<script src="<?= base_url('assets/pricing') ?>/plugins/syotimer/jquery.syotimer.js"></script>
<!-- aos -->
<script src="<?= base_url('assets/pricing') ?>/plugins/adroit/adroit.js"></script>
<!-- Main Script -->
<script src="<?= base_url('assets/pricing') ?>/plugins/jquery-validation/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?= base_url('assets/pricing') ?>/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/pricing') ?>/js/script.js"></script>

<script>
    $("#subscribemail").submit(function (e) {
        e.preventDefault();
    }).validate({
        errorPlacement: function (label, element) {
            label.addClass('text-white');
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            var _url = '<?= base_url('subscribe_mail') ?>';
            var _post = 'post';
            var _postType = 'json';
            $.ajax({
                type: _post,
                url: _url,
                data: $('#subscribemail').serialize(),
                dataType: _postType,
                success: function (_returnData) {

                    if (_returnData.result == 'success') {
                        $("#subscribemail").trigger("reset");
                        swal(
                                'Congratulation',
                                _returnData.message,
                                'success'
                                )
                        $('#Subscribe').val();
                        return false;
                    } else if (_returnData.result == 'exists') {
                        swal(
                                'Error!',
                                _returnData.message,
                                'error'
                                )
                    } else {
                        swal(
                                'Error!',
                                _returnData.message,
                                'error'
                                )
                    }
                }
            });
            return false;
        }
    });

    $("#contactfromhome").submit(function (e) {
        e.preventDefault();
    }).validate({
        submitHandler: function (form) {
            var _url = '<?= base_url('contact_us_mail') ?>';
            var _post = 'post';
            var _postType = 'json';
            $.ajax({
                type: _post,
                url: _url,
                data: $('#contactfromhome').serialize(),
                dataType: _postType,
                success: function (_returnData) {

                    if (_returnData.result == 'success') {
                        $("#contactfromhome").trigger("reset");
                        swal(
                                'Thanks',
                                _returnData.message,
                                'success'
                                )
                        return false;
                    }
                }
            });
            return false;
        }
    });
</script>
<script>
    $(document).on('change', '#plan_type', function () {
        var id = $(this).val();
        $.ajax({
            type: 'post',
            url: '<?= base_url() ?>admin/Master/allowProfileType',
            data: {id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.allowProfileType == 1) {
                    $('#profile_type').prop("required", true);
                    $('#profile_type_div').css({"display": "block"});
                } else {
                    $('#profile_type').prop("required", false);
                    $('#profile_type_div').css({"display": "none"});
                }
            }
        });
    });
</script>

</body>

<!-- Mirrored from demo.themefisher.com/themefisher/biztrox/homepage-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 May 2019 12:28:43 GMT -->
</html>