<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> User
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/addEditUser">Add User</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>User Name</th>
                                    <th>User Email</th>
                                    <th>Plan Name</th>
                                    <th>Plan Expiry</th>
                                    <th>Password</th>
                                    <th>Short Link</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($user_data) && !empty($user_data)) {
                                    $sn = 0;
                                    foreach ($user_data as $key => $value) {
                                        if (($value->user_id != 1) && ($value->user_id != $this->user_id)) {
                                            $sn++;
                                            ?>                  
                                            <tr>
                                                <td><?= $sn ?></td>
                                                <td><?= $value->user_name ?></td> 
                                                <td><?= $value->user_email ?></td> 
                                                <td><?= isset($value->ref_plan_id) && !empty($value->ref_plan_id) ? getPlanName($value->ref_plan_id) : '' ?>
                                                <td><?= date('d-m-Y H:i', strtotime($value->plan_end_date)) ?></td> 
                                                <td><?= $value->raw ?></td> 
                                                <td>
                                                    <a href="<?= isset($value->user_short_link) && !empty($value->user_short_link) ? $value->user_short_link : 'javascript:void(0);' ?>" <?= isset($value->user_short_link) && !empty($value->user_short_link) ? 'target="_blank"' : '' ?>>
                                                        <span class="badge badge-success badge-pill"><?= isset($value->user_short_link) && !empty($value->user_short_link) ? 'Short Link' : 'Short Link Not Created' ?></span>
                                                    </a>
                                                </td> 
                                                </td> 
                                                <td>
                                                    <div class='d-flex'>
                                                        <?php if ($menu_rights['edit_right']) { ?>
                                                            <a href='<?php echo base_url() ?>admin/Master/addEditUser/<?= $value->user_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                                <i class="fal fa-edit"></i>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($menu_rights['delete_right']) { ?>
                                                            <a href='javascript:void(0);' data-id="<?= $value->user_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_user' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                                <i class="fal fa-times"></i>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        var swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
                .fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                })
                .then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('admin/Dashboard/deleteUser') ?>',
                            dataType: 'json',
                            data: {id: id},
                            success: function (returnData) {
                                if (returnData.result == true) {
                                    ``
                                    window.location.reload();
                                } else {
                                    swalWithBootstrapButtons.fire(
                                            "Something Wrong",
                                            "Your record not deleted :(",
                                            "error"
                                            );
                                }
                                return false;
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons.fire(
                                "Cancelled",
                                "Your record is safe :)",
                                "success"
                                );
                    }
                });
    });
</script>