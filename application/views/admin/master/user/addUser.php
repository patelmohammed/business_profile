<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add User
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/user">User</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Master/addEditUser', $arrayName = array('id' => 'addEditUser', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_name">User Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="user_name" id="user_name" placeholder="User Name" required>
                                <div class="invalid-feedback">
                                    User Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="user_email">User Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="email" class="form-control" name="user_email" id="user_email" placeholder="User Email" required>
                                <div class="invalid-feedback">
                                    User Email Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mobile">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="mobile" id="mobile" placeholder="Contact Number" required>
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="plan_type">Plan <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="plan_type" id="plan_type" required="">
                                    <option></option>
                                    <?php
                                    if (isset($plan_data) && !empty($plan_data)) {
                                        foreach ($plan_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= $v1->plan_id ?>"><?= $v1->plan_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Plan Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3" style="display:none;" id="profile_type_div">
                                <label class="form-label" for="profile_type">Profile Type <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="profile_type" id="profile_type">
                                    <option></option>
                                    <option value="1">Service</option>
                                    <option value="2">Resume</option>
                                </select>
                                <div class="invalid-feedback">
                                    Profile Type Required
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 0px;margin-bottom: 0px;padding: 0px;margin-bottom: 6px;">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="checkall_side_menu">Side Menu Access</label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="checkall_side_menu">
                                        <label class="custom-control-label" for="checkall_side_menu">Select All</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Menu</th>
                                            <th>Full Access</th>
                                            <th>View</th>
                                            <th>Add</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($side_menu)) {
                                            foreach ($side_menu as $key => $value) {
                                                if ($value->menu_url == '#') {
                                                    ?>
                                                    <tr>
                                                        <td colspan="6"><b><?= $value->menu_name ?></b></td>
                                                    </tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td><b><?= $value->menu_name ?></b></td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_full_access check_all" id="full_access_<?= $value->menu_id ?>" name="full_access[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>">
                                                                    <label class="custom-control-label" for="full_access_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="view_<?= $value->menu_id ?>" name="view[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>">
                                                                    <label class="custom-control-label" for="view_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="add_<?= $value->menu_id ?>" name="add[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>">
                                                                    <label class="custom-control-label" for="add_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="edit_<?= $value->menu_id ?>" name="edit[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>">
                                                                    <label class="custom-control-label" for="edit_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="frame-wrap">
                                                                <div class="custom-control custom-checkbox custom-control-inline custom-switch">
                                                                    <input type="checkbox" class="custom-control-input check_child" id="delete_<?= $value->menu_id ?>" name="delete[<?= $value->menu_id ?>]" data-id="<?= $value->menu_id ?>">
                                                                    <label class="custom-control-label" for="delete_<?= $value->menu_id ?>"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                if (!empty($value->sub_menu)) {
                                                    $data['subMenuData'] = $value->sub_menu;
                                                    echo $this->load->view('admin/master/user/sub_menu_rights', $data, true);
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#plan_type").select2({
            placeholder: "Select plan",
            allowClear: true,
            width: '100%'
        });
        $("#profile_type").select2({
            placeholder: "Select profile type",
            allowClear: true,
            width: '100%'
        });
        $('#addEditUser').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                user_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkUserName') ?>",
                        type: "get"
                    }
                },
                user_email: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkUserEmail') ?>",
                        type: "get"
                    }
                }
            },
            messages: {
                user_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                user_email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });

        $(document).on('change', '.check_full_access', function () {
            var i = $(this).data('id');
            if ($(this).is(':checked')) {
                $("#view_" + i).prop("checked", true);
                $("#add_" + i).prop("checked", true);
                $("#edit_" + i).prop("checked", true);
                $("#delete_" + i).prop("checked", true);
            } else {
                $("#view_" + i).prop("checked", false);
                $("#add_" + i).prop("checked", false);
                $("#edit_" + i).prop("checked", false);
                $("#delete_" + i).prop("checked", false);
            }
        });

        $(document).on('change', '#checkall_side_menu', function () {
            if ($(this).is(':checked')) {
                $(".check_all").prop("checked", true);
                $(".check_child").prop("checked", true);
            } else {
                $(".check_all").prop("checked", false);
                $(".check_child").prop("checked", false);
            }
        });

        $(document).on('change', '.check_child', function () {
            var i = $(this).data('id');
            if ($("#view_" + i).is(':checked') && $("#add_" + i).is(':checked') && $("#edit_" + i).is(':checked') && $("#delete_" + i).is(':checked')) {
                $("#full_access_" + i).prop("checked", true);
            } else {
                $("#full_access_" + i).prop("checked", false);
            }
        });
    });

    $(document).on('change', '#plan_type', function () {
        var id = $(this).val();
        $.ajax({
            type: 'post',
            url: '<?= base_url() ?>admin/Master/allowProfileType',
            data: {id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.allowProfileType == 1) {
                    $('#profile_type').prop("required", true);
                    $('#profile_type_div').css({"display": "block"});
                } else {
                    $('#profile_type').prop("required", false);
                    $('#profile_type_div').css({"display": "none"});
                }
            }
        });
    });
</script>