<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Add Plan
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Master/plan">Plan</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Master/addEditPlan/' . $encrypted_id, $arrayName = array('id' => 'addEditPlan')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="plan_name">Plan Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="plan_name" id="plan_name" placeholder="Plan Name" required value="<?= isset($plan_data->plan_name) && !empty($plan_data->plan_name) ? $plan_data->plan_name : '' ?>">
                                <div class="invalid-feedback">
                                    Plan Name Required / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="plan_price">Plan Price <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="plan_price" id="plan_price" placeholder="Plan Price" required value="<?= isset($plan_data->plan_price) && !empty($plan_data->plan_price) ? $plan_data->plan_price : '' ?>">
                                <div class="invalid-feedback">
                                    Plan Price Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="plan_sale_price">Sale Price</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="plan_sale_price" name="plan_sale_price" placeholder="Sale Price" value="<?= isset($plan_data->plan_sale_price) && !empty($plan_data->plan_sale_price) ? $plan_data->plan_sale_price : '' ?>" <?= isset($plan_data->is_sale) && !empty($plan_data->is_sale) ? ($plan_data->is_sale == 1 ? 'required=""' : '') : '' ?>>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input id="sale_switch" name="sale_switch" type="checkbox" class="custom-control-input" <?= isset($plan_data->is_sale) && !empty($plan_data->is_sale) ? (set_checked($plan_data->is_sale, 1)) : '' ?>>
                                                <label class="custom-control-label fw-500" for="sale_switch">Sale</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="plan_sale_price">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_allow_profile_type" class="custom-control-input" id="is_allow_profile_type" <?= isset($plan_data->is_allow_profile_type) && !empty($plan_data->is_allow_profile_type) ? (set_checked($plan_data->is_allow_profile_type, 1)) : '' ?>>
                                    <label class="custom-control-label" for="is_allow_profile_type">Can Choose Profile Type</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="product_limit">Product Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="product_limit" id="product_limit" placeholder="Product Limit" value="<?= isset($plan_data->product_limit) && !empty($plan_data->product_limit) ? $plan_data->product_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Product Limit Requireds
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address_limit">Address Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="address_limit" id="address_limit" placeholder="Address Limit" value="<?= isset($plan_data->address_limit) && !empty($plan_data->address_limit) ? $plan_data->address_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Address Limit Requireds
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_limit">Service Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="service_limit" id="service_limit" placeholder="Service Limit" value="<?= isset($plan_data->service_limit) && !empty($plan_data->service_limit) ? $plan_data->service_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Service Limit Requireds
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="payment_info_limit">Payment Info Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="payment_info_limit" id="payment_info_limit" placeholder="Payment Info Limit" value="<?= isset($plan_data->payment_info_limit) && !empty($plan_data->payment_info_limit) ? $plan_data->payment_info_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Payment Info Limit Requireds
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="item_limit">Item Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="item_limit" id="item_limit" placeholder="Item Limit" value="<?= isset($plan_data->item_limit) && !empty($plan_data->item_limit) ? $plan_data->item_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Item Limit Requireds
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="gallery_category_limit">Gallery Category Limit</label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="gallery_category_limit" id="gallery_category_limit" placeholder="Gallery category Limit" value="<?= isset($plan_data->gallery_category_limit) && !empty($plan_data->gallery_category_limit) ? $plan_data->gallery_category_limit : '' ?>">
                                <div class="invalid-feedback">
                                    Gallery category Limit Requireds
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_limit">Client Limit</label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control numbersonly" name="client_limit" id="client_limit" placeholder="Client Limit" value="<?= isset($plan_data->client_limit) && !empty($plan_data->client_limit) ? $plan_data->client_limit : '' ?>" <?= isset($plan_data->is_allow_insert_client) && !empty($plan_data->is_allow_insert_client) ? 'required' : '' ?>>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input type="checkbox" name="is_allow_insert_client" class="custom-control-input" id="is_allow_insert_client" <?= isset($plan_data->is_allow_insert_client) && !empty($plan_data->is_allow_insert_client) ? (set_checked($plan_data->is_allow_insert_client, 1)) : '' ?>>
                                                <label class="custom-control-label" for="is_allow_insert_client">Allow Client?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row" id="plan_desc">
                            <?php
                            if (isset($plan_desc_data) && !empty($plan_desc_data)) {
                                foreach ($plan_desc_data as $key3 => $value3) {
                                    $key3++;
                                    ?>
                                    <div class="col-md-6 mb-3 plan_desc_row" id="row_<?= $key3 ?>">
                                        <label class="form-label" for="plan_desc_<?= $key3 ?>">Plan Description</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control address" name="plan_desc[]" id="plan_desc<?= $key3 ?>" placeholder="Plan Description" value="<?= isset($value3->plan_description) && !empty($value3->plan_description) ? $value3->plan_description : '' ?>">
                                            <div class="input-group-append">
                                                <?php if ($key3 == 1) { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary add_plan_desc" title="Add" data-toggle="tooltip">
                                                        <i class="fal fa-plus"></i>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_plan_desc" title="Delete" data-toggle="tooltip" data-id="<?= $key3 ?>">
                                                        <i class="fal fa-minus"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($plan_desc_data) && !empty($plan_desc_data) ? count($plan_desc_data) : 0 ?>';
    var id = '<?= isset($plan_data->plan_id) && !empty($plan_data->plan_id) ? $plan_data->plan_id : '' ?>';
    $(document).ready(function () {
        if (suffix <= 0) {
            plan_desc();
        }
        $('#addEditPlan').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                plan_name: {
                    remote: {
                        url: "<?= base_url('/admin/Master/checkPlanName/') ?>" + id,
                        type: "get"
                    }
                }
            },
            messages: {
                plan_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '.add_plan_desc', function () {
        plan_desc();
    });

    function plan_desc() {
        suffix++;
        var row = '';
        row += '<div class="col-md-6 mb-3 plan_desc_row" id="row_' + suffix + '">';
        row += '<label class="form-label" for="plan_desc_' + suffix + '">Plan Description</label>';
        row += '<div class="input-group">';
        row += '<input type="text" class="form-control address" name="plan_desc[]" id="plan_desc_' + suffix + '" placeholder="Plan Description" value="">';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary add_plan_desc" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_plan_desc" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#plan_desc').append(row);
    }

    $(document).on('click', '.remove_plan_desc', function () {
        var id = $(this).data('id');
        if ($('.plan_desc_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });

    $(document).on('click', '#sale_switch', function () {
        if ($(this).prop("checked") == true) {
            $('#plan_sale_price').prop("required", true);
        } else {
            $('#plan_sale_price').prop("required", false);
        }
    });

    $(document).on('click', '#is_allow_insert_client', function () {
        if ($(this).prop("checked") == true) {
            $('#client_limit').prop("required", true);
        } else {
            $('#client_limit').prop("required", false);
        }
    });
</script>