<style>
    .theme_color_picker input {
        display: none;
    }

    .theme_color_picker .button {
        display: inline-block;
        position: relative;
        width: 50px;
        height: 50px;
        margin: 10px;
        cursor: pointer;
    }

    .theme_color_picker .button span {
        display: block;
        position: absolute;
        width: 50px;
        height: 50px;
        padding: 0;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        border-radius: 100%;
        background: #eeeeee;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
        transition: ease .3s;
    }

    .theme_color_picker .button span:hover {
        padding: 10px;
    }

    .green .button span {
        background: #78cc6d;
    }

    .blue .button span {
        background: #0856c1;
    }

    .orange .button span {
        background: #ff9800;
    }

    .pink .button span {
        background: #ff5e94;
    }

    .purple .button span {
        background: #c446da;
    }

    .red .button span {
        background: #e8676b;
    }

    .checked_color{
        border: 4px solid black;
    }
    .un_checked{
        border: 0px;
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-cog'></i> Edit Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Setting', $arrayName = array('id' => 'addEditSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#general_setting">General</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#social_link">Social</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#vcard">V Card</a></li>
                        </ul>
                        <div class="tab-content py-3">
                            <div class="tab-pane fade show active" id="general_setting" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Cover Photo <i class="text-danger">(File in JPG,PNG) File Size 1920x1280px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="cover_photo" class="custom-file-input image_validation_1mb" id="cover_photo">
                                            <label class="custom-file-label" for="cover_photo">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= file_exists($profile_data->cover_photo) && isset($profile_data->cover_photo) && !empty($profile_data->cover_photo) ? base_url() . $profile_data->cover_photo : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                        <input type="hidden" class="form-control" value="<?= isset($profile_data->cover_photo) && !empty($profile_data->cover_photo) ? $profile_data->cover_photo : '' ?>" name="hidden_cover_photo"/>
                                        <input type="hidden" class="form-control" value="<?= isset($profile_data->cover_compressed_photo) && !empty($profile_data->cover_compressed_photo) ? $profile_data->cover_compressed_photo : '' ?>" name="hidden_cover_new_photo"/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Profile Photo <i class="text-danger">(File in JPG,PNG) File Size 172x172px</i></label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <label class="custom-file-label" for="profile_photo" id="update_profile_pic" data-id="<?= isset($profile_data->profile_id) && !empty($profile_data->profile_id) ? $profile_data->profile_id : '' ?>" data-value="tbl_user_profile">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_profile_photo_active" name="is_profile_photo_active" type="checkbox" class="custom-control-input" <?= isset($profile_data->is_active_profile_photo) && !empty($profile_data->is_active_profile_photo) ? (set_checked($profile_data->is_active_profile_photo, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_profile_photo_active">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-3 profile_photo_preview">
                                        <img src="<?= file_exists($profile_data->profile_photo) && isset($profile_data->profile_photo) && !empty($profile_data->profile_photo) ? base_url() . $profile_data->profile_photo : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                        <input type="hidden" class="form-control" value="<?= isset($profile_data->profile_photo) && !empty($profile_data->profile_photo) ? $profile_data->profile_photo : '' ?>" name="hidden_profile_photo"/>
                                        <input type="hidden" class="form-control" value="<?= isset($profile_data->profile_compressed_photo) && !empty($profile_data->profile_compressed_photo) ? $profile_data->profile_compressed_photo : '' ?>" name="hidden_profile_new_photo"/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="designation">Designation</label>
                                        <div class="input-group">
                                            <input type="text" name="designation" id="designation" class="form-control textonly" value="<?= isset($profile_data->designation) && !empty($profile_data->designation) ? $profile_data->designation : '' ?>" <?= isset($profile_data->is_active_designation) && !empty($profile_data->is_active_designation) ? ($profile_data->is_active_designation == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_designation_active" name="is_designation_active" type="checkbox" class="custom-control-input" <?= isset($profile_data->is_active_designation) && !empty($profile_data->is_active_designation) ? (set_checked($profile_data->is_active_designation, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_designation_active">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (isset($user_data->ref_profile_type) && !empty($user_data->ref_profile_type) && $user_data->ref_profile_type == 1) { ?>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                        </div>
                                    </div>
                                <?php } else if (isset($user_data->ref_profile_type) && !empty($user_data->ref_profile_type) && $user_data->ref_profile_type == 2) { ?>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label">Resume / CV <i class="text-danger">(File in PDF)</i></label>
                                            <div class="custom-file">
                                                <input type="file" name="document" class="custom-file-input image_validation_1mb" id="document">
                                                <label class="custom-file-label" for="document">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-row">
                                    <div class="col-md-12 mb-3 theme_color_picker">
                                        <label class="form-label">Theme Color</label><br>
                                        <label class="green">
                                            <input type="radio" name="theme_color" value="green" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'green') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'green' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                        <label class="blue">
                                            <input type="radio" name="theme_color" value="blue" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'blue') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'blue' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                        <label class="orange">
                                            <input type="radio" name="theme_color" value="orange" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'orange') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'orange' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                        <label class="pink">
                                            <input type="radio" name="theme_color" value="pink" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'pink') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'pink' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                        <label class="purple">
                                            <input type="radio" name="theme_color" value="purple" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'purple') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'purple' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                        <label class="red">
                                            <input type="radio" name="theme_color" value="red" class="change_theme_radio" <?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? set_checked($profile_data->theme_color, 'red') : '' ?>>
                                            <div class="layer"></div>
                                            <div class="button"><span class="<?= isset($profile_data->theme_color) && !empty($profile_data->theme_color) ? ($profile_data->theme_color == 'red' ? 'checked_color' : 'un_checked') : '' ?>"></span></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="social_link" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="whatsapp">Whatsapp Contact Number</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fab fa-whatsapp"></i></span>
                                            </div>
                                            <input type="text" name="whatsapp" id="whatsapp" class="form-control" placeholder="Eg: 919988776655" value="<?= isset($profile_data->whatsapp) && !empty($profile_data->whatsapp) ? str_replace('https://wa.me/', '', $profile_data->whatsapp) : '' ?>" <?= isset($profile_data->is_active_whatsapp) && !empty($profile_data->is_active_whatsapp) ? ($profile_data->is_active_whatsapp == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_whatsapp" name="is_active_whatsapp" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_whatsapp) && !empty($profile_data->is_active_whatsapp) ? (set_checked($profile_data->is_active_whatsapp, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_whatsapp">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="facebook">Facebook</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fab fa-facebook"></i></span>
                                            </div>
                                            <input type="text" name="facebook" id="facebook" class="form-control" value="<?= isset($profile_data->facebook) && !empty($profile_data->facebook) ? $profile_data->facebook : '' ?>" <?= isset($profile_data->is_active_facebook) && !empty($profile_data->is_active_facebook) ? ($profile_data->is_active_facebook == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_facebook" name="is_active_facebook" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_facebook) && !empty($profile_data->is_active_facebook) ? (set_checked($profile_data->is_active_facebook, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_facebook">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="instagram">Instagram</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fab fa-instagram"></i></span>
                                            </div>
                                            <input type="text" name="instagram" id="instagram" class="form-control" value="<?= isset($profile_data->instagram) && !empty($profile_data->instagram) ? $profile_data->instagram : '' ?>" <?= isset($profile_data->is_active_instagram) && !empty($profile_data->is_active_instagram) ? ($profile_data->is_active_instagram == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_instagram" name="is_active_instagram" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_instagram) && !empty($profile_data->is_active_instagram) ? (set_checked($profile_data->is_active_instagram, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_instagram">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="twitter">Twitter</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fab fa-twitter"></i></span>
                                            </div>
                                            <input type="text" name="twitter" id="twitter" class="form-control" value="<?= isset($profile_data->twitter) && !empty($profile_data->twitter) ? $profile_data->twitter : '' ?>" <?= isset($profile_data->is_active_twitter) && !empty($profile_data->is_active_twitter) ? ($profile_data->is_active_twitter == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_twitter" name="is_active_twitter" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_twitter) && !empty($profile_data->is_active_twitter) ? (set_checked($profile_data->is_active_twitter, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_twitter">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="linkedin">linkedin</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                                            </div>
                                            <input type="text" name="linkedin" id="linkedin" class="form-control" value="<?= isset($profile_data->linkedin) && !empty($profile_data->linkedin) ? $profile_data->linkedin : '' ?>" <?= isset($profile_data->is_active_linkedin) && !empty($profile_data->is_active_linkedin) ? ($profile_data->is_active_linkedin == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_linkedin" name="is_active_linkedin" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_linkedin) && !empty($profile_data->is_active_linkedin) ? (set_checked($profile_data->is_active_linkedin, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_linkedin">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="website">Website</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-globe"></i></span>
                                            </div>
                                            <input type="text" name="website" id="website" class="form-control" value="<?= isset($profile_data->website) && !empty($profile_data->website) ? $profile_data->website : '' ?>" <?= isset($profile_data->is_active_website) && !empty($profile_data->is_active_website) ? ($profile_data->is_active_website == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_website" name="is_active_website" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_website) && !empty($profile_data->is_active_website) ? (set_checked($profile_data->is_active_website, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_website">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr style="border-bottom: 1px dashed #886ab5;">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="custom_link_1">Custom Link 1</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-icons"></i></span>
                                            </div>
                                            <input type="text" name="custom_link_1" id="custom_link_1" class="form-control" value="<?= isset($profile_data->custom_link_1) && !empty($profile_data->custom_link_1) ? $profile_data->custom_link_1 : '' ?>" <?= isset($profile_data->is_active_custom_link_1) && !empty($profile_data->is_active_custom_link_1) ? ($profile_data->is_active_custom_link_1 == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_custom_link_1" name="is_active_custom_link_1" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_custom_link_1) && !empty($profile_data->is_active_custom_link_1) ? (set_checked($profile_data->is_active_custom_link_1, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_custom_link_1">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="custom_link_2">Custom Link 2</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-icons"></i></span>
                                            </div>
                                            <input type="text" name="custom_link_2" id="custom_link_2" class="form-control" value="<?= isset($profile_data->custom_link_2) && !empty($profile_data->custom_link_2) ? $profile_data->custom_link_2 : '' ?>" <?= isset($profile_data->is_active_custom_link_2) && !empty($profile_data->is_active_custom_link_2) ? ($profile_data->is_active_custom_link_2 == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_custom_link_2" name="is_active_custom_link_2" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_custom_link_2) && !empty($profile_data->is_active_custom_link_2) ? (set_checked($profile_data->is_active_custom_link_2, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_custom_link_2">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="custom_link_3">Custom Link 3</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-icons"></i></span>
                                            </div>
                                            <input type="text" name="custom_link_3" id="custom_link_3" class="form-control" value="<?= isset($profile_data->custom_link_3) && !empty($profile_data->custom_link_3) ? $profile_data->custom_link_3 : '' ?>" <?= isset($profile_data->is_active_custom_link_3) && !empty($profile_data->is_active_custom_link_3) ? ($profile_data->is_active_custom_link_3 == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_custom_link_3" name="is_active_custom_link_3" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_custom_link_3) && !empty($profile_data->is_active_custom_link_3) ? (set_checked($profile_data->is_active_custom_link_3, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_custom_link_3">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="custom_link_4">Custom Link 4</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-icons"></i></span>
                                            </div>
                                            <input type="text" name="custom_link_4" id="custom_link_4" class="form-control" value="<?= isset($profile_data->custom_link_4) && !empty($profile_data->custom_link_4) ? $profile_data->custom_link_4 : '' ?>" <?= isset($profile_data->is_active_custom_link_4) && !empty($profile_data->is_active_custom_link_4) ? ($profile_data->is_active_custom_link_4 == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_custom_link_4" name="is_active_custom_link_4" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_custom_link_4) && !empty($profile_data->is_active_custom_link_4) ? (set_checked($profile_data->is_active_custom_link_4, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_custom_link_4">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="custom_link_5">Custom Link 5</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fal fa-icons"></i></span>
                                            </div>
                                            <input type="text" name="custom_link_5" id="custom_link_5" class="form-control" value="<?= isset($profile_data->custom_link_5) && !empty($profile_data->custom_link_5) ? $profile_data->custom_link_5 : '' ?>" <?= isset($profile_data->is_active_custom_link_5) && !empty($profile_data->is_active_custom_link_5) ? ($profile_data->is_active_custom_link_5 == 1 ? 'required=""' : '') : '' ?>>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_custom_link_5" name="is_active_custom_link_5" type="checkbox" class="custom-control-input check_box_validation" <?= isset($profile_data->is_active_custom_link_5) && !empty($profile_data->is_active_custom_link_5) ? (set_checked($profile_data->is_active_custom_link_5, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_custom_link_5">Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="vcard" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="vcard_first_name">First Name</label>
                                        <input type="text" name="vcard_first_name" id="vcard_first_name" class="form-control textonly" placeholder="First Name" value="<?= isset($profile_data->vcf_first_name) && !empty($profile_data->vcf_first_name) ? $profile_data->vcf_first_name : '' ?>" required="">
                                        <div class="invalid-feedback">
                                            First Name Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="vcard_last_name">Last Name</label>
                                        <input type="text" name="vcard_last_name" id="vcard_last_name" class="form-control textonly" placeholder="Last Name" value="<?= isset($profile_data->vcf_last_name) && !empty($profile_data->vcf_last_name) ? $profile_data->vcf_last_name : '' ?>" required="">
                                        <div class="invalid-feedback">
                                            Last Name Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        <label class="form-label" for="vcard_country_id">Country Code <span class="text-danger">*</span></label>
                                        <select class="select2 form-control" name="vcard_country_id" id="vcard_country_id" required="">
                                            <option></option>
                                            <?php
                                            if (isset($country_data) && !empty($country_data)) {
                                                foreach ($country_data as $k1 => $v1) {
                                                    ?>
                                                    <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>" <?= isset($profile_data->vcf_country_id) && !empty($profile_data->vcf_country_id) ? set_selected($profile_data->vcf_country_id, $v1->id) : '' ?>><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            Category Required
                                        </div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label" for="vcard_contact_number">Contact Number</label>
                                        <input type="text" name="vcard_contact_number" id="vcard_contact_number" class="form-control numbersonly" placeholder="Contact Number" value="<?= isset($profile_data->vcf_phone) && !empty($profile_data->vcf_phone) ? $profile_data->vcf_phone : '' ?>" required="">
                                        <div class="invalid-feedback">
                                            Contact Number Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="vcard_address">Address</label>
                                        <input type="text" name="vcard_address" id="vcard_address" class="form-control address" placeholder="Address" value="<?= isset($profile_data->vcf_address) && !empty($profile_data->vcf_address) ? $profile_data->vcf_address : '' ?>">
                                        <div class="invalid-feedback">
                                            Address Required
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="vcard_email">Email Address</label>
                                        <input type="email" name="vcard_email" id="vcard_email" class="form-control" placeholder="Email Address" value="<?= isset($profile_data->vcf_email) && !empty($profile_data->vcf_email) ? $profile_data->vcf_email : '' ?>">
                                        <div class="invalid-feedback">
                                            Email Address Required
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="vcard_date_of_birth">Date of Birth</label>
                                        <input type="text" name="vcard_date_of_birth" id="vcard_date_of_birth" class="form-control" placeholder="Date of Birth" readonly="" value="<?= isset($profile_data->vcf_date_of_birth) && !empty($profile_data->vcf_date_of_birth) ? date('d-m-Y', strtotime($profile_data->vcf_date_of_birth)) : '' ?>">
                                        <div class="invalid-feedback">
                                            Date of Birth Required
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="imgCrop" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 1.25rem 1.25rem 0rem 1.25rem;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
            </div>
            <form id="upload-img-frm">
                <div class="modal-body" id="purchaseSaleDetail" style="padding-top: 0rem;">
                    <div style="width:320px;height:320px;margin: 0 auto;">
                        <div id="upload-profile"></div>
                    </div>
                    <div class="custom-file">
                        <input type="file" name="cover_photo" class="custom-file-input image_validation_1mb" id="upload" accept="image/*">
                        <label class="custom-file-label" for="cover_photo">Choose file</label>
                    </div>
                    <input type="hidden" name="upload_image" id="upload_image" value="" />
                    <input type="hidden" name="tbl_id" id="tbl_id" value="" />
                    <input type="hidden" name="tbl_name" id="tbl_name" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary upload-result">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).ready(function () {
        $('#vcard_date_of_birth').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            endDate: "nextday",
            maxDate: today,
            onClose: function () {
                $(this).valid();
            }
        });
        $("#vcard_country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $('#addEditSetting').validate({validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '#is_designation_active', function () {
        if ($(this).prop("checked") == true) {
            $('#designation').prop("required", true);
        } else {
            $('#designation').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_whatsapp', function () {
        if ($(this).prop("checked") == true) {
            $('#whatsapp').prop("required", true);
        } else {
            $('#whatsapp').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_facebook', function () {
        if ($(this).prop("checked") == true) {
            $('#facebook').prop("required", true);
        } else {
            $('#facebook').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_instagram', function () {
        if ($(this).prop("checked") == true) {
            $('#instagram').prop("required", true);
        } else {
            $('#instagram').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_twitter', function () {
        if ($(this).prop("checked") == true) {
            $('#twitter').prop("required", true);
        } else {
            $('#twitter').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_linkedin', function () {
        if ($(this).prop("checked") == true) {
            $('#linkedin').prop("required", true);
        } else {
            $('#linkedin').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_website', function () {
        if ($(this).prop("checked") == true) {
            $('#website').prop("required", true);
        } else {
            $('#website').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_custom_link_1', function () {
        if ($(this).prop("checked") == true) {
            $('#custom_link_1').prop("required", true);
        } else {
            $('#custom_link_1').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_custom_link_2', function () {
        if ($(this).prop("checked") == true) {
            $('#custom_link_2').prop("required", true);
        } else {
            $('#custom_link_2').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_custom_link_3', function () {
        if ($(this).prop("checked") == true) {
            $('#custom_link_3').prop("required", true);
        } else {
            $('#custom_link_3').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_custom_link_4', function () {
        if ($(this).prop("checked") == true) {
            $('#custom_link_4').prop("required", true);
        } else {
            $('#custom_link_4').prop("required", false);
        }
    });
    $(document).on('click', '#is_active_custom_link_5', function () {
        if ($(this).prop("checked") == true) {
            $('#custom_link_5').prop("required", true);
        } else {
            $('#custom_link_5').prop("required", false);
        }
    });

    var limit = 6;
    $('.check_box_validation').on('click', function (evt) {
        var checked = 0;
        $('.check_box_validation').each(function () {
            if ($(this).prop("checked") == true) {
                checked += parseInt(1);
            }
        });
        if (checked > limit) {
            this.checked = false;
            swalWithBootstrapButtons.fire("Alert!", "You can active maximum " + limit + " link.", "error");

        }
    });
    $(document).on('click', '.change_theme_radio', function () {
        $('.change_theme_radio').each(function () {
            if ($(this).is(':checked')) {
                $(this).next().next().find('span').removeClass('un_checked');
                $(this).next().next().find('span').addClass('checked_color');
            } else {
                $(this).next().next().find('span').removeClass('checked_color');
                $(this).next().next().find('span').addClass('un_checked');
            }
        });
    });


    var $uploadCrop;
    var $imageclick = null;
    $(document).on('click', '#update_profile_pic', function () {
        $imageclick = $(this).prev().find('img');
        var id = $(this).data('id');
        var tbl = $(this).data('value');
        console.log(id);
        console.log(tbl);
        $('#imgCrop #tbl_id').val(id);
        $('#imgCrop #tbl_name').val(tbl);
        $uploadCrop = $('#upload-profile').croppie({
            viewport: {
                width: 172,
                height: 172,
                type: 'circle'
            },
            enableExif: true
        });
        $('#imgCrop').modal({show: true, backdrop: 'static', keyboard: false});
    });
    $(document).ready(function () {
        $('#upload').on('change', function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swalWithBootstrapButtons.fire("Alert!", "Sorry - you're browser doesn't support the FileReader API", "error");
            }
        });
    });

    $(document).on('click', '.upload-result', function () {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $('#upload_image').val(resp);
            $.ajax({
                type: "POST",
                data: $('#upload-img-frm').serialize(),
                url: "<?= base_url('admin/customer/Setting/profile/') ?>",
                dataType: "json",
                success: function (_returnData) {
                    $uploadCrop = null;
                    $('#imgCrop').modal('hide');
                    $('#upload-profile').croppie('destroy');
                    $('#upload').val('');
                    $('#upload_image').val('');
                    $('#imgCrop #tbl_id').val('');
                    $('#imgCrop #tbl_name').val('');
                    if (_returnData.res == 'success') {
                        $imageclick.attr('src', _returnData.img);
                        $('.profile_photo_preview').find('img').attr('src', _returnData.img);
                    } else {
                        swalWithBootstrapButtons.fire("Alert!", _returnData.img, "error");
                    }

                },
                error: function (e) {
                    $('#imgCrop').modal('hide');
                    $uploadCrop = null;
                    $('#imgCrop').modal('hide');
                    $('#upload-profile').croppie('destroy');
                    $('#upload').val('');
                    $('#upload_image').val('');
                    $('#imgCrop #tbl_id').val('');
                    $('#imgCrop #tbl_name').val('');
                    swalWithBootstrapButtons.fire("Alert!", "Network Error, Try After Refresh Page", "error");
                }
            });
        });
    });
    $(document).ready(function () {
        $("#imgCrop").on('hidden.bs.modal', function () {
            $uploadCrop = null;
            $('#upload-profile').croppie('destroy');
            $('#upload').val('');
            $('#upload_image').val('');
            $('#imgCrop #tbl_id').val('');
            $('#imgCrop #tbl_name').val('');
        });
    });
</script>