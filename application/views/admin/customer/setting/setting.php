<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-cog'></i> Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Designation</th>
                                    <th>Cover Photo</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($profile_data) && !empty($profile_data)) {
                                    ?>                  
                                    <tr> 
                                        <td><?= $profile_data->designation ?></td>
                                        <td><img src="<?= file_exists($profile_data->cover_photo) && isset($profile_data->cover_photo) && !empty($profile_data->cover_photo) ? base_url() . $profile_data->cover_photo : base_url('assets/admin/img/no_image.jpg') ?>" height="90px" alt="no preview available"></td>
                                        <td>
                                            <div class='d-flex'>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href='<?php echo base_url() ?>admin/customer/Setting/addEditSetting/<?= $profile_data->profile_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                        <i class="fal fa-edit"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>