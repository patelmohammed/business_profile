<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user'></i> Edit Client
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/About/client">Client</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/About/addEditClient/' . $encrypted_id, $arrayName = array('id' => 'addEditClient', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_name">Client Name <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="client_name" id="client_name" placeholder="Client Name" required value="<?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?>">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="is_active_client" class="custom-control-input" id="is_active_client" <?= isset($client_data->is_active_client) && !empty($client_data->is_active_client) ? set_checked($client_data->is_active_client, 1) : '' ?>>
                                                <label class="custom-control-label" for="is_active_client">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">
                                        Client Name Required / Already Exist
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Client Logo <i class="text-danger">(File in JPG,PNG) File Size 160x160px</i></label>
                                <div class="custom-file" id="client_logo_div">
                                    <input type="file" name="client_logo" class="custom-file-input image_validation_1mb" id="client_logo">
                                    <label class="custom-file-label" for="client_logo">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3"></div>
                            <div class="col-md-6 mb-3">
                                <img src="<?= isset($client_data->client_logo) && !empty($client_data->client_logo) && file_exists($client_data->client_logo) ? base_url() . $client_data->client_logo : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($client_data->client_logo) && !empty($client_data->client_logo) ? $client_data->client_logo : '' ?>" name="hidden_client_logo"/>
                                <input type="hidden" class="form-control" value="<?= isset($client_data->compressed_client_logo) && !empty($client_data->compressed_client_logo) ? $client_data->compressed_client_logo : '' ?>" name="hidden_new_client_logo"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    var id = '<?= isset($client_data->client_id) && !empty($client_data->client_id) ? $client_data->client_id : '' ?>';
    $(document).ready(function () {
        $('#addEditClient').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                client_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/About/checkClientName/') ?>" + id,
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                client_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>