<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-images'></i> Add Category
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Gallery">Category</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Gallery/addEditCategory', $arrayName = array('id' => 'addEditCategory', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="gallery_category_name">Category Name <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="gallery_category_name" id="gallery_category_name" placeholder="Category Name" required value="">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                                <label class="custom-control-label" for="is_active">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">
                                        Category Name Required / Already Exist
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $('#addEditCategory').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                gallery_category_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Gallery/checkCategoryName') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                gallery_category_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>