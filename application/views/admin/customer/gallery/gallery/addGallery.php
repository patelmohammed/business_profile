<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user'></i> Add Portfolio / Product
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Gallery/portfolioGallery">Portfolio / Product</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Gallery/addEditPortfolioGallery', $arrayName = array('id' => 'addEditPortfolioGallery', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_gallery_category_id">Category <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="ref_gallery_category_id" id="ref_gallery_category_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($category_data) && !empty($category_data)) {
                                        foreach ($category_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->gallery_category_id) && !empty($v1->gallery_category_id) ? $v1->gallery_category_id : '' ?>"><?= isset($v1->gallery_category_name) && !empty($v1->gallery_category_name) ? $v1->gallery_category_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Category Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="gallery_name">Portfolio / Product Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="gallery_name" id="gallery_name" placeholder="Portfolio / Product Name" required>
                                <div class="invalid-feedback">
                                    Portfolio - Product Names Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG)</i></label>
                                <div class="custom-file">
                                    <input type="file" name="gallery_image" class="custom-file-input image_validation_1mb" id="gallery_image" required="">
                                    <label class="custom-file-label" for="gallery_image">Choose file</label>
                                </div>
                                <div class="invalid-feedback">
                                    Image Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="company_name">Company Name <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="company_name" id="company_name" placeholder="Company Name">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="is_active_company_name" class="custom-control-input" id="is_active_company_name">
                                                <label class="custom-control-label" for="is_active_company_name">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">
                                        Company Names Required
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="gallery_description">Description</label>
                                <textarea class="form-control address js-summernote_description" rows="5" name="gallery_description" id="gallery_description"></textarea>
                                <div class="invalid-feedback">
                                    Gallery Description Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $(".select2").select2({
            placeholder: "Select category",
            allowClear: true,
            width: '100%'
        });
        $('#addEditPortfolioGallery').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                gallery_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Gallery/checkGalleryName') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                gallery_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '#is_active_company_name', function () {
        if ($(this).prop("checked") == true) {
            $('#company_name').prop("required", true);
        } else {
            $('#company_name').prop("required", false);
        }
    });
</script>