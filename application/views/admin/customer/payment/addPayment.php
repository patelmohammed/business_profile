<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-money-bill-wave-alt'></i> Add Payment Information
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Payment">Payment Information</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Payment/addEditPaymentInformation', $arrayName = array('id' => 'addEditPaymentInformation', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="bank_name">Bank Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="bank_name" id="bank_name" placeholder="Bank Name" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="branch_name">Branch Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="branch_name" id="branch_name" placeholder="Branch Name" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="acc_holder_name">Account Holder Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="acc_holder_name" id="acc_holder_name" placeholder="Account Holder Name" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="account_number">Account Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="account_number" id="account_number" placeholder="Account Number" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ifsc_code">IFSC Code <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control address" name="ifsc_code" id="ifsc_code" placeholder="IFSC Code" required>
                            </div>
                            <!--                            <div class="col-md-6 mb-3">
                                                            <label class="form-label" for="copy_detail">Copy Detail <span class="text-danger">*</span></label>
                                                            <input tabindex="2" type="text" class="form-control address" name="copy_detail" id="copy_detail" placeholder="Copy Detail" required>
                                                        </div>-->
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">QR Code <i class="text-danger">(File in JPG,PNG)</i></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="qr_code" class="custom-file-input image_validation_1mb" id="qr_code">
                                        <label class="custom-file-label" for="qr_code">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input id="is_active_qr_code" name="is_active_qr_code" type="checkbox" class="custom-control-input" checked="">
                                                <label class="custom-control-label fw-500" for="is_active_qr_code">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $('#addEditPaymentInformation').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>