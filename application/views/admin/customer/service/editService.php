<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-cog'></i> Edit Service
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Service">Service</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Service/addEditService/' . $encrypted_id, $arrayName = array('id' => 'addEditService')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="service_title">Services Title</label>
                                <input type="text" name="service_title" id="service_title" class="form-control address" value="<?= isset($service_data->service_title) && !empty($service_data->service_title) ? $service_data->service_title : '' ?>">
                            </div>
                        </div>
                        <div id="service_item_div">
                            <?php
                            if (isset($service_item_data) && !empty($service_item_data)) {
                                foreach ($service_item_data as $key => $value) {
                                    $key++;
                                    ?>
                                    <div class="form-row service_item_row" id="row_<?= $key ?>">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="service_item_title_<?= $key ?>">Services Title</label>
                                            <input type="text" name="service_item_title[<?= $key ?>]" id="service_item_title_<?= $key ?>" class="form-control address" value="<?= isset($value->service_item_title) && !empty($value->service_item_title) ? $value->service_item_title : '' ?>" placeholder="Services Title">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="service_item_desc_<?= $key ?>">Services Description</label>
                                            <div class="input-group">
                                                <input type="text" name="service_item_desc[<?= $key ?>]" id="service_item_desc_<?= $key ?>" class="form-control address" value="<?= isset($value->service_item_desc) && !empty($value->service_item_desc) ? $value->service_item_desc : '' ?>" placeholder="Services Description">
                                                <div class="input-group-append">
                                                    <?php if ($key == 1) { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="service_item()" title="Add" data-toggle="tooltip">
                                                            <i class="fal fa-plus"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_service_item" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                            <i class="fal fa-minus"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>

                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_service_item_<?= $key ?>" name="is_active_service_item[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active_service_item) && !empty($value->is_active_service_item) ? (set_checked($value->is_active_service_item, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_service_item_<?= $key ?>"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var limit = <?= getPlanServiceLimit($this->plan_id) ?>;
    var suffix = '<?= isset($service_item_data) && !empty($service_item_data) ? count($service_item_data) : 0 ?>';
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-primary",
            cancelButton: "btn btn-danger mr-2"
        },
        buttonsStyling: false
    });

    $(document).ready(function () {
        if (suffix == 0) {
            service_item();
        }
        $('#addEditService').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function service_item() {
        if ($('.service_item_row').length >= limit) {
            swalWithBootstrapButtons.fire("Alert!", "You can add maximum " + limit + " service.", "error");
            return false;
        }
        suffix++;
        var row = '';
        row += '<div class="form-row service_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="service_item_title_' + suffix + '">Services Title</label>';
        row += '<input type="text" name="service_item_title[' + suffix + ']" id="service_item_title_' + suffix + '" class="form-control" value="" placeholder="Services Title">';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="service_item_desc_' + suffix + '">Services Description</label>';
        row += '<div class="input-group">';
        row += '<input type="text" name="service_item_desc[' + suffix + ']" id="service_item_desc_' + suffix + '" class="form-control" value="" placeholder="Services Description">';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="service_item()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_service_item" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_service_item_' + suffix + '" name="is_active_service_item[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_service_item_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#service_item_div').append(row);
    }

    $(document).on('click', '.remove_service_item', function () {
        var id = $(this).data('id');
        if ($('.service_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>