<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-cog'></i> Service
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Main Service Title</th>
                                    <th>Service Title</th>
                                    <th>Service Description</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($service_data) && !empty($service_data)) {
                                    foreach ($service_data as $key => $value) {
                                        ?>                  
                                        <tr>
                                            <td><?= isset($value->service_title) && !empty($value->service_title) ? $value->service_title : '' ?></td>
                                            <td><?= isset($value->service_item_title) && !empty($value->service_item_title) ? $value->service_item_title : '' ?></td>
                                            <td><?= isset($value->service_item_desc) && !empty($value->service_item_desc) ? $value->service_item_desc : '' ?></td>
                                            <td>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href="javascript:void(0);" data-id="<?= $value->service_item_id ?>" data-url="<?= base_url() ?>admin/customer/Service/activeDeactiveServiceItem" class="activeDeactive" data-is_active="<?= $value->is_active_service_item ?>">
                                                        <?php if ($value->is_active_service_item == 1) { ?>
                                                            <span class="badge badge-success badge-pill">Active</span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger badge-pill">Deactive</span>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/customer/Service/addEditService/<?= isset($value->service_id) && !empty($value->service_id) ? $value->service_id : '' ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>