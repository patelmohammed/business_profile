<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-map'></i> Edit Map
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Contact/map">Map</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Contact/addEditMap/' . $encrypted_id, $arrayName = array('id' => 'addEditMap')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="map">Map Link <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="5" name="map" id="map" required><?= isset($map_data->map) && !empty($map_data->map) ? $map_data->map : '' ?></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($map_data->is_active) && !empty($map_data->is_active) ? set_checked($map_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $('#addEditMap').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>