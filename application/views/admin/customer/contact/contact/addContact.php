<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user'></i> Add Contact
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Contact">Contact</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Contact/addEditContact', $arrayName = array('id' => 'addEditContact')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_name">Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_email">Email Address</label>
                                <input tabindex="2" type="email" class="form-control" name="contact_email" id="contact_email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="country_id">Country Code <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="country_id" id="country_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($country_data) && !empty($country_data)) {
                                        foreach ($country_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->id) && !empty($v1->id) ? $v1->id : '' ?>"><?= isset($v1->name) && !empty($v1->name) ? $v1->name : '' ?><?= isset($v1->phonecode) && !empty($v1->phonecode) ? ' (+' . $v1->phonecode . ')' : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Category Required
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="contact_phone">Contact Phone <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="contact_phone" id="contact_phone" placeholder="Contact Number" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="address_of">Address Of <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="address_of" id="address_of" placeholder="Add of Eg: Home,Office Etc." required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_address">Address <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="5" name="contact_address" id="contact_address" required></textarea>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="map_link">Navigation Map link</label>
                                <input tabindex="2" type="text" class="form-control" name="map_link" id="map_link" placeholder="Navigation Map link">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                    <label class="custom-control-label" for="is_active">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $("#country_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        $('#addEditContact').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                contact_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Contact/checkContactName') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                contact_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>