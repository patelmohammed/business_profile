<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user'></i> Contact
        </h1>
        <div class="d-flex mr-0">
            <?php if (($menu_rights['add_right']) && ((getPlanData($this->plan_id)->address_limit) > count($contact_data))) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Contact/addEditContact">Add Contact</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Address</th>
                                    <th>Email Address</th>
                                    <th>Contact Number</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($contact_data) && !empty($contact_data)) {
                                    $sn = 0;
                                    foreach ($contact_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr>
                                            <td><?= $sn ?></td>
                                            <td><?= $value->contact_address ?></td>
                                            <td><?= $value->contact_email ?></td>
                                            <td><?= $value->contact_phone ?></td>
                                            <td>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href="javascript:void(0);" data-id="<?= $value->contact_id ?>" data-url="<?= base_url() ?>admin/customer/Contact/activeDeactiveContact" class="activeDeactive" data-is_active="<?= $value->is_active ?>">
                                                        <?php if ($value->is_active == 1) { ?>
                                                            <span class="badge badge-success badge-pill">Active</span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger badge-pill">Deactive</span>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/customer/Contact/addEditContact/<?= $value->contact_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/customer/Contact/deleteContact') ?>" data-id="<?= $value->contact_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>