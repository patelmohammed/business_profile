<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-language'></i> Add Language
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/userLanguage">Language</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditUserLanguage', $arrayName = array('id' => 'addEditUserLanguage')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="language_name">Language Name <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="language_name" id="language_name" placeholder="Language Name" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                                <label class="custom-control-label" for="is_active">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="language_per">Language Percentage <span class="text-danger">*</span></label>
                                <input tabindex="2" type="number" max="100" class="form-control numbersonly" name="language_per" id="language_per" placeholder="Language Percentage" required>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $('#addEditUserLanguage').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                language_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Resume/checkUserLanguage') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                language_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>