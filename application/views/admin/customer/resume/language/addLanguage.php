<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-language'></i> Add Language
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/userLanguage">Language</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditUserLanguage', $arrayName = array('id' => 'addEditUserLanguage')) ?>
                    <div class="panel-content" id="language_div">

                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    var suffix = 0;
    $(document).ready(function () {
        language();
        $('#addEditUserLanguage').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                language_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Resume/checkUserLanguage') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                language_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function language() {
        suffix++;
        var row = '';
        row += '<div class="form-row language_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="language_name_' + suffix + '">Language Name <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="language_name[' + suffix + ']" id="language_name_' + suffix + '" placeholder="Language Name" required>';
        row += '<div class="input-group-append">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input type="checkbox" name="is_active[' + suffix + ']" class="custom-control-input" id="is_active_' + suffix + '" checked="">';
        row += '<label class="custom-control-label" for="is_active_' + suffix + '">Active</label>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="language_per_' + suffix + '">Language Percentage <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="number" max="100" class="form-control numbersonly" name="language_per[' + suffix + ']" id="language_per_' + suffix + '" placeholder="Language Percentage" required>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary" onclick="language()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_language_row" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#language_div').append(row);
    }

    $(document).on('click', '.remove_language_row', function () {
        var id = $(this).data('id');
        if ($('.language_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>