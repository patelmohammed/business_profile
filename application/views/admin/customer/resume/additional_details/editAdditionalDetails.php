<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-plus'></i> Edit Additional Details
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/additionalDetails">Additional Details</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditAdditionalDetails/' . $encrypted_id, $arrayName = array('id' => 'addEditAdditionalDetails')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="additional_title">Title <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="additional_title" id="additional_title" placeholder="Title" required value="<?= isset($category_data->additional_title) && !empty($category_data->additional_title) ? $category_data->additional_title : '' ?>">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($category_data->is_active) && !empty($category_data->is_active) ? set_checked($category_data->is_active, 1) : '' ?>>
                                                <label class="custom-control-label" for="is_active">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row" id="resume_desc">
                            <?php
                            if (isset($description_data) && !empty($description_data)) {
                                foreach ($description_data as $key => $value) {
                                    $key++;
                                    ?>
                                    <div class="col-md-6 mb-3 resume_desc_row" id="row_<?= $key ?>">
                                        <label class="form-label" for="additional_description_<?= $key ?>">Description</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control address" name="additional_description[<?= $key ?>]" id="additional_description_<?= $key ?>" placeholder="Description" value="<?= isset($value->additional_description) && !empty($value->additional_description) ? $value->additional_description : '' ?>">
                                            <div class="input-group-append">
                                                <?php if ($key == 1) { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary" onclick="resume_desc()" title="Add" data-toggle="tooltip">
                                                        <i class="fal fa-plus"></i>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_resume_desc" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                        <i class="fal fa-minus"></i>
                                                    </a>
                                                <?php } ?>
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input type="checkbox" name="is_active_additional_description[<?= $key ?>]" class="custom-control-input" id="is_active_additional_description_<?= $key ?>" <?= isset($value->is_active) && !empty($value->is_active) ? set_checked($value->is_active, 1) : '' ?>>
                                                        <label class="custom-control-label" for="is_active_additional_description_<?= $key ?>"></label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    var id = '<?= isset($category_data->additional_title_id) && !empty($category_data->additional_title_id) ? $category_data->additional_title_id : '' ?>';
    var suffix = '<?= isset($description_data) && !empty($description_data) ? count($description_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix <= 0) {
            resume_desc();
        }
        $('#addEditAdditionalDetails').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                additional_title: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Resume/checkAdditionalDetails/') ?>" + id,
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                additional_title: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });


    function resume_desc() {
        suffix++;
        var row = '';
        row += '<div class="col-md-6 mb-3 resume_desc_row" id="row_' + suffix + '">';
        row += '<label class="form-label" for="additional_description_' + suffix + '">Description</label>';
        row += '<div class="input-group">';
        row += '<input type="text" class="form-control address" name="additional_description[' + suffix + ']" id="additional_description_' + suffix + '" placeholder="Description" value="">';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary" onclick="resume_desc()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_resume_desc" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input type="checkbox" name="is_active_additional_description[' + suffix + ']" class="custom-control-input" id="is_active_additional_description_' + suffix + '" checked>';
        row += '<label class="custom-control-label" for="is_active_additional_description_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';

        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#resume_desc').append(row);
    }

    $(document).on('click', '.remove_resume_desc', function () {
        var id = $(this).data('id');
        if ($('.resume_desc_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>