<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-plus'></i> Add Additional Details
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/additionalDetails">Additional Details</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditAdditionalDetails', $arrayName = array('id' => 'addEditAdditionalDetails')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="additional_title">Title <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="additional_title" id="additional_title" placeholder="Title" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                                <label class="custom-control-label" for="is_active">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row" id="resume_desc">

                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    var suffix = 0;
    $(document).ready(function () {
        resume_desc();
        $('#addEditAdditionalDetails').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                additional_title: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Resume/checkAdditionalDetails') ?>",
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                additional_title: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    function resume_desc() {
        suffix++;
        var row = '';
        row += '<div class="col-md-6 mb-3 resume_desc_row" id="row_' + suffix + '">';
        row += '<label class="form-label" for="additional_description_' + suffix + '">Description</label>';
        row += '<div class="input-group">';
        row += '<input type="text" class="form-control address" name="additional_description[' + suffix + ']" id="additional_description_' + suffix + '" placeholder="Description" value="">';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary" onclick="resume_desc()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_resume_desc" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input type="checkbox" name="is_active_additional_description[' + suffix + ']" class="custom-control-input" id="is_active_additional_description_' + suffix + '" checked>';
        row += '<label class="custom-control-label" for="is_active_additional_description_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';

        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#resume_desc').append(row);
    }

    $(document).on('click', '.remove_resume_desc', function () {
        var id = $(this).data('id');
        if ($('.resume_desc_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>