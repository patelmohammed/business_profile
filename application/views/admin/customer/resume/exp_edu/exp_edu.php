<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-graduation-cap'></i> Experience And Education
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/addEditExpEdu">Add Experience And Education</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Experience / Education Period</th>
                                    <th>Designation / Course Name</th>
                                    <th>Company / University Name</th>
                                    <th>Description</th>
                                    <th>Experience Or Education</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($experience_education_data) && !empty($experience_education_data)) {
                                    $sn = 0;
                                    foreach ($experience_education_data as $key => $value) {
                                        $sn++;
                                        ?>                  
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td>
                                                <span class="<?= isset($value->current_status) && !empty($value->current_status) && $value->current_status == 1 ? 'text-primary' : '' ?>"><?= isset($value->period_from) && !empty($value->period_from) ? $value->period_from . (isset($value->current_status) && !empty($value->current_status) && $value->current_status == 1 ? ' - Present' : (isset($value->period_to) && !empty($value->period_to) ? ' - ' . $value->period_to : '')) : '' ?></span>
                                            </td> 
                                            <td><?= $value->designation_course_name ?></td> 
                                            <td><?= $value->company_university_name ?></td> 
                                            <td><?= $value->description ?></td> 
                                            <td><?= $value->is_exp_or_edu ?></td> 
                                            <td>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href="javascript:void(0);" data-id="<?= $value->exp_edu_id ?>" data-url="<?= base_url() ?>admin/customer/Resume/activeDeactiveExpEdu" class="activeDeactive" data-is_active="<?= $value->is_active ?>">
                                                        <?php if ($value->is_active == 1) { ?>
                                                            <span class="badge badge-success badge-pill">Active</span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger badge-pill">Deactive</span>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/customer/Resume/addEditExpEdu/<?= $value->exp_edu_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/customer/Resume/deleteExpEdu') ?>" data-id="<?= $value->exp_edu_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>