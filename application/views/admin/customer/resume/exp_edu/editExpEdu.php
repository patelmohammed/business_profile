<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-graduation-cap'></i> Edit Experience Or Education
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/client">Client</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditExpEdu/' . $encrypted_id, $arrayName = array('id' => 'addEditExpEdu')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="is_experience" name="is_exp_or_edu" required="" value="experience" <?= isset($exp_edu_data->is_exp_or_edu) && !empty($exp_edu_data->is_exp_or_edu) ? set_checked($exp_edu_data->is_exp_or_edu, 'experience') : '' ?>>
                                    <label class="custom-control-label" for="is_experience">Experience</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="is_education" name="is_exp_or_edu" value="education" <?= isset($exp_edu_data->is_exp_or_edu) && !empty($exp_edu_data->is_exp_or_edu) ? set_checked($exp_edu_data->is_exp_or_edu, 'education') : '' ?>>
                                    <label class="custom-control-label" for="is_education">Education</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="period_from">Experience / Education Period From <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control year" name="period_from" id="period_from" placeholder="Experience / Education Period From" required value="<?= isset($exp_edu_data->period_from) && !empty($exp_edu_data->period_from) ? $exp_edu_data->period_from : '' ?>" readonly="">
                                <div class="invalid-feedback">
                                    Experience / Education From Required
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="period_to">Experience / Education To <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control year" name="period_to" id="period_to" placeholder="Experience / Education Period To" value="<?= isset($exp_edu_data->period_to) && !empty($exp_edu_data->period_to) ? $exp_edu_data->period_to : '' ?>" readonly="" <?= isset($exp_edu_data->current_status) && !empty($exp_edu_data->current_status) && $exp_edu_data->current_status == 1 ? 'required' : '' ?>>
                                <div class="invalid-feedback">
                                    Experience / Education To Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="designation_course_name">Designation / Course Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="designation_course_name" id="designation_course_name" placeholder="Designation / Course Name" required value="<?= isset($exp_edu_data->designation_course_name) && !empty($exp_edu_data->designation_course_name) ? $exp_edu_data->designation_course_name : '' ?>">
                                <div class="invalid-feedback">
                                    Designation / Course Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="company_university_name">Company / University Name <span class="text-danger">*</span></label>
                                    <input tabindex="2" type="text" class="form-control textonly" name="company_university_name" id="company_university_name" placeholder="Company / University Name" required value="<?= isset($exp_edu_data->company_university_name) && !empty($exp_edu_data->company_university_name) ? $exp_edu_data->company_university_name : '' ?>">
                                    <div class="invalid-feedback">
                                        Company / University Name Required
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="current_status">&nbsp;</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="current_status" class="custom-control-input" id="current_status" <?= isset($exp_edu_data->current_status) && !empty($exp_edu_data->current_status) ? set_checked($exp_edu_data->current_status, 1) : '' ?>>
                                            <label class="custom-control-label" for="current_status">Currently Student / Education?</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="is_active">&nbsp;</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($exp_edu_data->is_active) && !empty($exp_edu_data->is_active) ? set_checked($exp_edu_data->is_active, 1) : '' ?>>
                                            <label class="custom-control-label" for="is_active">Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="description">Description <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" rows="5" name="description" id="description" required><?= isset($exp_edu_data->description) && !empty($exp_edu_data->description) ? $exp_edu_data->description : '' ?></textarea>
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $('#addEditExpEdu').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '#current_status', function () {
        if ($(this).prop("checked") == true) {
            $('#period_to').prop("required", true);
        } else {
            $('#period_to').prop("required", false);
        }
    });
</script>