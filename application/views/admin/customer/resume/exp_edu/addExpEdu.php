<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-graduation-cap'></i> Add Experience Or Education
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Resume/client">Client</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Resume/addEditExpEdu', $arrayName = array('id' => 'addEditExpEdu')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="is_experience" name="is_exp_or_edu" required="" value="experience">
                                    <label class="custom-control-label" for="is_experience">Experience</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="is_education" name="is_exp_or_edu" value="education">
                                    <label class="custom-control-label" for="is_education">Education</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="period_from">Experience / Education Period From <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control year" name="period_from" id="period_from" placeholder="Experience / Education Period From" required readonly="">
                                <div class="invalid-feedback">
                                    Experience / Education From Required
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="period_to">Experience / Education To <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control year" name="period_to" id="period_to" placeholder="Experience / Education Period To" readonly="">
                                <div class="invalid-feedback">
                                    Experience / Education To Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="designation_course_name">Designation / Course Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control address" name="designation_course_name" id="designation_course_name" placeholder="Designation / Course Name" required>
                                <div class="invalid-feedback">
                                    Designation / Course Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="company_university_name">Company / University Name <span class="text-danger">*</span></label>
                                    <input tabindex="2" type="text" class="form-control address" name="company_university_name" id="company_university_name" placeholder="Company / University Name" required>
                                    <div class="invalid-feedback">
                                        Company / University Name Required
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="current_status">&nbsp;</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="current_status" class="custom-control-input" id="current_status">
                                            <label class="custom-control-label" for="current_status">Current Student / Education?</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="is_active">&nbsp;</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" checked="">
                                            <label class="custom-control-label" for="is_active">Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="description">Description <span class="text-danger">*</span></label>
                                    <textarea class="form-control address" rows="5" name="description" id="description" required></textarea>
                                    <div class="invalid-feedback">
                                        Description Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    $(document).ready(function () {
        $('#addEditExpEdu').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '#current_status', function () {
        if ($(this).prop("checked") == true) {
            $('#period_to').prop("required", true);
        } else {
            $('#period_to').prop("required", false);
        }
    });
</script>