<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-burger-soda'></i> Add Item
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/customer/Gallery/portfolioGallery">Portfolio / Product</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/customer/Menu/addEditItem/' . $encrypted_id, $arrayName = array('id' => 'addEditItem')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_category_id">Category <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="ref_category_id" id="ref_category_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($category_data) && !empty($category_data)) {
                                        foreach ($category_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->category_id) && !empty($v1->category_id) ? $v1->category_id : '' ?>" <?= isset($item_data->ref_category_id) && !empty($item_data->ref_category_id) ? set_selected($item_data->ref_category_id, $v1->category_id) : '' ?>><?= isset($v1->category_name) && !empty($v1->category_name) ? $v1->category_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Category Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="item_name">Item Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="item_name" id="item_name" placeholder="Item Name" required value="<?= isset($item_data->item_name) && !empty($item_data->item_name) ? $item_data->item_name : '' ?>">
                                <div class="invalid-feedback">
                                    Item Name Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="item_price">Price <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control numbersonly" name="item_price" id="item_price" placeholder="Price" required="" value="<?= isset($item_data->item_price) && !empty($item_data->item_price) ? $item_data->item_price : '' ?>">
                                <div class="invalid-feedback">
                                    Price Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="item_sale_price">Sale Price</label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control numbersonly" name="item_sale_price" id="item_sale_price" placeholder="Sale Price" value="<?= isset($item_data->item_sale_price) && !empty($item_data->item_sale_price) ? $item_data->item_sale_price : '' ?>" <?= isset($item_data->is_sale) && !empty($item_data->is_sale) ? 'required' : '' ?>>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="is_sale" class="custom-control-input" id="is_sale" <?= isset($item_data->is_sale) && !empty($item_data->is_sale) ? set_checked($item_data->is_sale, 1) : '' ?>>
                                                <label class="custom-control-label" for="is_sale">Active</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">
                                        Sale Price Required
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="item_description">Description</label>
                                <textarea class="form-control address" rows="5" name="item_description" id="item_description"><?= isset($item_data->item_description) && !empty($item_data->item_description) ? $item_data->item_description : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Gallery Description Required / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($item_data->is_active) && !empty($item_data->is_active) ? set_checked($item_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var user_id = '<?= isset($this->user_id) && !empty($this->user_id) ? $this->user_id : '' ?>';
    var id = '<?= isset($item_data->item_id) && !empty($item_data->item_id) ? $item_data->item_id : '' ?>';
    $(document).ready(function () {
        $(".select2").select2({
            placeholder: "Select menu category",
            allowClear: true,
            width: '100%'
        });
        $('#addEditItem').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                item_name: {
                    remote: {
                        url: "<?= base_url('/admin/customer/Menu/checkItemName/') ?>" + id,
                        type: "get",
                        data: {user_id: function () {
                                return user_id
                            }}
                    }
                }
            },
            messages: {
                item_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });

    $(document).on('click', '#is_sale', function () {
        if ($(this).prop("checked") == true) {
            $('#item_sale_price').prop("required", true);
        } else {
            $('#item_sale_price').prop("required", false);
        }
    });
</script>