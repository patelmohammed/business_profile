<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-user'></i> About
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>About Title</th>
                                    <th>About Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($about_data) && !empty($about_data)) {
                                    ?>                  
                                    <tr>
                                        <td><?= isset($about_data->about_title) && !empty($about_data->about_title) ? $about_data->about_title : '' ?></td>
                                        <td><?= isset($about_data->about_desc) && !empty($about_data->about_desc) ? $about_data->about_desc : '' ?></td>
                                        <td>
                                            <div class='d-flex'>
                                                <?php if ($menu_rights['edit_right']) { ?>
                                                    <a href='<?php echo base_url() ?>admin/customer/About/addEditAbout/<?= isset($about_data->about_id) && !empty($about_data->about_id) ? $about_data->about_id : '' ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                        <i class="fal fa-edit"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>