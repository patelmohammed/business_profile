<aside class="page-sidebar">
    <div class="page-logo">
        <a href="javascript:void(0);" class="page-logo-link press-scale-down d-flex align-items-center position-relative">
            <!--<img src="<?= base_url() ?>assets/admin/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">-->
            <span class="page-logo-text mr-1 text-center">Weborative</span>
            <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
            <!--<i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>-->
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">
        <div class="nav-filter">
            <div class="position-relative">
                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                    <i class="fal fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="info-card">
            <img src="<?= base_url() ?>assets/admin/img/demo/avatars/avatar-admin.png" class="profile-image rounded-circle" alt="Dr. Codex Lantern">
            <div class="info-card-text">
                <a href="#" class="d-flex align-items-center text-white">
                    <span class="text-truncate text-truncate-sm d-inline-block">
                        <?= $this->session->userdata('user_name') ?>
                    </span>
                </a>
                <span class="d-inline-block text-truncate text-truncate-sm"><?= $this->session->userdata('user_name') ?></span>
            </div>
            <img src="<?= base_url() ?>assets/admin/img/card-backgrounds/cover-2-lg.png" class="cover" alt="cover">
        </div>
        <ul id="js-nav-menu" class="nav-menu">
            <li class="<?= $this->page_id == 'DASHBOARD' ? 'active' : '' ?>">
                <a href="<?= base_url() ?>admin/Dashboard" title="Dashboard" data-filter-tags="application intel">
                    <i class="fal fa-info-circle"></i>
                    <span class="nav-link-text" data-i18n="nav.application_intel">Dashboard</span>
                </a>
            </li>
            <li class="<?= ($this->page_id == 'MASTER') && ($this->menu_id == 'USER' || $this->menu_id == 'PLAN') ? 'open active' : '' ?>">
                <a href="javascript:void(0);" title="User" data-filter-tags="user">
                    <i class="fal fa-shopping-bag"></i>
                    <span class="nav-link-text" data-i18n="nav.application_intel">Master</span>
                </a>
                <ul>
                    <li class="<?= $this->menu_id == 'USER' ? 'active' : '' ?>">
                        <a href="<?= base_url() ?>admin/Master/user" title="User">
                            <span class="nav-link-text">User</span>
                        </a>
                    </li>
                    <li class="<?= $this->menu_id == 'PLAN' ? 'active' : '' ?>">
                        <a href="<?= base_url() ?>admin/Master/plan" title="Plan">
                            <span class="nav-link-text">Plan</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="filter-message js-filter-message bg-success-600"></div>
    </nav>
    <!-- END PRIMARY NAVIGATION -->
    <!-- NAV FOOTER -->
    <div class="nav-footer shadow-top">
        <a href="#" onclick="return false;" data-action="toggle" data-class="nav-function-minify" class="hidden-md-down">
            <i class="ni ni-chevron-right"></i>
            <i class="ni ni-chevron-right"></i>
        </a>
        <ul class="list-table m-auto nav-footer-buttons">
            <li>
                <a href="tel:+919033335354" data-toggle="tooltip" data-placement="top" title="Pilot Service">
                    <i class="fal fa-comments"></i>
                </a>
            </li>
            <li>
                <a href="tel:+919723666565" data-toggle="tooltip" data-placement="top" title="Support Call">
                    <i class="fal fa-life-ring"></i>
                </a>
            </li>
            <li>
                <a href="tel:+919723666565" data-toggle="tooltip" data-placement="top" title="Tech Person call">
                    <i class="fal fa-phone"></i>
                </a>
            </li>
        </ul>
    </div>
</aside>
<div class="page-content-wrapper">
    <!-- BEGIN Page Header -->
    <header class="page-header" role="banner">
        <!-- we need this logo when user switches to nav-function-top -->
        <div class="page-logo">
            <a href="#" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
                <img src="<?= base_url() ?>assets/admin/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                <span class="page-logo-text mr-1">SmartAdmin WebApp</span>
                <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
                <i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
            </a>
        </div>
        <!-- DOC: nav menu layout change shortcut -->
        <div class="hidden-md-down dropdown-icon-menu position-relative">
            <a href="#" class="header-btn btn js-waves-off" onclick="changeMenuWidth();" data-action="toggle" data-class="nav-function-hidden" title="Hide Navigation">
                <i class="ni ni-menu"></i>
            </a>
        </div>
        <!-- DOC: mobile button appears during mobile width -->
        <div class="hidden-lg-up">
            <a href="#" class="header-btn btn press-scale-down" data-action="toggle" data-class="mobile-nav-on">
                <i class="ni ni-menu"></i>
            </a>
        </div>
        <div class="ml-auto d-flex">
            <!-- app user menu -->
            <div>
                <a href="#" data-toggle="dropdown" title="drlantern@gotbootstrap.com" class="header-icon d-flex align-items-center justify-content-center ml-2">
                    <img src="<?= base_url() ?>assets/admin/img/demo/avatars/avatar-admin.png" class="profile-image rounded-circle" alt="Dr. Codex Lantern">
                    <!-- you can also add username next to the avatar with the codes below:
                                                        <span class="ml-1 mr-1 text-truncate text-truncate-header hidden-xs-down">Me</span>
                                                        <i class="ni ni-chevron-down hidden-xs-down"></i> -->
                </a>
                <div class="dropdown-menu dropdown-menu-animated dropdown-lg">
                    <div class="dropdown-header bg-trans-gradient d-flex flex-row py-4 rounded-top">
                        <div class="d-flex flex-row align-items-center mt-1 mb-1 color-white">
                            <span class="mr-2">
                                <img src="<?= base_url() ?>assets/admin/img/demo/avatars/avatar-admin.png" class="rounded-circle profile-image" alt="Dr. Codex Lantern">
                            </span>
                            <div class="info-card-text">
                                <div class="fs-lg text-truncate text-truncate-lg"><?= $this->session->userdata('user_name') ?></div>
                                <span class="text-truncate text-truncate-md opacity-80"><?= $this->session->userdata('user_email') ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider m-0"></div>
                    <div class="dropdown-divider m-0"></div>
                    <a href="<?= base_url() ?>admin/Home/addEditProfile" class="dropdown-item">
                        <span data-i18n="drpdwn.settings">Profile</span>
                    </a>
                    <a href="#" class="dropdown-item" data-action="app-fullscreen">
                        <span data-i18n="drpdwn.fullscreen">Fullscreen</span>
                        <i class="float-right text-muted fw-n">F11</i>
                    </a>
                    <div class="dropdown-divider m-0"></div>
                    <a class="dropdown-item fw-500 pt-3 pb-3" href="<?= base_url() ?>admin/Auth/logout">
                        <span data-i18n="drpdwn.page-logout">Logout</span>
                        <span class="float-right fw-n">&commat;<?= $this->session->userdata('user_name') ?></span>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <!-- END Page Header -->