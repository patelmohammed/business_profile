<!-- pricing -->
<section class="section">
    <div class="">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-2">Pricing For You</h2>
                <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    <br> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
            </div>
            <!-- pricing item -->
            <?php echo form_open(base_url() . 'Home/buy', $arrayName = array('id' => 'Buy', 'class' => 'row')) ?>
            <?php
            if (isset($plan_data) && !empty($plan_data)) {
                foreach ($plan_data as $k1 => $v1) {
                    ?>
                    <div class="col-lg-3 col-sm-6 mb-md-50">
                        <div class="rounded border">
                            <div class="p-4 border-bottom">
                                <h3 class="text-color mb-0"><?= isset($v1->plan_name) && !empty($v1->plan_name) ? $v1->plan_name : '' ?></h3>
                                <i class="square-icon translateY-10 float-right ti-gift"></i>
                            </div>
                            <div class="pt-5 px-4">
                                <h2 class="font-weight-bold mb-3">&#8377;<?= isset($v1->plan_price) && !empty($v1->plan_price) ? $v1->plan_price : '' ?><span class="h5">/year</span></h2>
                                <?php
                                if (isset($v1->plan_desc) && !empty($v1->plan_desc)) {
                                    ?>
                                    <ul class="pl-0 mb-30">
                                        <?php
                                        foreach ($v1->plan_desc as $k2 => $v2) {
                                            ?>
                                            <li class="font-secondary text-color py-10 border-bottom">
                                                <i class="text-primary mr-2 ti-arrow-circle-right"></i><?= isset($v2->plan_description) && !empty($v2->plan_description) ? $v2->plan_description : '' ?>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                }
                                ?>
                                <button type="submit" name="plan_type" class="btn btn-primary translateY-25" id="<?= isset($v1->plan_id) && !empty($v1->plan_id) ? $v1->plan_id : '' ?>" value="<?= isset($v1->plan_id) && !empty($v1->plan_id) ? $v1->plan_id : '' ?>">Buy Now</button>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <?= form_close() ?>
            <!-- /pricing item -->
        </div>
    </div>
</section>
<!-- /pricing -->