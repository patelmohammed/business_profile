<?php

use JeroenDesloovere\VCard\VCard;

class Home extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'HOME';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
        $this->load->library('email');
    }

    public function index($user_id = NULL, $name = NULL) {
        if (isset($user_id) && !empty($user_id)) {
            $this->card_user_id = decodeId($user_id);
        } else {
            $this->card_user_id = 1;
        }
        $data = [];
        $view = 'index';
        $this->page_title = 'HOME';
        $data['user_info'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->card_user_id, 'Live');
        $this->profile_type = isset($data['user_info']->ref_profile_type) && !empty($data['user_info']->ref_profile_type) ? $data['user_info']->ref_profile_type : '';
        $data['profile_data'] = $this->Common_model->getDataById2('tbl_user_profile', 'ref_user_id', $this->card_user_id, 'Live');
        $data['about_data'] = $this->Common_model->getDataById2('tbl_about', 'ref_user_id', $this->card_user_id, 'Live');
        $data['gallery_data'] = $this->Home_model->getProductWithCategory($this->card_user_id);
        $data['gallery_data_cnt'] = 0;
        foreach ($data['gallery_data'] as $key => $value) {
            $data['gallery_data_cnt'] += count($value->gallery);
        }

        if ($this->profile_type == 1 || $this->profile_type == 5) {
            $data['service_data'] = $this->Common_model->getDataById2('tbl_user_service', 'ref_user_id', $this->card_user_id, 'Live');
            if (isset($data['service_data']) && !empty($data['service_data'])) {
                $data['service_data']->service_item_data = $this->Home_model->getServiceItemById($data['service_data']->service_id);
            }
        } else if ($this->profile_type == 2) {
            $data['experience_data'] = $this->Home_model->getExpEduData($this->card_user_id, 'experience');
            $data['education_data'] = $this->Home_model->getExpEduData($this->card_user_id, 'education');
            $data['skill_data'] = $this->Common_model->getDataById('tbl_user_technical_skill', 'ref_user_id', $this->card_user_id, 'Live', 'is_active', 1);
            $data['language_data'] = $this->Common_model->getDataById('tbl_user_language', 'ref_user_id', $this->card_user_id, 'Live', 'is_active', 1);
            $data['additional_data'] = $this->Home_model->getResumeData($this->card_user_id);
        }
        $data['client_data'] = $this->Common_model->getDataById('tbl_user_client', 'ref_user_id', $this->card_user_id, 'Live', 'is_active_client', 1);
        $data['contact_data'] = $this->Common_model->getDataById('tbl_contact', 'ref_user_id', $this->card_user_id, 'Live', 'is_active', 1);
        $data['map_data'] = $this->Common_model->getDataById2('tbl_map', 'ref_user_id', $this->card_user_id, 'Live', 'is_active', 1);
        $data['payment_data'] = $this->Common_model->getDataById('tbl_user_payment_info', 'ref_user_id', $this->card_user_id, 'Live', 'is_active', 1);
        $data['card_user_id'] = $this->card_user_id;
        $this->load_view($view, $data, FALSE);
    }

    public function pricing() {
        $data = [];
        $view = 'pricing';
        $this->page_title = 'HOME';
        $data['plan_data'] = $this->Home_model->gePlanData();
        $this->load_view($view, $data);
    }

    public function buy() {
        if ($this->input->post()) {
            $data = [];
            $data['plan_type'] = $this->input->post('plan_type');
            $data['plan_data'] = $this->Home_model->gePlanData();
            $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
            $view = 'sign_up';
            $this->page_title = 'CREATE PROFILE';
            $this->load_view($view, $data);
        } else {
            redirect(base_url('Home/pricing'));
        }
    }

    public function signUp() {
        if ($this->input->post()) {
            $insert_data = $data = [];
            $insert_data['user_name'] = $this->input->post('user_name');
            $insert_data['user_number'] = $this->input->post('contact_number');
            $insert_data['user_email'] = $this->input->post('email_address');
            $insert_data['country_id'] = $this->input->post('country_id');

            $password = '';
            $chk_uniq = FALSE;
            while ($chk_uniq == FALSE) {
                $password = randomPassword();
                $chk_uniq = $this->Common_model->chkUniqueData('tbl_user_info', 'user_password', md5($password), 'Live');
            }

            $insert_data['user_password'] = md5($password);
            $ref_plan_id = $this->input->post('plan_type');
            $insert_data['ref_plan_id'] = $ref_plan_id;
            $insert_data['ref_profile_type'] = $this->input->post('profile_type');
            $insert_data['raw'] = $password;
            $insert_data['user_type'] = 2;
            $insert_data['payment_date'] = date('Y/m/d H:i:s');
            $insert_data['plan_end_date'] = date('Y/m/d H:i:s', strtotime('+1 year'));
            $insert_data['InsUser'] = '';
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
            $id = $this->Common_model->insertInformation($insert_data, 'tbl_user_info');
            if (isset($id) && !empty($id)) {
                $from_email = "inquiry@foodmohalla.in";
                $this->email->from($from_email, COMPANY_NAME);
                $this->email->subject("Login Password");
                $this->email->message("Your login password for " . $insert_data['user_email'] . ' is ' . $password);
                $this->email->to($insert_data['user_email']);
                $this->email->send();

                $this->Common_model->insertCustomerProfile($id);
                $this->Common_model->insertCustomerSideMenuRight($id, $ref_plan_id, $insert_data['ref_profile_type']);
                if ($ref_plan_id == 1 || $ref_plan_id == 2 || $ref_plan_id == 5) {
                    $this->Common_model->insertCustomerServiceResume($id, $insert_data['ref_profile_type']);
                }
                $this->Common_model->insertCustomerAbout($id);
                $this->Common_model->insertContactMap($id);

                redirect(base_url('admin/Auth'));
            } else {
                redirect(base_url('Home/pricing'));
            }
        } else {
            redirect(base_url('Home/pricing'));
        }
    }

    public function contactUs() {
        $data = array();
        if ($this->input->post()) {
            $contact_name = $this->input->post('name');
            $insert_data['contact_us_name'] = isset($contact_name) && !empty($contact_name) ? strip_tags($contact_name) : '';

            $contact_email = $this->input->post('email');
            $insert_data['contact_us_email'] = isset($contact_email) && !empty($contact_email) ? $contact_email : '';

            $contact_message = $this->input->post('message');
            $insert_data['contact_us_message'] = isset($contact_message) && !empty($contact_message) ? strip_tags($contact_message) : '';

            $insert_data['ref_user_id'] = $this->card_user_id;
            $insert_data['InsUser'] = $this->card_user_id;
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

            $user_info = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->card_user_id, 'Live');

            $from_email = 'inquiry@foodmohalla.in';
            $this->email->from($from_email, $user_info->user_name);
            $this->email->subject('Contact');
            $this->email->message('Hi <b>' . $contact_name . '</b><br><br>' . 'Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
            $this->email->to($contact_email);
            if ($this->email->send()) {
                $id = $this->Common_model->insertInformation($insert_data, 'tbl_user_contact_us');

                $this->email->from($from_email, $contact_name);
                $this->email->subject('Contact');
                $this->email->message('Name : ' . $contact_name . '<br>Email : ' . $contact_email . '<br> Messsage : ' . $contact_message);
                $this->email->to($user_info->user_email);
                $this->email->send();

                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function contactVcardExportService($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $vcfCardDetail = $this->Common_model->getUserDataForVCF($user_id);
            require_once 'vendor/Behat-Transliterator/Transliterator.php';
            require_once 'vendor/jeroendesloovere-vcard/VCard.php';
            // define vcard
            $vcardObj = new VCard();

            // add personal data
            if ((isset($vcfCardDetail->vcf_first_name) && !empty(isset($vcfCardDetail->vcf_first_name))) || (isset($vcfCardDetail->vcf_last_name) && !empty($vcfCardDetail->vcf_last_name))) {
                $name = (isset($vcfCardDetail->vcf_first_name) && !empty($vcfCardDetail->vcf_first_name) ? $vcfCardDetail->vcf_first_name : '') . (isset($vcfCardDetail->vcf_last_name) && !empty($vcfCardDetail->vcf_last_name) ? ' - ' . $vcfCardDetail->vcf_last_name : '');
                $vcardObj->addName($name);
            }
            if (isset($vcfCardDetail->vcf_date_of_birth) && !empty($vcfCardDetail->vcf_date_of_birth)) {
                $vcardObj->addBirthday($vcfCardDetail->vcf_date_of_birth);
            }
            if (isset($vcfCardDetail->vcf_email) && !empty($vcfCardDetail->vcf_email)) {
                $vcardObj->addEmail($vcfCardDetail->vcf_email);
            }
            if ((isset($vcfCardDetail->vcf_phone) && !empty($vcfCardDetail->vcf_phone))) {
                $contact_number = (isset($vcfCardDetail->vcf_country_id) && !empty($vcfCardDetail->vcf_country_id) ? '+' . getCountryCode($vcfCardDetail->vcf_country_id) : '') . (isset($vcfCardDetail->vcf_phone) && !empty($vcfCardDetail->vcf_phone) ? $vcfCardDetail->vcf_phone : '');
                $vcardObj->addPhoneNumber($contact_number);
            }
            if (isset($vcfCardDetail->vcf_address) && !empty($vcfCardDetail->vcf_address)) {
                $vcardObj->addAddress($vcfCardDetail->vcf_address);
            }

            return $vcardObj->download();
        }
    }

}
