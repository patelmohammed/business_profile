<?php

class Dashboard extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'DASHBOARD';
        $this->load->model('Dashboard_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
        $this->load->model('Setting_model');
    }

    public function index() {
        $data = [];
        $view = 'admin/home/index';
        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW');
        $this->page_title = 'DASHBOARD';
        $this->load_admin_view($view, $data);
    }

    public function addEditProfile() {
        $this->menu_id = 'PROFILE';
        $id = $this->user_id;
        if ($this->input->post()) {
            $user_info = $this->Setting_model->getUserDataById($this->user_id);
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');
            if ((isset($old_password) && !empty($old_password))) {
                if (md5($old_password) == $user_info->user_password) {
                    if ((isset($new_password) && !empty($new_password)) && (isset($confirm_new_password) && !empty($confirm_new_password))) {
                        if ($new_password == $confirm_new_password) {
                            $insert_data['user_password'] = md5($new_password);
                            $insert_data['raw'] = $new_password;
                        } else {
                            $this->_show_message("New Password And Confirm Password not matched", "error");
                            redirect('admin/Dashboard/addEditProfile');
                        }
                    }
                } else {
                    $this->_show_message("Old Password not matched", "error");
                    redirect('admin/Dashboard/addEditProfile');
                }
            }
            $insert_data['country_id'] = $this->input->post('country_id');
            $insert_data['user_number'] = $this->input->post('user_number');
            $user_name = $this->input->post('user_name');
            $insert_data['user_name'] = $user_name;
            if (isset($user_name) && !empty($user_name)) {
                $user_data['user_name'] = $user_name;
                $this->session->set_userdata($user_data);
            }

            $new_path = 'assets/admin/img/demo/avatars/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['profile_photo']['name']) && isset($_FILES['profile_photo']['name'])) {
                if ($_FILES['profile_photo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('profile_photo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About/aboutUs');
                    } else {
                        $image = $this->upload->data();
                        $profile_photo_url = $new_path . $image['file_name'];

                        $user_data['profile_photo'] = $profile_photo_url;
                        $this->session->set_userdata($user_data);
                    }
                } else {
                    $profile_photo_url = $this->input->post('hidden_profile_photo');
                }
            } else {
                $profile_photo_url = $this->input->post('hidden_profile_photo');
            }
            $insert_data['profile_photo'] = $profile_photo_url;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->_show_message("Your profile changed successfully", "success");
                $this->Common_model->updateInformation2($insert_data, 'user_id', $id, 'tbl_user_info');
            }
            redirect('admin/Dashboard');
        } else {
            if (isset($id) && !empty($id)) {
                $data = [];
                $data['user_data'] = $this->Setting_model->getUserDataById($this->user_id);
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $view = 'admin/home/editProfile';
                $this->page_title = 'PROFILE';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("You cant insert new profile", "error");
                redirect('admin/Dashboard');
            }
        }
    }

}
