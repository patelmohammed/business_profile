<?php

class Gallery extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'PORTFOLIO_PRODUCT';
        $this->load->model('Gallery_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'CATEGORY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CATEGORY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CATEGORY', 'VIEW');
        $data['category_data'] = $this->Common_model->getDataById('tbl_gallery_category', 'ref_user_id', $this->user_id, 'Live');
        $view = 'admin/customer/gallery/category/gallery_category';
        $this->page_title = 'CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function addEditCategory($encrypted_id = "") {
        $this->menu_id = 'CATEGORY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $insert_data['gallery_category_name'] = $this->input->post('gallery_category_name');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $gallery_category = $this->Common_model->getDataById('tbl_gallery_category', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->gallery_category_limit) > count($gallery_category)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $this->Common_model->InsertInformation($insert_data, 'tbl_gallery_category');
                } else {
                    $this->_show_message("You cant insert new category", "error");
                    redirect('admin/customer/Gallery');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'gallery_category_id', $id, 'tbl_gallery_category');
            }
            redirect('admin/customer/Gallery');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $gallery_category = $this->Common_model->getDataById('tbl_gallery_category', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->gallery_category_limit) > count($gallery_category)) {
                    $this->Common_model->check_menu_access('CATEGORY', 'ADD');
                    $data = [];
                    $view = 'admin/customer/gallery/category/addCategory';
                    $this->page_title = 'CATEGORY';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new category", "error");
                    redirect('admin/customer/Gallery');
                }
            } else {
                $this->Common_model->check_menu_access('CATEGORY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['category_data'] = $this->Common_model->getDataById2('tbl_gallery_category', 'gallery_category_id', $id, 'Live');
                $view = 'admin/customer/gallery/category/editCategory';
                $this->page_title = 'CATEGORY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkCategoryName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Gallery_model->getCategoryName($category_id);
            $respone = $this->Gallery_model->checkCategoryName($this->input->get('gallery_category_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Gallery_model->checkCategoryName($this->input->get('gallery_category_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function activeDeactiveCategory() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('gallery_category_id', $id)->update('tbl_gallery_category');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('gallery_category_id', $id)->update('tbl_gallery_category');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function deleteCategory() {
        $this->Common_model->check_menu_access('PORTFOLIO_PRODUCT_GALLERY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('gallery_category_id', $id);
            $this->db->update('tbl_gallery_category');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function portfolioGallery() {
        $data = [];
        $this->menu_id = 'PORTFOLIO_PRODUCT_GALLERY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PORTFOLIO_PRODUCT_GALLERY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PORTFOLIO_PRODUCT_GALLERY', 'VIEW');
        $data['gallery_data'] = $this->Common_model->getDataById('tbl_gallery', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/gallery/gallery/gallery';
        $this->page_title = 'GALLERY';
        $this->load_admin_view($view, $data);
    }

    public function addEditPortfolioGallery($encrypted_id = "") {
        $this->menu_id = 'PORTFOLIO_PRODUCT_GALLERY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $new_path = 'assets/images/gallery/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['gallery_image']['name']) && isset($_FILES['gallery_image']['name'])) {
                if ($_FILES['gallery_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('gallery_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/customer/Gallery/portfolioGallery');
                    } else {
                        $image = $this->upload->data();
                        $gallery_image_url = $new_path . $image['file_name'];
                        $insert_data['file_extension'] = pathinfo($image['file_name'], PATHINFO_EXTENSION);
                    }
                } else {
                    $gallery_image_url = $this->input->post('hidden_gallery_image');
                }
            } else {
                $gallery_image_url = $this->input->post('hidden_gallery_image');
            }
            $insert_data['gallery_image'] = $gallery_image_url;

            $insert_data['ref_gallery_category_id'] = $this->input->post('ref_gallery_category_id');
            $insert_data['gallery_name'] = $this->input->post('gallery_name');
            $insert_data['gallery_description'] = $this->input->post('gallery_description');

            $insert_data['company_name'] = $this->input->post('company_name');
            $insert_data['is_active_company_name'] = $this->input->post('is_active_company_name') == 'on' ? 1 : 0;

            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $gallery_data = $this->Common_model->getDataById('tbl_gallery', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->product_limit) > count($gallery_data)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $this->Common_model->InsertInformation($insert_data, 'tbl_gallery');
                } else {
                    $this->_show_message("You cant insert new portfolio / product detail", "error");
                    redirect('admin/customer/Gallery/portfolioGallery');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'gallery_id', $id, 'tbl_gallery');
            }
            redirect('admin/customer/Gallery/portfolioGallery');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $gallery_data = $this->Common_model->getDataById('tbl_gallery', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->product_limit) > count($gallery_data)) {
                    $this->Common_model->check_menu_access('PORTFOLIO_PRODUCT_GALLERY', 'ADD');
                    $data = [];
                    $data['category_data'] = $this->Common_model->getDataById('tbl_gallery_category', 'ref_user_id', $this->user_id, 'Live', 'is_active', 1);
                    $view = 'admin/customer/gallery/gallery/addGallery';
                    $this->page_title = 'GALLERY';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new portfolio / product detail", "error");
                    redirect('admin/customer/Gallery/portfolioGallery');
                }
            } else {
                $this->Common_model->check_menu_access('PORTFOLIO_PRODUCT_GALLERY', 'EDIT');
                $data = [];
                $data['category_data'] = $this->Common_model->getDataById('tbl_gallery_category', 'ref_user_id', $this->user_id, 'Live', 'is_active', 1);
                $data['encrypted_id'] = $encrypted_id;
                $data['gallery_data'] = $this->Common_model->getDataById2('tbl_gallery', 'gallery_id', $id, 'Live');
                $view = 'admin/customer/gallery/gallery/editdGallery';
                $this->page_title = 'GALLERY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkGalleryName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Gallery_model->getGalleryName($category_id);
            $respone = $this->Gallery_model->checkGalleryName($this->input->get('gallery_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Gallery_model->checkGalleryName($this->input->get('gallery_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deletePortfolioGallery() {
        $this->Common_model->check_menu_access('PORTFOLIO_PRODUCT_GALLERY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('gallery_id', $id);
            $this->db->update('tbl_gallery');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveGallery() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('gallery_id', $id)->update('tbl_gallery');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('gallery_id', $id)->update('tbl_gallery');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
