<?php

class About extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'ABOUT';
        $this->load->model('About_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $encrypted_id = '';
        $this->menu_id = 'ABOUT_ME';
        $about_data = $this->Common_model->getDataById2('tbl_about', 'ref_user_id', $this->user_id, 'Live');
        if (isset($about_data) && !empty($about_data)) {
            $encrypted_id = $about_data->about_id;
        }
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $insert_data['about_title'] = $this->input->post('about_title');
            $insert_data['about_desc'] = $this->input->post('about_desc');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'about_id', $id, 'tbl_about');
            }
            redirect('admin/customer/About');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('ABOUT_ME', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['about_data'] = $this->Common_model->getDataById2('tbl_about', 'about_id', $id, 'Live');
                $view = 'admin/customer/about/editAbout';
                $this->page_title = 'ABOUT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function client() {
        $data = [];
        $this->menu_id = 'ABOUT_CLIENT';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('ABOUT_CLIENT');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('ABOUT_CLIENT', 'VIEW');
        $data['client_data'] = $this->Common_model->getDataById('tbl_user_client', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/client/client';
        $this->page_title = 'ABOUT CLIENT';
        $this->load_admin_view($view, $data);
    }

    public function addEditClient($encrypted_id = "") {
        $this->menu_id = 'ABOUT_CLIENT';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $new_path = 'assets/images/client/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            $new_image_path = 'assets/upload/';
            if (!is_dir($new_image_path)) {
                if (!mkdir($new_image_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['client_logo']['name']) && isset($_FILES['client_logo']['name'])) {
                if ($_FILES['client_logo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('client_logo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About/aboutUs');
                    } else {
                        $image = $this->upload->data();
                        $client_logo_url = $new_path . $image['file_name'];

                        $new_client_logo_url = $new_image_path . $image['file_name'];
                        $this->Common_model->resizeImage($client_logo_url, $new_client_logo_url, 160, 160);
                    }
                } else {
                    $client_logo_url = $this->input->post('hidden_client_logo');
                    $new_client_logo_url = $this->input->post('hidden_new_client_logo');
                }
            } else {
                $client_logo_url = $this->input->post('hidden_client_logo');
                $new_client_logo_url = $this->input->post('hidden_new_client_logo');
            }
            $insert_data['client_logo'] = $client_logo_url;
            $insert_data['compressed_client_logo'] = $new_client_logo_url;

            $insert_data['client_name'] = $this->input->post('client_name');
            $insert_data['is_active_client'] = $this->input->post('is_active_client') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $client_data = $this->Common_model->getDataById('tbl_user_client', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->client_limit) > count($client_data)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_user_client');
                } else {
                    $this->_show_message("You cant insert new client detail", "error");
                    redirect('admin/customer/About/client');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'client_id', $id, 'tbl_user_client');
            }
            redirect('admin/customer/About/client');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $client_data = $this->Common_model->getDataById('tbl_user_client', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->client_limit) > count($client_data)) {
                    $this->Common_model->check_menu_access('ABOUT_CLIENT', 'ADD');
                    $data = [];
                    $view = 'admin/customer/client/addClient';
                    $this->page_title = 'ABOUT CLIENT';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new client detail", "error");
                    redirect('admin/customer/About/client');
                }
            } else {
                $this->Common_model->check_menu_access('ABOUT_CLIENT', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['client_data'] = $this->Common_model->getDataById2('tbl_user_client', 'client_id', $id, 'Live');
                $view = 'admin/customer/client/editClient';
                $this->page_title = 'ABOUT CLIENT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkClientName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->About_model->getClientName($category_id);
            $respone = $this->About_model->checkClientName($this->input->get('client_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->About_model->checkClientName($this->input->get('client_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteClient() {
        $this->Common_model->check_menu_access('ABOUT_CLIENT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('client_id', $id);
            $this->db->update('tbl_user_client');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveClient() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active_client', 1)->where('client_id', $id)->update('tbl_user_client');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active_client', 0)->where('client_id', $id)->update('tbl_user_client');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
