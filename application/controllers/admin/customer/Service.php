<?php

class Service extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'SERVICE';
        $this->load->model('Service_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'SERVICE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('SERVICE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('SERVICE', 'VIEW');
        $data['service_data'] = $this->Service_model->getServiceById($this->user_id);
        $view = 'admin/customer/service/service';
        $this->page_title = 'SERVICE';
        $this->load_admin_view($view, $data);
    }

    public function addEditService($encrypted_id = "") {
        $this->menu_id = 'SERVICE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['service_title'] = $this->input->post('service_title');
            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'service_id', $id, 'tbl_user_service');
                $this->Common_model->deleteRecord($id, 'tbl_user_service_item', 'ref_service_id');
                $this->Service_model->insertServiceItem($id);
            }
            redirect('admin/customer/Service');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SERVICE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['service_data'] = $this->Common_model->getDataById2('tbl_user_service', 'service_id', $id, 'Live');
                $data['service_item_data'] = $this->Service_model->getServiceItemById($id);
                $view = 'admin/customer/service/editService';
                $this->page_title = 'SERVICE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function activeDeactiveServiceItem() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active_service_item', 1)->where('service_item_id', $id)->update('tbl_user_service_item');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active_service_item', 0)->where('service_item_id', $id)->update('tbl_user_service_item');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
