<?php

class Contact extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'CONTACT';
        $this->load->model('Contact_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'CONTACT_PAGE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CONTACT_PAGE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CONTACT_PAGE', 'VIEW');
        $data['contact_data'] = $this->Common_model->getDataById('tbl_contact', 'ref_user_id', $this->user_id, 'Live');
        $view = 'admin/customer/contact/contact/contact';
        $this->page_title = 'CONTACT';
        $this->load_admin_view($view, $data);
    }

    public function addEditContact($encrypted_id = "") {
        $this->menu_id = 'CONTACT_PAGE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $insert_data['address_of'] = $this->input->post('address_of');
            $insert_data['contact_name'] = $this->input->post('contact_name');
            $insert_data['contact_email'] = $this->input->post('contact_email');
            $insert_data['ref_country_id'] = $this->input->post('country_id');
            $insert_data['contact_phone'] = $this->input->post('contact_phone');
            $insert_data['contact_address'] = $this->input->post('contact_address');
            $insert_data['map_link'] = $this->input->post('map_link');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $contact_data = $this->Common_model->getDataById('tbl_contact', 'ref_user_id', $this->user_id, 'Live');
                if ((getPlanData($this->plan_id)->address_limit) > count($contact_data)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_contact');
                } else {
                    $this->_show_message("You cant insert new contact detail", "error");
                    redirect('admin/customer/Contact');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'contact_id', $id, 'tbl_contact');
            }
            redirect('admin/customer/Contact');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $contact_data = $this->Common_model->getDataById('tbl_contact', 'ref_user_id', $this->user_id, 'Live');
                if ((getPlanData($this->plan_id)->address_limit) > count($contact_data)) {
                    $this->Common_model->check_menu_access('CONTACT', 'ADD');
                    $data = [];
                    $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                    $view = 'admin/customer/contact/contact/addContact';
                    $this->page_title = 'CONTACT';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new contact detail", "error");
                    redirect('admin/customer/Contact');
                }
            } else {
                $this->Common_model->check_menu_access('CONTACT', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $data['contact_data'] = $this->Common_model->getDataById2('tbl_contact', 'contact_id', $id, 'Live');
                $view = 'admin/customer/contact/contact/editContact';
                $this->page_title = 'CONTACT';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkContactName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Contact_model->getContactName($category_id);
            $respone = $this->Contact_model->checkContactName($this->input->get('contact_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Contact_model->checkContactName($this->input->get('contact_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function activeDeactiveContact() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('contact_id', $id)->update('tbl_contact');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('contact_id', $id)->update('tbl_contact');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function deleteContact() {
        $this->Common_model->check_menu_access('CONTACT', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('contact_id', $id);
            $this->db->update('tbl_contact');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function map() {
        $data = [];
        $this->menu_id = 'CONTACT_MAP';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('CONTACT_MAP');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('CONTACT_MAP', 'VIEW');
        $data['map_data'] = $this->Common_model->getDataById2('tbl_map', 'ref_user_id', $this->user_id, 'Live');
        $view = 'admin/customer/contact/map/map';
        $this->page_title = 'MAP';
        $this->load_admin_view($view, $data);
    }

    public function addEditMap($encrypted_id = "") {
        $this->menu_id = 'CONTACT_MAP';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['map'] = $this->input->post('map');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'map_id', $id, 'tbl_map');
            }
            redirect('admin/customer/Contact/map');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('CONTACT_MAP', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['map_data'] = $this->Common_model->getDataById2('tbl_map', 'map_id', $id, 'Live');
                $view = 'admin/customer/contact/map/editMap';
                $this->page_title = 'MAP';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("You cant insert new map detail", "error");
                redirect('admin/customer/Contact/map');
            }
        }
    }

}
