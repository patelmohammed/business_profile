<?php

class Setting extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'SETTING';
        $this->load->model('Setting_model');
        $this->load->model('Common_model');
        $this->load->model('Resume_model');
    }

    public function index() {
        $profile_data = $this->Common_model->getDataById2('tbl_user_profile', 'ref_user_id', $this->user_id, 'Live');
        $this->menu_id = 'SETTING';
        $encrypted_id = $profile_data->profile_id;
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $new_path = 'assets/images/vcard/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            $new_image_path = 'assets/upload/';
            if (!is_dir($new_image_path)) {
                if (!mkdir($new_image_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['cover_photo']['name']) && isset($_FILES['cover_photo']['name'])) {
                if ($_FILES['cover_photo']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('cover_photo')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About/aboutUs');
                    } else {
                        $image = $this->upload->data();
                        $cover_photo_url = $new_path . $image['file_name'];

                        $new_cover_photo_url = $new_image_path . $image['file_name'];
                        $this->Common_model->resizeImage($cover_photo_url, $new_cover_photo_url, 1920, 1280);
                    }
                } else {
                    $cover_photo_url = $this->input->post('hidden_cover_photo');
                    $new_cover_photo_url = $this->input->post('hidden_cover_new_photo');
                }
            } else {
                $cover_photo_url = $this->input->post('hidden_cover_photo');
                $new_cover_photo_url = $this->input->post('hidden_cover_new_photo');
            }
            $insert_data['cover_photo'] = $cover_photo_url;
            $insert_data['cover_compressed_photo'] = $new_cover_photo_url;

            $insert_data['is_active_profile_photo'] = $this->input->post('is_profile_photo_active') == 'on' ? 1 : 0;
//            if (!empty($_FILES['profile_photo']['name']) && isset($_FILES['profile_photo']['name'])) {
//                if ($_FILES['profile_photo']['name']) {
//                    $config['upload_path'] = $new_path;
//                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
//                    $config['max_size'] = "*";
//                    $config['max_width'] = "*";
//                    $config['max_height'] = "*";
//                    $config['encrypt_name'] = FALSE;
//
//                    $this->load->library('upload', $config);
//                    if (!$this->upload->do_upload('profile_photo')) {
//                        $error = array('error' => $this->upload->display_errors());
//                        $this->_show_message("Somethig wrong", "error");
//                        redirect('admin/About/aboutUs');
//                    } else {
//                        $image = $this->upload->data();
//                        $profile_photo_url = $new_path . $image['file_name'];
//
//                        $new_profile_photo_url = $new_image_path . $image['file_name'];
//                        $this->Common_model->resizeImage($profile_photo_url, $new_profile_photo_url, 172, 172);
//                    }
//                } else {
//                    $profile_photo_url = $this->input->post('hidden_profile_photo');
//                    $new_profile_photo_url = $this->input->post('hidden_profile_new_photo');
//                }
//            } else {
//                $profile_photo_url = $this->input->post('hidden_profile_photo');
//                $new_profile_photo_url = $this->input->post('hidden_profile_new_photo');
//            }
//            $insert_data['profile_photo'] = $profile_photo_url;
//            $insert_data['profile_compressed_photo'] = $new_profile_photo_url;

            if (!empty($_FILES['document']['name']) && isset($_FILES['document']['name'])) {
                if ($_FILES['document']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'pdf|PDF|docs|DOCS';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('document')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About/aboutUs');
                    } else {
                        $image = $this->upload->data();
                        $document_url = $new_path . $image['file_name'];
                    }
                } else {
                    $document_url = $this->input->post('hidden_document');
                }
            } else {
                $document_url = $this->input->post('hidden_document');
            }
            $insert_data['document'] = $document_url;

            $insert_data['is_active_designation'] = $this->input->post('is_designation_active') == 'on' ? 1 : 0;
            $insert_data['designation'] = $this->input->post('designation');

            $insert_data['is_active_whatsapp'] = $this->input->post('is_active_whatsapp') == 'on' ? 1 : 0;
            $whatsapp = $this->input->post('whatsapp');
            if (isset($whatsapp) && !empty($whatsapp)) {
                $insert_data['whatsapp'] = 'https://wa.me/' . $whatsapp;
            }

            $insert_data['is_active_facebook'] = $this->input->post('is_active_facebook') == 'on' ? 1 : 0;
            $insert_data['facebook'] = $this->input->post('facebook');

            $insert_data['is_active_instagram'] = $this->input->post('is_active_instagram') == 'on' ? 1 : 0;
            $insert_data['instagram'] = $this->input->post('instagram');


            $insert_data['is_active_twitter'] = $this->input->post('is_active_twitter') == 'on' ? 1 : 0;
            $insert_data['twitter'] = $this->input->post('twitter');

            $insert_data['is_active_linkedin'] = $this->input->post('is_active_linkedin') == 'on' ? 1 : 0;
            $insert_data['linkedin'] = $this->input->post('linkedin');

            $insert_data['is_active_website'] = $this->input->post('is_active_website') == 'on' ? 1 : 0;
            $insert_data['website'] = $this->input->post('website');

            $insert_data['is_active_custom_link_1'] = $this->input->post('is_active_custom_link_1') == 'on' ? 1 : 0;
            $insert_data['custom_link_1'] = $this->input->post('custom_link_1');

            $insert_data['is_active_custom_link_2'] = $this->input->post('is_active_custom_link_2') == 'on' ? 1 : 0;
            $insert_data['custom_link_2'] = $this->input->post('custom_link_2');

            $insert_data['is_active_custom_link_3'] = $this->input->post('is_active_custom_link_3') == 'on' ? 1 : 0;
            $insert_data['custom_link_3'] = $this->input->post('custom_link_3');

            $insert_data['is_active_custom_link_4'] = $this->input->post('is_active_custom_link_4') == 'on' ? 1 : 0;
            $insert_data['custom_link_4'] = $this->input->post('custom_link_4');

            $insert_data['is_active_custom_link_5'] = $this->input->post('is_active_custom_link_5') == 'on' ? 1 : 0;
            $insert_data['custom_link_5'] = $this->input->post('custom_link_5');

            $insert_data['theme_color'] = $this->input->post('theme_color');

            $insert_data['vcf_first_name'] = $this->input->post('vcard_first_name');
            $insert_data['vcf_last_name'] = $this->input->post('vcard_last_name');
            $vcard_date_of_birth = $this->input->post('vcard_date_of_birth');
            $insert_data['vcf_date_of_birth'] = (isset($vcard_date_of_birth) && !empty($vcard_date_of_birth) ? date('Y-m-d', strtotime($vcard_date_of_birth)) : NULL);
            $insert_data['vcf_email'] = $this->input->post('vcard_email');
            $insert_data['vcf_country_id'] = $this->input->post('vcard_country_id');
            $insert_data['vcf_phone'] = $this->input->post('vcard_contact_number');
            $insert_data['vcf_address'] = $this->input->post('vcard_address');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'profile_id', $id, 'tbl_user_profile');
            }
            redirect('admin/customer/Setting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SETTING', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['profile_data'] = $this->Common_model->getDataById2('tbl_user_profile', 'profile_id', $id, 'Live');
                $data['user_data'] = $this->Setting_model->getUserDataById($this->user_id);
                $data['country_data'] = $this->Common_model->getDataById('tbl_country', 'del_status', 'Live', 'Live');
                $view = 'admin/customer/setting/editSetting';
                $this->page_title = 'SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

//    public function addEditSetting() {
//        $data = [];
//        $this->menu_id = 'SETTING';
//        $data['menu_rights'] = $this->Common_model->get_menu_rights('SETTING');
//        if (empty($data['menu_rights'])) {
//            redirect('admin/Auth/Unauthorized');
//        }
//        $this->Common_model->check_menu_access('SETTING', 'VIEW');
//        $data['profile_data'] = $this->Common_model->getDataById2('tbl_user_profile', 'ref_user_id', $this->user_id, 'Live');
//        $view = 'admin/customer/setting/setting';
//        $this->page_title = 'SETTING';
//        $this->load_admin_view($view, $data);
//    }

    public function profile() {
        $new_path = 'assets/images/vcard/';
        if (!is_dir($new_path)) {
            if (!mkdir($new_path, 0777, true)) {
                die('Not Created');
            }
        }
        if ($this->input->post()) {
            $id = $this->input->post('tbl_id');
            $tbl = $this->input->post('tbl_name');
            $img = $this->input->post('upload_image');
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $datas = base64_decode($img);
            $name = md5($id) . $tbl . '.png';
            $file = $new_path . $name;
            $success = file_put_contents($file, $datas);
            if ($success) {
                $insert_data['profile_photo'] = $file;
                $insert_data['profile_compressed_photo'] = $file;
                $this->Common_model->updateInformation2($insert_data, 'profile_id', $id, $tbl);
                echo json_encode(['res' => 'success', 'img' => base_url($file . '?_t=' . time())]);
                die;
            } else {
                echo json_encode(['res' => 'error', 'img' => 'Something Went Wrong With Server, Try Again']);
                die;
            }
        }
        echo json_encode(['res' => 'error', 'img' => 'Wrong Method Try Again']);
        die;
    }

}
