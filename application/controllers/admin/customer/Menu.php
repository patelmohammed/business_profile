<?php

class Menu extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'MENU';
        $this->load->model('Menu_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'MENU_CATEGORY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MENU_CATEGORY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MENU_CATEGORY', 'VIEW');
        $data['category_data'] = $this->Common_model->getDataById('tbl_menu_category', 'ref_user_id', $this->user_id, 'Live');
        $view = 'admin/customer/menu/category/menu_category';
        $this->page_title = 'MENU CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function addEditMenuCategory($encrypted_id = "") {
        $this->menu_id = 'MENU_CATEGORY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $insert_data['category_name'] = $this->input->post('category_name');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['ref_user_id'] = $this->user_id;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_menu_category');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'category_id', $id, 'tbl_menu_category');
            }
            redirect('admin/customer/Menu');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('MENU_CATEGORY', 'ADD');
                $data = [];
                $view = 'admin/customer/menu/category/addCategory';
                $this->page_title = 'MENU CATEGORY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('MENU_CATEGORY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['category_data'] = $this->Common_model->getDataById2('tbl_menu_category', 'category_id', $id, 'Live');
                $view = 'admin/customer/menu/category/editCategory';
                $this->page_title = 'MENU CATEGORY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkMenuCategoryName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Menu_model->getMenuCategoryName($category_id);
            $respone = $this->Menu_model->checkMenuCategoryName($this->input->get('category_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Menu_model->checkMenuCategoryName($this->input->get('category_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteMenuGallery() {
        $this->Common_model->check_menu_access('MENU_CATEGORY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('category_id', $id);
            $this->db->update('tbl_menu_category');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveCategory() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('category_id', $id)->update('tbl_menu_category');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('category_id', $id)->update('tbl_menu_category');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function item() {
        $data = [];
        $this->menu_id = 'MENU_ITEM';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('MENU_ITEM');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('MENU_ITEM', 'VIEW');
        $data['item_data'] = $this->Common_model->getDataById('tbl_menu_item', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/menu/item/item';
        $this->page_title = 'MENU CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function addEditItem($encrypted_id = "") {
        $this->menu_id = 'MENU_ITEM';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $insert_data['ref_category_id'] = $this->input->post('ref_category_id');
            $insert_data['item_name'] = $this->input->post('item_name');
            $insert_data['item_price'] = $this->input->post('item_price');

            $insert_data['item_sale_price'] = $this->input->post('item_sale_price');
            $insert_data['is_sale'] = $this->input->post('is_sale') == 'on' ? 1 : 0;

            $insert_data['item_description'] = $this->input->post('item_description');

            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $item_data = $this->Common_model->getDataById('tbl_menu_item', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->item_limit) > count($item_data)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $this->Common_model->InsertInformation($insert_data, 'tbl_menu_item');
                } else {
                    $this->_show_message("You cant insert new Item", "error");
                    redirect('admin/customer/Menu/item');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'item_id', $id, 'tbl_menu_item');
            }
            redirect('admin/customer/Menu/item');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $item_data = $this->Common_model->getDataById('tbl_menu_item', 'ref_user_id', $this->user_id);
                if ((getPlanData($this->plan_id)->item_limit) > count($item_data)) {
                    $this->Common_model->check_menu_access('MENU_ITEM', 'ADD');
                    $data = [];
                    $data['category_data'] = $this->Common_model->getDataById('tbl_menu_category', 'ref_user_id', $this->user_id, 'Live', 'is_active', 1);
                    $view = 'admin/customer/menu/item/addItem';
                    $this->page_title = 'MENU ITEM';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new Item", "error");
                    redirect('admin/customer/Menu/item');
                }
            } else {
                $this->Common_model->check_menu_access('MENU_ITEM', 'EDIT');
                $data = [];
                $data['category_data'] = $this->Common_model->getDataById('tbl_menu_category', 'ref_user_id', $this->user_id, 'Live', 'is_active', 1);
                $data['encrypted_id'] = $encrypted_id;
                $data['item_data'] = $this->Common_model->getDataById2('tbl_menu_item', 'item_id', $id, 'Live');
                $view = 'admin/customer/menu/item/editItem';
                $this->page_title = 'MENU ITEM';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkItemName($item_id = '') {
        $user_id = $this->input->get('user_id');
        if ($item_id != '') {
            $item_name = $this->Menu_model->getItemName($item_id);
            $respone = $this->Menu_model->checkItemName($this->input->get('item_name'), $user_id, $item_name);
            echo $respone;
            die;
        } else {
            $response = $this->Menu_model->checkItemName($this->input->get('item_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteMenuItem() {
        $this->Common_model->check_menu_access('MENU_ITEM', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('item_id', $id);
            $this->db->update('tbl_menu_item');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveItem() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('item_id', $id)->update('tbl_menu_item');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('item_id', $id)->update('tbl_menu_item');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
