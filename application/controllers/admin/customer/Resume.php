<?php

class Resume extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'RESUME';
        $this->load->model('Resume_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'EXPERIENCE_EDUCATION';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('EXPERIENCE_EDUCATION');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('EXPERIENCE_EDUCATION', 'VIEW');
        $data['experience_education_data'] = $this->Common_model->getDataById('tbl_experience_education', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/resume/exp_edu/exp_edu';
        $this->page_title = 'EXPERIENCE EDUCATION';
        $this->load_admin_view($view, $data);
    }

    public function addEditExpEdu($encrypted_id = "") {
        $this->menu_id = 'EXPERIENCE_EDUCATION';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['is_exp_or_edu'] = $this->input->post('is_exp_or_edu');
            $insert_data['period_from'] = $this->input->post('period_from');
            $insert_data['period_to'] = $this->input->post('period_to');
            $insert_data['designation_course_name'] = $this->input->post('designation_course_name');
            $insert_data['company_university_name'] = $this->input->post('company_university_name');
            $insert_data['description'] = $this->input->post('description');
            $insert_data['current_status'] = $this->input->post('current_status') == 'on' ? 1 : 0;
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['ref_user_id'] = $this->user_id;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->InsertInformation($insert_data, 'tbl_experience_education');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'exp_edu_id', $id, 'tbl_experience_education');
            }
            redirect('admin/customer/Resume');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('EXPERIENCE_EDUCATION', 'ADD');
                $data = [];
                $view = 'admin/customer/resume/exp_edu/addExpEdu';
                $this->page_title = 'EXPERIENCE EDUCATION';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('EXPERIENCE_EDUCATION', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['exp_edu_data'] = $this->Common_model->getDataById2('tbl_experience_education', 'exp_edu_id', $id, 'Live');
                $view = 'admin/customer/resume/exp_edu/editExpEdu';
                $this->page_title = 'EXPERIENCE EDUCATION';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteExpEdu() {
        $this->Common_model->check_menu_access('EXPERIENCE_EDUCATION', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('exp_edu_id', $id);
            $this->db->update('tbl_experience_education');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveExpEdu() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('exp_edu_id', $id)->update('tbl_experience_education');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('exp_edu_id', $id)->update('tbl_experience_education');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function technicalSkill() {
        $data = [];
        $this->menu_id = 'TECHNICAL_SKILL';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('TECHNICAL_SKILL');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('TECHNICAL_SKILL', 'VIEW');
        $data['skill_data'] = $this->Common_model->getDataById('tbl_user_technical_skill', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/resume/technical_skill/technical_skill';
        $this->page_title = 'EXPERIENCE EDUCATION';
        $this->load_admin_view($view, $data);
    }

    public function addEditTechnicalSkill($encrypted_id = "") {
        $this->menu_id = 'TECHNICAL_SKILL';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['skill_name'] = $this->input->post('skill_name');
            $insert_data['skill_per'] = $this->input->post('skill_per');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['ref_user_id'] = $this->user_id;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->InsertInformation($insert_data, 'tbl_user_technical_skill');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'user_skill_id', $id, 'tbl_user_technical_skill');
            }
            redirect('admin/customer/Resume/technicalSkill');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('TECHNICAL_SKILL', 'ADD');
                $data = [];
                $view = 'admin/customer/resume/technical_skill/addTechnicalSkill';
                $this->page_title = 'EXPERIENCE EDUCATION';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('TECHNICAL_SKILL', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['skill_data'] = $this->Common_model->getDataById2('tbl_user_technical_skill', 'user_skill_id', $id, 'Live');
                $view = 'admin/customer/resume/technical_skill/editTechnicalSkill';
                $this->page_title = 'EXPERIENCE EDUCATION';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkSkillName($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Resume_model->getSkillName($category_id);
            $respone = $this->Resume_model->checkSkillName($this->input->get('skill_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Resume_model->checkSkillName($this->input->get('skill_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteTechnicalSkill() {
        $this->Common_model->check_menu_access('TECHNICAL_SKILL', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('user_skill_id', $id);
            $this->db->update('tbl_user_technical_skill');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveSkill() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('user_skill_id', $id)->update('tbl_user_technical_skill');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('user_skill_id', $id)->update('tbl_user_technical_skill');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function userLanguage() {
        $data = [];
        $this->menu_id = 'USER_LANGUAGE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('USER_LANGUAGE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('USER_LANGUAGE', 'VIEW');
        $data['language_data'] = $this->Common_model->getDataById('tbl_user_language', 'ref_user_id', $this->user_id);
        $view = 'admin/customer/resume/language/language';
        $this->page_title = 'LANGUAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditUserLanguage($encrypted_id = "") {
        $this->menu_id = 'USER_LANGUAGE';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            if ($id == "" || $id == '' || $id == NULL) {
                $language_name = $this->input->post('language_name');
                $language_per = $this->input->post('language_per');
                $is_active = $this->input->post('is_active');
                if (isset($language_name) && !empty($language_name)) {
                    foreach ($language_name as $key => $value) {
                        if ((isset($value) && !empty($value)) && (isset($language_per[$key]) && !empty($language_per[$key]))) {
                            $insert_data = array();
                            $insert_data['language_name'] = $value;
                            $insert_data['language_per'] = $language_per[$key];
                            $insert_data['is_active'] = $is_active[$key] == 'on' ? 1 : 0;
                            $insert_data['ref_user_id'] = $this->user_id;
                            $insert_data['InsUser'] = $this->user_id;
                            $insert_data['InsTerminal'] = $this->input->ip_address();
                            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                            $this->Common_model->InsertInformation($insert_data, 'tbl_user_language');
                        }
                    }
                }
            } else {
                $insert_data['language_name'] = $this->input->post('language_name');
                $insert_data['language_per'] = $this->input->post('language_per');
                $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'user_language_id', $id, 'tbl_user_language');
            }
            redirect('admin/customer/Resume/userLanguage');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('USER_LANGUAGE', 'ADD');
                $data = [];
                $view = 'admin/customer/resume/language/addLanguage';
                $this->page_title = 'LANGUAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('USER_LANGUAGE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['language_data'] = $this->Common_model->getDataById2('tbl_user_language', 'user_language_id', $id, 'Live');
                $view = 'admin/customer/resume/language/editLanguage';
                $this->page_title = 'LANGUAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkUserLanguage($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Resume_model->getUserLanguage($category_id);
            $respone = $this->Resume_model->checkUserLanguage($this->input->get('language_name'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Resume_model->checkUserLanguage($this->input->get('language_name'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteUserLanguage() {
        $this->Common_model->check_menu_access('USER_LANGUAGE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('user_language_id', $id);
            $this->db->update('tbl_user_language');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveLanguage() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('user_language_id', $id)->update('tbl_user_language');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('user_language_id', $id)->update('tbl_user_language');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

    public function additionalDetails() {
        $data = [];
        $this->menu_id = 'ADDITION_DETAILS';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('ADDITION_DETAILS');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('ADDITION_DETAILS', 'VIEW');
        $data['resume_category_data'] = $this->Resume_model->getResumeData($this->user_id);
        $view = 'admin/customer/resume/additional_details/additional_details';
        $this->page_title = 'ADDITION DETAILS';
        $this->load_admin_view($view, $data);
    }

    public function addEditAdditionalDetails($encrypted_id = "") {
        $this->menu_id = 'ADDITION_DETAILS';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['additional_title'] = $this->input->post('additional_title');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['ref_user_id'] = $this->user_id;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $category_id = $this->Common_model->InsertInformation($insert_data, 'tbl_additional_detail');
                if (isset($category_id) && !empty($category_id)) {
                    $this->Resume_model->insertResumeDescription($category_id);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'additional_title_id', $id, 'tbl_additional_detail');
                if (isset($id) && !empty($id)) {
                    $this->Common_model->deleteRecord($id, 'tbl_additional_detail_item', 'ref_additional_title_id');
                    $this->Resume_model->insertResumeDescription($id);
                }
            }
            redirect('admin/customer/Resume/additionalDetails');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('ADDITION_DETAILS', 'ADD');
                $data = [];
                $view = 'admin/customer/resume/additional_details/addAdditionalDetails';
                $this->page_title = 'ADDITION DETAILS';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('ADDITION_DETAILS', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['category_data'] = $this->Common_model->getDataById2('tbl_additional_detail', 'additional_title_id', $id, 'Live');
                $data['description_data'] = $this->Common_model->getDataById('tbl_additional_detail_item', 'ref_additional_title_id', $id);
                $view = 'admin/customer/resume/additional_details/editAdditionalDetails';
                $this->page_title = 'ADDITION DETAILS';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkAdditionalDetails($category_id = '') {
        $user_id = $this->input->get('user_id');
        if ($category_id != '') {
            $catgory_name = $this->Resume_model->getAdditionalDetails($category_id);
            $respone = $this->Resume_model->checkAdditionalDetails($this->input->get('additional_title'), $user_id, $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Resume_model->checkAdditionalDetails($this->input->get('additional_title'), $user_id);
            echo $response;
            die;
        }
    }

    public function deleteAdditionalDetails() {
        $this->Common_model->check_menu_access('ADDITION_DETAILS', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('additional_title_id', $id);
            $this->db->update('tbl_additional_detail');
            if ($this->db->affected_rows() > 0) {
                $this->db->set('del_status', "Deleted");
                $this->db->where('ref_additional_title_id', $id);
                $this->db->update('tbl_additional_detail_item');
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactiveAdditiona() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('additional_title_id', $id)->update('tbl_additional_detail');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('additional_title_id', $id)->update('tbl_additional_detail');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
