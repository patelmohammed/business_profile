<?php

class Payment extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'PAYMENT_INFORMATION';
        $this->load->model('Payment_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'PAYMENT_INFORMATION';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PAYMENT_INFORMATION');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PAYMENT_INFORMATION', 'VIEW');
        $data['payment_data'] = $this->Common_model->getDataById('tbl_user_payment_info', 'ref_user_id', $this->user_id, 'Live');
        $view = 'admin/customer/payment/payment';
        $this->page_title = 'PAYMENT INFORMATION';
        $this->load_admin_view($view, $data);
    }

    public function addEditPaymentInformation($encrypted_id = "") {
        $this->menu_id = 'PAYMENT_INFORMATION';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();

            $new_path = 'assets/images/payment/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['qr_code']['name']) && isset($_FILES['qr_code']['name'])) {
                if ($_FILES['qr_code']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('qr_code')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/customer/Payment');
                    } else {
                        $image = $this->upload->data();
                        $qr_code_url = $new_path . $image['file_name'];
                    }
                } else {
                    $qr_code_url = $this->input->post('hidden_qr_code');
                }
            } else {
                $qr_code_url = $this->input->post('hidden_qr_code');
            }
            $insert_data['qr_code'] = $qr_code_url;
            $insert_data['is_active_qr_code'] = $this->input->post('is_active_qr_code') == 'on' ? 1 : 0;

            $insert_data['bank_name'] = $this->input->post('bank_name');
            $insert_data['acc_holder_name'] = $this->input->post('acc_holder_name');
            $insert_data['branch_name'] = $this->input->post('branch_name');
            $insert_data['account_number'] = $this->input->post('account_number');
            $insert_data['ifsc_code'] = $this->input->post('ifsc_code');
//            $insert_data['copy_detail'] = $this->input->post('copy_detail');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $payment_data = $this->Common_model->getDataById('tbl_user_payment_info', 'ref_user_id', $this->user_id, 'Live');
                if ((getPlanData($this->plan_id)->payment_info_limit) > count($payment_data)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['InsUser'] = $this->user_id;
                    $insert_data['InsTerminal'] = $this->input->ip_address();
                    $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                    $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_user_payment_info');
                } else {
                    $this->_show_message("You cant insert new payment information", "error");
                    redirect('admin/customer/Payment');
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'payment_info_id', $id, 'tbl_user_payment_info');
            }
            redirect('admin/customer/Payment');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $payment_data = $this->Common_model->getDataById('tbl_user_payment_info', 'ref_user_id', $this->user_id, 'Live');
                if ((getPlanData($this->plan_id)->payment_info_limit) > count($payment_data)) {
                    $this->Common_model->check_menu_access('PAYMENT_INFORMATION', 'ADD');
                    $data = [];
                    $view = 'admin/customer/payment/addPayment';
                    $this->page_title = 'PAYMENT INFORMATION';
                    $this->load_admin_view($view, $data);
                } else {
                    $this->_show_message("You cant insert new payment information", "error");
                    redirect('admin/customer/Payment');
                }
            } else {
                $this->Common_model->check_menu_access('PAYMENT_INFORMATION', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['payment_data'] = $this->Common_model->getDataById2('tbl_user_payment_info', 'payment_info_id', $id, 'Live');
                $view = 'admin/customer/payment/editPayment';
                $this->page_title = 'PAYMENT INFORMATION';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deletePaymentInformation() {
        $this->Common_model->check_menu_access('PAYMENT_INFORMATION', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('payment_info_id', $id);
            $this->db->update('tbl_user_payment_info');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function activeDeactivePayment() {
        $data = array();
        $id = $this->input->post('id');
        $is_active = $this->input->post('is_active');
        if (isset($id) && !empty($id)) {
            if ($is_active == 0) {
                $this->db->set('is_active', 1)->where('payment_info_id', $id)->update('tbl_user_payment_info');
                $data['result'] = TRUE;
            } else {
                $this->db->set('is_active', 0)->where('payment_info_id', $id)->update('tbl_user_payment_info');
                $data['result'] = FALSE;
            }
        }
        echo json_encode($data);
        die;
    }

}
