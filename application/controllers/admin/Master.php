<?php

class Master extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'MASTER';
        $this->load->model('Master_model');
        $this->load->model('Common_model');
        $this->load->model('Auth_model');
    }

    public function index() {
        $data = [];
        $view = 'admin/home/index';
        $this->Common_model->check_menu_access('DASHBOARD', 'VIEW');
        $this->page_title = 'DASHBOARD';
        $this->load_admin_view($view, $data);
    }

    public function user() {
        $data = [];
        $this->menu_id = 'USER';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('USER');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('USER', 'VIEW');
        $data['user_data'] = $this->Common_model->geAlldata('tbl_user_info');
        $view = 'admin/master/user/user';
        $this->page_title = 'USER';
        $this->load_admin_view($view, $data);
    }

    public function addEditUser($encrypted_id = "") {
        $this->menu_id = 'USER';
        $id = $encrypted_id;
        $user_menu_access_arr = [];
        if ($this->input->post()) {
            $user_info = $user_menu_data = array();
            $insert_data['user_name'] = $this->input->post('user_name');
            $insert_data['user_email'] = $this->input->post('user_email');
            $insert_data['user_number'] = $this->input->post('mobile');

            $plan_type = $this->input->post('plan_type');
            $insert_data['ref_plan_id'] = $plan_type;
            if (allowProfileType($plan_type) == 1) {
                $insert_data['ref_profile_type'] = $this->input->post('profile_type');
            }

            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if ((isset($password) && !empty($password)) && (isset($confirm_password) && !empty($confirm_password))) {
                if ($password == $confirm_password) {
                    $insert_data['user_password'] = md5($password);
                } else {
                    $this->_show_message("Password And Confirm Password not matched", "error");
                    if ($id == "" || $id == '' || $id == NULL) {
                        redirect('admin/Master/addEditUser');
                    } else {
                        redirect('admin/Master/addEditUser/' . $encrypted_id);
                    }
                }
            }

            $full_access = $this->input->post('full_access');
            $view = $this->input->post('view');
            $add = $this->input->post('add');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');

            $ins_menu = [];
            if (!empty($full_access)) {
                foreach ($full_access as $key => $value) {
                    $ins_menu[$key]['full_access'] = $value == 'on' ? 1 : 0;
                }
            }
            if (!empty($view)) {
                foreach ($view as $vi_key => $vi_value) {
                    $ins_menu[$vi_key]['view_right'] = $vi_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($add)) {
                foreach ($add as $ad_key => $ad_value) {
                    $ins_menu[$ad_key]['add_right'] = $ad_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($edit)) {
                foreach ($edit as $ed_key => $ed_value) {
                    $ins_menu[$ed_key]['edit_right'] = $ed_value == 'on' ? 1 : 0;
                }
            }
            if (!empty($delete)) {
                foreach ($delete as $del_key => $del_value) {
                    $ins_menu[$del_key]['delete_right'] = $del_value == 'on' ? 1 : 0;
                }
            }

            if (!empty($ins_menu)) {
                foreach ($ins_menu as $key => $value) {
                    $ins_menu[$key]['ref_menu_id'] = $key;
                    $user_menu_data[] = $ins_menu[$key];
                }
            }

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $user_id = $this->Common_model->insertInformation($insert_data, 'tbl_user_info');
                $this->Auth_model->insertSideMenuAccess($user_menu_data, $user_id);
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'user_id', $id, 'tbl_user_info');
                $this->Auth_model->updateSideMenuAccess($user_menu_data, $id);
            }
            redirect('admin/Master/user');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('USER', 'ADD');
                $data = [];
                $view = 'admin/master/user/addUser';
                $data['side_menu'] = $this->Auth_model->getMenuMSTData();
                $data['plan_data'] = $this->Common_model->geAlldata('tbl_plan');
                $this->page_title = 'USER';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('USER', 'EDIT');
                $data = [];
                $data['side_menu'] = $this->Auth_model->getMenuAccessById($encrypted_id);
                $data['plan_data'] = $this->Common_model->geAlldata('tbl_plan');
                $data['encrypted_id'] = $encrypted_id;
                $data['user_data'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $id, 'Live');
                $view = 'admin/master/user/editUser';
                $this->page_title = 'USER';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkUserName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Master_model->getUserName($category_id);
            $respone = $this->Master_model->checkUserName($this->input->get('user_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkUserName($this->input->get('user_name'));
            echo $response;
            die;
        }
    }

    public function checkUserEmail($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Master_model->getUserEmail($category_id);
            $respone = $this->Master_model->checkUserEmail($this->input->get('user_email'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkUserEmail($this->input->get('user_email'));
            echo $response;
            die;
        }
    }

    public function deleteUser() {
        $this->Common_model->check_menu_access('USER', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('id', $id);
            $this->db->update('user_information');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function plan() {
        $data = [];
        $this->menu_id = 'PLAN';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PLAN');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PLAN', 'VIEW');
        $data['plan_data'] = $this->Common_model->geAlldata('tbl_plan');
        $view = 'admin/master/plan/plan';
        $this->page_title = 'PLAN';
        $this->load_admin_view($view, $data);
    }

    public function addEditPlan($encrypted_id = "") {
        $this->menu_id = 'PLAN';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['plan_name'] = $this->input->post('plan_name');
            $insert_data['plan_price'] = $this->input->post('plan_price');
            $insert_data['is_allow_profile_type'] = $this->input->post('is_allow_profile_type') == 'on' ? 1 : 0;
            $insert_data['is_allow_insert_client'] = $this->input->post('is_allow_insert_client') == 'on' ? 1 : 0;

            $insert_data['product_limit'] = $this->input->post('product_limit');
            $insert_data['address_limit'] = $this->input->post('address_limit');
            $insert_data['service_limit'] = $this->input->post('service_limit');
            $insert_data['payment_info_limit'] = $this->input->post('payment_info_limit');
            $insert_data['item_limit'] = $this->input->post('item_limit');
            $insert_data['client_limit'] = $this->input->post('client_limit');
            $insert_data['gallery_category_limit'] = $this->input->post('gallery_category_limit');

            $insert_data['is_sale'] = $this->input->post('sale_switch') == 'on' ? 1 : 0;
            $insert_data['plan_sale_price'] = $this->input->post('plan_sale_price');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_plan');
                if (isset($plan_id) && !empty($plan_id)) {
                    $this->Master_model->insert_plan_desc($plan_id);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->updateInformation2($insert_data, 'plan_id', $id, 'tbl_plan');

                if (isset($id) && !empty($id)) {
                    $this->Common_model->deleteRecord($id, 'tbl_plan_desc', 'ref_plan_id');
                    $this->Master_model->insert_plan_desc($id);
                }
            }
            redirect('admin/Master/plan');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('PLAN', 'ADD');
                $data = [];
                $view = 'admin/master/plan/addPlan';
                $this->page_title = 'PLAN';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('PLAN', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['plan_data'] = $this->Common_model->getDataById2('tbl_plan', 'plan_id', $id, 'Live');
                $data['plan_desc_data'] = $this->Master_model->getPlanDescription($id);
                $view = 'admin/master/plan/editPlan';
                $this->page_title = 'PLAN';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkPlanName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Master_model->getPlanName($category_id);
            $respone = $this->Master_model->checkPlanName($this->input->get('plan_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Master_model->checkPlanName($this->input->get('plan_name'));
            echo $response;
            die;
        }
    }

    public function deletePlan() {
        $this->Common_model->check_menu_access('PLAN', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('plan_id', $id);
            $this->db->update('tbl_plan');
            if ($this->db->affected_rows() > 0) {
                $this->db->set('del_status', "Deleted");
                $this->db->where('ref_plan_id', $id);
                $this->db->update('tbl_plan_desc');
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function allowProfileType() {
        $data = array();
        $id = $this->input->post('id');
        $data['allowProfileType'] = allowProfileType($id);
        echo json_encode($data);
        die;
    }

}
