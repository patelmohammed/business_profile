<?php

Class My_Controller extends CI_Controller {

    public $page_id;
    public $page_title;
    public $menu_id;
    public $sub_menu_id;
    public $add_message;
    public $user_id;
    public $role;

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
        $this->page_id = '';
        $this->page_title = '';
        $this->menu_id = '';
        $this->sub_menu_id = '';
        $this->user_id = $this->session->userdata('user_id');
        $this->role = $this->session->userdata('role');
        $this->user_type = $this->session->userdata('user_type');
        $this->plan_id = $this->session->userdata('plan_id');

        //Client Side
        $this->card_user_id = '';
        $this->profile_type = '';
    }

    public function load_view($view, $data, $flag = true) {
        if ($flag) {
            $data['footer_data'] = $this->Common_model->get_footer();
            $this->load->view('header', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('footer', $data);
        }
    }

    public function load_admin_view($view, $data, $flag = true) {
        $data['add_message'] = $this->session->flashdata('message');
        $data['user_info'] = $this->Common_model->getDataById2('tbl_user_info', 'user_id', $this->user_id, 'Live');
        if ($flag) {
            $this->load->view('admin/home/header', $data);
            $this->load->view('admin/home/side_menu', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('admin/home/footer', $data);
        }
    }

    public function is_login() {
        $dat = $this->user_id;
        if (!empty($dat)) {
            return true;
        }
        return false;
    }

    public function _show_message($message, $type = 'message') {
        $message = 'toastr["' . $type . '"]("' . $message . '");';
        $this->session->set_flashdata('message', $message);
    }

}
