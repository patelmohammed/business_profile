<?php

class Gallery_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkCategoryName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND gallery_category_name != '$currentName'" : '');
        $sql = "SELECT gallery_category_name FROM tbl_gallery_category 
                WHERE gallery_category_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getCategoryName($id) {
        $sql = "SELECT gallery_category_name FROM tbl_gallery_category
                WHERE del_status = 'Live'
                AND gallery_category_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->gallery_category_name;
    }

    public function checkGalleryName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND gallery_name != '$currentName'" : '');
        $sql = "SELECT gallery_name FROM tbl_gallery 
                WHERE gallery_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getGalleryName($id) {
        $sql = "SELECT gallery_name FROM tbl_gallery
                WHERE del_status = 'Live'
                AND gallery_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->gallery_name;
    }

}
