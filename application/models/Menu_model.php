<?php

class Menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkMenuCategoryName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND category_name != '$currentName'" : '');
        $sql = "SELECT category_name FROM tbl_menu_category 
                WHERE category_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getMenuCategoryName($id) {
        $sql = "SELECT category_name FROM tbl_menu_category
                WHERE del_status = 'Live'
                AND category_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->category_name;
    }

    public function checkItemName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND item_name != '$currentName'" : '');
        $sql = "SELECT item_name FROM tbl_menu_item 
                WHERE item_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getItemName($id) {
        $sql = "SELECT item_name FROM tbl_menu_item
                WHERE del_status = 'Live'
                AND item_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->item_name;
    }

}
