<?php

class Contact_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkContactName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND contact_name != '$currentName'" : '');
        $sql = "SELECT contact_name FROM tbl_contact 
                WHERE contact_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getContactName($id) {
        $sql = "SELECT contact_name FROM tbl_contact
                WHERE del_status = 'Live'
                AND contact_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->contact_name;
    }

}
