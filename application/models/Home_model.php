<?php

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function gePlanData() {
        $sql = "SELECT * 
                FROM tbl_plan 
                WHERE del_status = 'Live'";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->plan_desc = $this->getPlanDescById($value->plan_id);
            }
        }
        return $result;
    }

    public function getPlanDescById($plan_id) {
        $sql = "SELECT * 
                FROM tbl_plan_desc
                WHERE ref_plan_id = '$plan_id'";
        return $this->db->query($sql)->result();
    }

    public function getResumeData($user_id) {
        $sql = "SELECT rc.* 
                FROM tbl_additional_detail rc 
                WHERE rc.ref_user_id = '$user_id' AND rc.del_status = 'Live' AND rc.is_active = '1'";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->additional_description = $this->getResumeDescription($value->additional_title_id);
            }
        }
        return $result;
    }

    public function getResumeDescription($category_id) {
        $sql = "SELECT * 
                FROM tbl_additional_detail_item 
                WHERE del_status = 'Live' AND ref_additional_title_id = '$category_id' AND is_active = '1'";
        return $this->db->query($sql)->result();
    }

    public function getExpEduData($id, $type = NULL) {
        $sql = "SELECT * 
                FROM tbl_experience_education tee
                WHERE tee.ref_user_id = '$id' AND tee.del_status = 'Live' AND tee.is_active = '1'";
        if (isset($type) && !empty($type)) {
            $sql .= " AND tee.is_exp_or_edu = '$type'";
        }
        $sql .= " ORDER BY tee.current_status DESC, tee.period_to DESC, tee.period_from DESC";
        return $this->db->query($sql)->result();
    }

    public function getServiceItemById($id) {
        $sql = "SELECT * 
                FROM tbl_user_service_item 
                WHERE ref_service_id = '$id' AND del_status = 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function getProductWithCategory($user_id) {
        $sql = "SELECT gc.gallery_category_id, gc.gallery_category_name
                FROM tbl_gallery_category gc
                WHERE gc.ref_user_id = '$user_id' AND gc.is_active = '1' AND gc.del_status  = 'Live'";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->gallery = $this->getGalleryDataByCategoryId($value->gallery_category_id, $user_id);
            }
        }
        return $result;
    }

    public function getGalleryDataByCategoryId($category_id, $user_id) {
        $sql = "SELECT * 
                FROM tbl_gallery g
                WHERE g.ref_gallery_category_id = '$category_id' AND g.ref_user_id = '$user_id' AND g.del_status = 'Live' AND g.is_active = '1'";
        return $this->db->query($sql)->result();
    }

}
