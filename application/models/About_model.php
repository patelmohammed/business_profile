<?php

class About_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkClientName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND client_name != '$currentName'" : '');
        $sql = "SELECT client_name FROM tbl_user_client 
                WHERE client_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getClientName($id) {
        $sql = "SELECT client_name FROM tbl_user_client
                WHERE del_status = 'Live'
                AND client_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->client_name;
    }

}
