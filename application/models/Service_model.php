<?php

class Service_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertServiceItem($id) {
        $service_item_title = $this->input->post('service_item_title');
        $service_item_desc = $this->input->post('service_item_desc');
        $is_active_service_item = $this->input->post('is_active_service_item');
        if ((isset($service_item_title) && !empty($service_item_title)) && (isset($service_item_desc) && !empty($service_item_desc))) {
            foreach ($service_item_title as $key => $value) {
                $insert_data = array();
                if (isset($service_item_desc[$key]) && !empty($service_item_desc[$key])) {
                    $insert_data['ref_service_id'] = $id;
                    $insert_data['service_item_title'] = $value;
                    $insert_data['service_item_desc'] = $service_item_desc[$key];
                    $insert_data['is_active_service_item'] = isset($is_active_service_item[$key]) && !empty($is_active_service_item[$key]) && $is_active_service_item[$key] == 'on' ? 1 : 0;
                    $this->db->insert('tbl_user_service_item', $insert_data);
                }
            }
        }
    }

    public function getServiceItemById($id) {
        $sql = "SELECT * 
                FROM tbl_user_service_item 
                WHERE ref_service_id = '$id' AND del_status = 'Live' ";
        return $this->db->query($sql)->result();
    }

    public function getServiceById($id) {
        $sql = "SELECT * 
                FROM tbl_user_service tus
                LEFT JOIN tbl_user_service_item tsui ON tsui.ref_service_id = tus.service_id AND tsui.del_status = 'Live'
                WHERE tus.ref_user_id = '$id' AND tus.del_status = 'Live' 
                GROUP BY tsui.service_item_id";
        return $this->db->query($sql)->result();
    }

}
