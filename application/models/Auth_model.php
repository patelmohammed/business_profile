<?php

class Auth_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getUserByEmail($username) {
        $sql = "SELECT ui.* 
                FROM tbl_user_info ui 
                WHERE ui.del_status = 'Live' AND (ui.user_email = '$username' OR ui.user_number = '$username') LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function check_existing_generator_id($table_name, $field_name, $value) {
        $existing_id = '';
        if ($table_name != '' && $field_name != '') {
            $this->db->select($field_name);
            $this->db->from($table_name);
            $this->db->where($field_name, "$value");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $existing_id = $query->row();
            } else {
                $existing_id = '';
            }
        }

        return $existing_id;
    }

    function get_assigned_menu($user_id, $role) {
        if ($role == 'Admin') {
            $sql = "SELECT mm.*, 1 AS full_access, 1 AS view_right, 1 AS add_right, 1 AS edit_right, 1 AS delete_right
                FROM tbl_side_menu mm
                WHERE mm.del_status = 'Live' 
                ORDER BY ref_menu_id ASC , menu_order_no ASC";
        } else {
            $sql = "SELECT mm.*, full_access, umr.view_right, umr.add_right, umr.edit_right, umr.delete_right
                FROM tbl_side_menu mm
                INNER JOIN tbl_side_menu_rights umr ON umr.ref_menu_id = mm.menu_id
                WHERE umr.ref_user_id = '$user_id' AND mm.del_status = 'Live' 
                UNION
                (SELECT *, '1' AS full_access,'1' AS view_right,'1' AS add_right,'1' AS edit_right,'1' AS delete_right 
                FROM tbl_side_menu 
                WHERE menu_id IN 
                (SELECT DISTINCT mm1.ref_menu_id FROM tbl_side_menu mm1
                INNER JOIN tbl_side_menu_rights umr1 ON umr1.ref_menu_id = mm1.menu_id
                WHERE umr1.ref_user_id = '$user_id' AND mm1.ref_menu_id > 0) AND del_status = 'Live') 
                ORDER BY ref_menu_id ASC , menu_order_no ASC";
        }

        $str = $this->db->query($sql);
        return $str->result();
    }

    public function getMenuMSTData() {
        $sql = "SELECT * FROM tbl_side_menu tm 
                WHERE tm.ref_menu_id IS NULL AND del_status = 'Live' ORDER BY tm.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_add_sub_menu($value->menu_id);
                }
            }
        }
        return $res;
    }

    function get_add_sub_menu($MenuID) {
        $sql = "SELECT * FROM tbl_side_menu m 
                WHERE m.ref_menu_id = '" . $MenuID . "' AND del_status = 'Live' ORDER BY m.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_add_sub_menu($value->menu_id);
                }
            }
        }
        return $res;
    }

    public function insertSideMenuAccess($user_menu_data, $user_id) {
        if (!empty($user_menu_data)) {
            foreach ($user_menu_data as $ke => $val) {
                $user_menu_data[$ke]['ref_user_id'] = $user_id;
                $this->db->insert('tbl_side_menu_rights', $user_menu_data[$ke]);
            }
        }
    }

    function getMenuAccessById($ref_user_id) {
        $sql = "SELECT * FROM tbl_side_menu tm 
                LEFT JOIN tbl_side_menu_rights um ON um.ref_menu_id = tm.menu_id AND um.ref_user_id = $ref_user_id 
                WHERE tm.ref_menu_id IS NULL AND tm.del_status = 'Live' ORDER BY tm.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_edit_sub_menu($value->menu_id, $ref_user_id);
                }
            }
        }
        return $res;
    }

    function get_edit_sub_menu($MenuID, $ref_user_id) {
        $sql = "SELECT * FROM tbl_side_menu m 
                LEFT JOIN tbl_side_menu_rights um ON um.ref_menu_id = m.menu_id AND um.ref_user_id = $ref_user_id
                WHERE m.ref_menu_id = '" . $MenuID . "' AND m.del_status = 'Live' ORDER BY m.menu_order_no ASC";
        $str = $this->db->query($sql);
        $res = $str->result();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value->menu_url == '#') {
                    $res[$key]->sub_menu = $this->get_edit_sub_menu($value->menu_id, $ref_user_id);
                }
            }
        }
        return $res;
    }

    public function updateSideMenuAccess($user_menu_data, $user_id) {
        $this->db->where('ref_user_id', $user_id)->delete('tbl_side_menu_rights');
        if (!empty($user_menu_data)) {
            foreach ($user_menu_data as $ke => $val) {
                $user_menu_data[$ke]['ref_user_id'] = $user_id;
                $this->db->insert('tbl_side_menu_rights', $user_menu_data[$ke]);
            }
        }
    }

    public function createbitlyShortLink($user_id, $user_name = NULL) {
        $enc_id = encodeId($user_id);
        $url = 'http://biz.weborative.com/' . $enc_id . (isset($user_name) && !empty($user_name) ? '/' . strtolower(cleanString($user_name)) : '');
        $post_array = ["long_url" => $url, "domain" => "bit.ly", "group_guid" => "Bk6c9TwviZL"];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-ssl.bitly.com/v4/shorten",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post_array),
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer e77d85bdb38a09094aeef87b50551545a4344353",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->db->where('user_id', $user_id)->update('tbl_user_info', array('is_created_short_link' => '0'));
            $this->db->where('user_id', $user_id)->update('tbl_user_info', array('user_short_link' => $url));
            echo $url;
        } else {
            $response = json_decode($response);
            $this->db->where('user_id', $user_id)->update('tbl_user_info', array('is_created_short_link' => '1'));
            $this->db->where('user_id', $user_id)->update('tbl_user_info', array('user_short_link' => $response->link));
//            echo $response;
        }
    }

}
