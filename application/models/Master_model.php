<?php

class Master_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkUserName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_name != '$currentName'" : '');
        $sql = "SELECT user_name FROM tbl_user_info 
                WHERE user_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserName($id) {
        $sql = "SELECT user_name FROM tbl_user_info
                WHERE del_status = 'Live'
                AND user_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_name;
    }

    public function checkUserEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_email != '$currentName'" : '');
        $sql = "SELECT user_email FROM tbl_user_info 
                WHERE user_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserEmail($id) {
        $sql = "SELECT user_email FROM tbl_user_info
                WHERE del_status = 'Live'
                AND user_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_email;
    }

    public function checkPlanName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND plan_name != '$currentName'" : '');
        $sql = "SELECT plan_name FROM tbl_plan 
                WHERE plan_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getPlanName($id) {
        $sql = "SELECT plan_name FROM tbl_plan
                WHERE del_status = 'Live'
                AND plan_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->plan_name;
    }

    public function insert_plan_desc($plan_id) {
        $plan_desc = $this->input->post('plan_desc');
        if (isset($plan_desc) && !empty($plan_desc)) {
            foreach ($plan_desc as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $insert_data['ref_plan_id'] = $plan_id;
                    $insert_data['plan_description'] = $value;
                    $this->db->insert('tbl_plan_desc', $insert_data);
                }
            }
        }
    }

    public function getPlanDescription($plan_id) {
        $sql = "SELECT * 
                FROM tbl_plan_desc
                WHERE ref_plan_id = '$plan_id'";
        return $this->db->query($sql)->result();
    }

}
