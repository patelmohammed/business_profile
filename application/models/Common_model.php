<?php

class Common_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertInformation($data, $table_name) {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function updateInformation($data, $id, $table_name) {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
    }

    public function updateInformation2($data, $column_name, $id, $table_name, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }

        if (!empty($status) && $status == 'Live') {
            $this->db->where('del_status', 'Live');
        }

        $this->db->where($column_name, $id);
        if (!empty($column_name_2) && !empty($id_2)) {
            $this->db->where($column_name_2, $id_2);
        }

        $this->db->update($table_name, $data);
        return true;
    }

    public function deleteStatusChange($id, $table_name, $column_name) {
        $this->db->set('del_status', "Deleted");
        $this->db->where($column_name, $id);
        $this->db->update($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecord($id, $table_name, $column_name, $condition = NULL) {
        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRecordUpdateStatus($id, $table_name, $column_name, $condition = NULL, $status = NULL) {
        if (isset($status) && !empty($status)) {
            $set = explode('||', $status);
            $this->db->set($set[0], $set[1]);
        }

        if (isset($condition) && !empty($condition)) {
            $where = explode('||', $condition);
            $this->db->where($where[0], $where[1]);
        }

        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataById($table_id, $column_name, $id, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND del_status = '$status'";
        } else {
            $sql .= " AND del_status = 'Live'";
        }
        if ((isset($column_name_2) && !empty($column_name_2)) && (isset($id_2) && !empty($id_2))) {
            $sql .= " AND $column_name_2 = '$id_2'";
        }
        return $this->db->query($sql)->result();
    }

    public function getDataById2($table_id, $column_name, $id, $status = NULL, $column_name_2 = NULL, $id_2 = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = '$id' ";
        if (isset($status) && !empty($status)) {
            if ($status == '1') {
                $sql .= " AND status = '1'";
            } elseif ($status == 'Live') {
                $sql .= " AND del_status = 'Live'";
            }
        }
        if ((isset($column_name_2) && !empty($column_name_2)) && (isset($id_2) && !empty($id_2))) {
            $sql .= " AND $column_name_2 = '$id_2'";
        }
        return $this->db->query($sql)->row();
    }

    public function getDataByIdStatus($table_id, $column_name, $id, $status = NULL) {
        $sql = "SELECT * FROM $table_id WHERE $column_name = $id ";
        if (isset($status) && !empty($status)) {
            $sql .= " AND $status";
        } else {
            $sql .= " AND status = '1'";
        }
        return $this->db->query($sql)->row();
    }

    public function geAlldata($table_name) {
        $sql = "SELECT * FROM $table_name WHERE del_status = 'Live'";
        return $this->db->query($sql)->result();
    }

    public function deleteInformation($column_name, $id, $table_name) {
        $this->db->where($column_name, $id);
        $this->db->delete($table_name);
        return true;
    }

    public function getCompanyInformation() {
        $sql = "SELECT * FROM company_information";
        return $this->db->query($sql)->row();
    }

    public function generateNoWithCompanyCode($column_name, $table_name) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE status= '1' ";
        return $this->db->query($sql)->row()->Number;
    }

    public function getCompanyCode() {
        $sql = "SELECT company_code FROM company_information WHERE status = '1' LIMIT 1";
        return $this->db->query($sql)->row()->company_code;
    }

    public function chkUniqueCode($table_name, $column_name, $code, $status = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$code");
        if (!empty($status) && $status == 1) {
            $this->db->where('status', '1');
        }
        if (!empty($status) && $status == 'Live') {
            $this->db->where('status', 'Live');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function chkUniqueData($table_name, $column_name, $value, $status = '', $column_name_2 = '', $value_2 = '', $status_2 = '') {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where("$column_name", "$value");
        if (isset($status) && !empty($status)) {
            $this->db->where('del_status', 'Live');
        }

        if (isset($column_name_2) && !empty($column_name_2) && isset($value_2) && !empty($value_2)) {
            $this->db->where("$column_name_2", "$value_2");
        }
        if (isset($status_2) && !empty($status_2)) {
            if (!empty($status_2) && $status_2 == 1) {
                $this->db->where('status', '1');
            } else if (!empty($status_2) && $status_2 == 'Live') {
                $this->db->where('status', 'Live');
            }
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    public function generateNoIndividual($column_name, $table_name, $primary_id, $status = NULL) {
        $sql = "SELECT (IFNULL(MAX($column_name),0)+1)AS Number FROM $table_name WHERE 1 = 1 ";
        if (isset($status) && !empty($status)) {
            $where = explode('||', $status);
            if (isset($where[0]) && isset($where[1]) && !empty($where[1]) && !empty($where[1])) {
                $sql .= " AND $where[0] = '$where[1]'";
            }
        }
        if (isset($primary_id) && !empty($primary_id)) {
            $sql .= " AND ref_outlet_id = '$primary_id'";
        }
        return $this->db->query($sql)->row()->Number;
    }

    public function send_sms($contact, $text) {
        $apikey = '5c4158e36a3d8';
        $sender = 'FOODML';
        $route = 'trans_dnd';
        $message = urlencode($text);

        if (isset($contact) && !empty($contact)) {
            $api_params = '?apikey=' . $apikey . '&route=' . $route . '&sender=' . $sender . '&mobileno=' . $contact . '&text=' . $message;
            $smsGatewayUrl = "http://sms.mobileadz.in/api/push.json";
            $smsgatewaydata = $smsGatewayUrl . $api_params;
            $url = $smsgatewaydata;

            $ch = curl_init(); // initialize CURL
            curl_setopt($ch, CURLOPT_POST, false); // Set CURL Post Data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $data = array();
            if (curl_errno($ch)) { //catch if curl error exists and show it
                $data['log'] = 'Curl error: ' . curl_error($ch);
                $data['result'] = 'error';
                $data['msg'] = 'SMS Sending Failed!';
            } else {
                $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
                switch ($returnCode) {
                    case 200 :
                        $data['log'] = $result;
                        $data['result'] = 'success';
                        $data['msg'] = 'SMS Sending Successfully!';
                        break;
                    default :
                        $data['log'] = "HTTP ERROR: " . $returnCode;
                        $data['result'] = 'error';
                        $data['msg'] = 'SMS Sending Failed!';
                }
            }
        }
        curl_close($ch);
        return json_encode($data);
    }

    public function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80) {
        list($origWidth, $origHeight, $type) = getimagesize($sourceImage);

        if ($type == 1) {
            header('Content-Type: image/gif');
            $image = imagecreatefromgif($sourceImage);
        } elseif ($type == 2) {
            header('Content-Type: image/jpeg');
            $image = imagecreatefromjpeg($sourceImage);
        } elseif ($type == 3) {
            header('Content-Type: image/png');
            $image = imagecreatefrompng($sourceImage);
        } else {
            header('Content-Type: image/x-ms-bmp');
            $image = imagecreatefromwbmp($sourceImage);
        }

        if ($maxWidth == 0) {
            $maxWidth = $origWidth;
        }

        if ($maxHeight == 0) {
            $maxHeight = $origHeight;
        }

// Calculate ratio of desired maximum sizes and original sizes.
        $widthRatio = $maxWidth / $origWidth;
        $heightRatio = $maxHeight / $origHeight;

// Ratio used for calculating new image dimensions.
        $ratio = min($widthRatio, $heightRatio);

// Calculate new image dimensions.
        $newWidth = (int) $origWidth * $ratio;
        $newHeight = (int) $origHeight * $ratio;

// Create final image with new dimensions.
// if($type==1 or $type==3)
// {
//    $newImage = imagefill($newImage,0,0,0x7fff0000);
// }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);

        $transparent = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
        imagefill($newImage, 0, 0, $transparent);
        imagesavealpha($newImage, true);


        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
        imagepng($newImage, $targetImage);

// Free up the memory.
        imagedestroy($image);
        imagedestroy($newImage);

        return true;
    }

    public function check_menu_access($constant, $type) {
        $data = array();
        $user_assigned_menus = $this->session->userdata('side_menu');
        $return = false;
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    if ($value->view_right >= 1 && $type == 'VIEW') {
                        $return = TRUE;
                    }
                    if ($value->add_right >= 1 && $type == 'ADD') {
                        $return = TRUE;
                    }
                    if ($value->edit_right >= 1 && $type == 'EDIT') {
                        $return = TRUE;
                    }
                    if ($value->delete_right >= 1 && $type == 'DELETE') {
                        $return = TRUE;
                    }
                }
            }
        }
        if (!$return) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(['res' => 'error', 'result' => 'error', 'msg' => 'Unauthorized access']);
                die;
            } else {
                redirect('admin/Auth/Unauthorized');
            }
        }
        return $return;
    }

    public function get_menu_rights($constant) {
        $user_assigned_menus = $this->session->userdata('side_menu');
        if (!empty($user_assigned_menus)) {
            foreach ($user_assigned_menus as $key => $value) {
                if ($value->menu_constant === $constant) {
                    return (array) $value;
                }
            }
        } else {
            redirect('admin/Auth/Unauthorized');
        }
    }

    public function get_footer() {
        return $this->db->get('tbl_footer')->row();
    }

    public function insertCustomerProfile($id) {
        $inser_profile = $side_menu_right = array();
        $inser_profile['ref_user_id'] = $id;
        $inser_profile['cover_photo'] = 'assets/images/bg.jpg';
        $inser_profile['cover_compressed_photo'] = 'assets/images/bg.jpg';
        $inser_profile['profile_photo'] = 'assets/images/profile.png';
        $inser_profile['profile_compressed_photo'] = 'assets/images/profile.png';
        $inser_profile['theme_color'] = 'green';
        $inser_profile['InsUser'] = $id;
        $inser_profile['InsTerminal'] = $this->input->ip_address();
        $inser_profile['InsDateTime'] = date('Y/m/d H:i:s');
        $this->db->insert('tbl_user_profile', $inser_profile);
    }

    public function insertCustomerSideMenuRight($id, $ref_plan_id, $ref_profile_type) {
        $sql = "INSERT INTO tbl_side_menu_rights 
               (id, ref_user_id, ref_menu_id, full_access, view_right, add_right, edit_right, delete_right) 
                VALUES (NULL, '$id', '1', '0', '1', '1', '1', '0'), 
                       (NULL, '$id', '10', '0', '1', '0', '1', '0'), 
                       (NULL, '$id', '4', '0', '1', '0', '1', '0') ";
        if ($ref_plan_id == 1 || $ref_plan_id == 2) {
            $sql .= ", (NULL, '$id', '12', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '21', '0', '1', '0', '1', '0') 
                     , (NULL, '$id', '18', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '19', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '20', '1', '1', '1', '1', '1') ";
            if ($ref_profile_type == 1) {
                $sql .= ", (NULL, '$id', '8', '0', '1', '0', '1', '0') ";
            }
            if ($ref_profile_type == 2) {
                $sql .= ", (NULL, '$id', '13', '1', '1', '1', '1', '1') 
                         , (NULL, '$id', '14', '1', '1', '1', '1', '1') 
                         , (NULL, '$id', '15', '1', '1', '1', '1', '1') 
                         , (NULL, '$id', '16', '1', '1', '1', '1', '1') ";
            }
        }
        if ($ref_plan_id == 3 || $ref_plan_id == 4) {
            $sql .= ", (NULL, '$id', '4', '0', '1', '0', '1', '0') 
                     , (NULL, '$id', '12', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '21', '0', '1', '0', '1', '0') 
                     , (NULL, '$id', '20', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '23', '1', '1', '1', '1', '1') 
                     , (NULL, '$id', '24', '1', '1', '1', '1', '1') ";
            if ($ref_plan_id == 4) {
                $sql .= ", (NULL, '$id', '18', '1', '1', '1', '1', '1') 
                         , (NULL, '$id', '19', '1', '1', '1', '1', '1') ";
            }
        }
        if (isAllowInsertClient($ref_plan_id) == 1) {
            $sql .= ", (NULL, '$id', '5', '1', '1', '1', '1', '1') ";
        }
        $this->db->query($sql);
    }

    public function insertCustomerServiceResume($id, $ref_profile_type) {
        $inser_profile = array();
        if ($ref_profile_type == 1 || $ref_profile_type == 5) {
            $inser_profile['ref_user_id'] = $id;
            $inser_profile['service_title'] = 'My Services';
            $inser_profile['InsUser'] = $id;
            $inser_profile['InsTerminal'] = $this->input->ip_address();
            $inser_profile['InsDateTime'] = date('Y/m/d H:i:s');
            $this->db->insert('tbl_user_service', $inser_profile);
        }
    }

    public function insertCustomerAbout($id) {
        $inser_profile = array();
        $inser_profile['ref_user_id'] = $id;
        $inser_profile['about_title'] = 'About Me';
        $inser_profile['about_desc'] = 'About Description';
        $inser_profile['InsUser'] = $id;
        $inser_profile['InsTerminal'] = $this->input->ip_address();
        $inser_profile['InsDateTime'] = date('Y/m/d H:i:s');
        $this->db->insert('tbl_about', $inser_profile);
    }

    public function insertContactMap($id) {
        $inser_profile = array();
        $inser_profile['ref_user_id'] = $id;
        $inser_profile['map'] = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3719.781983766904!2d72.83588536493556!3d21.20081798590542!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04ef7dfac7651%3A0x3e9863c0f7b5d6d6!2sNear%20Meghani%20Tower%2C%20Surat%2C%20Gujarat%20395008!5e0!3m2!1sen!2sin!4v1598353347376!5m2!1sen!2sin';
        $inser_profile['InsUser'] = $id;
        $inser_profile['InsTerminal'] = $this->input->ip_address();
        $inser_profile['InsDateTime'] = date('Y/m/d H:i:s');
        $this->db->insert('tbl_map', $inser_profile);
    }

    public function getUserDataForVCF($user_id) {
        $sql = "SELECT upr.vcf_first_name, upr.vcf_last_name, upr.vcf_date_of_birth, upr.vcf_email, upr.vcf_country_id, upr.vcf_phone, upr.vcf_address 
                FROM tbl_user_profile upr 
                WHERE upr.ref_user_id = '$user_id' AND upr.del_status = 'Live'  ";
        return $this->db->query($sql)->row();
    }

}
