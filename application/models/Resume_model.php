<?php

class Resume_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkSkillName($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND skill_name != '$currentName'" : '');
        $sql = "SELECT skill_name FROM tbl_user_technical_skill 
                WHERE skill_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getSkillName($id) {
        $sql = "SELECT skill_name FROM tbl_user_technical_skill
                WHERE del_status = 'Live'
                AND user_skill_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->skill_name;
    }

    public function checkUserLanguage($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND language_name != '$currentName'" : '');
        $sql = "SELECT language_name FROM tbl_user_language 
                WHERE language_name = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserLanguage($id) {
        $sql = "SELECT language_name FROM tbl_user_language
                WHERE del_status = 'Live'
                AND user_language_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->language_name;
    }

    public function checkAdditionalDetails($name, $user_id, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND additional_title != '$currentName'" : '');
        $sql = "SELECT additional_title FROM tbl_additional_detail 
                WHERE additional_title = '$name' AND ref_user_id = '$user_id' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();


        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getAdditionalDetails($id) {
        $sql = "SELECT additional_title FROM tbl_additional_detail
                WHERE del_status = 'Live'
                AND additional_title_id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->additional_title;
    }

    public function insertResumeDescription($category_id) {
        $additional_description = $this->input->post('additional_description');
        $is_active_additional_description = $this->input->post('is_active_additional_description');
        if (isset($additional_description) && !empty($additional_description)) {
            foreach ($additional_description as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $insert_data['ref_user_id'] = $this->user_id;
                    $insert_data['ref_additional_title_id'] = $category_id;
                    $insert_data['additional_description'] = $value;
                    $insert_data['is_active'] = isset($is_active_additional_description[$key]) && !empty($is_active_additional_description[$key]) ? ($is_active_additional_description[$key] == 'on' ? 1 : 0) : '';
                    $this->db->insert('tbl_additional_detail_item', $insert_data);
                }
            }
        }
    }

    public function getResumeData($user_id) {
        $sql = "SELECT rc.* 
                FROM tbl_additional_detail rc 
                WHERE rc.ref_user_id = '$user_id' AND rc.del_status = 'Live'";
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]->additional_description = $this->getResumeDescription($value->additional_title_id);
            }
        }
        return $result;
    }

    public function getResumeDescription($category_id) {
        $sql = "SELECT * 
                FROM tbl_additional_detail_item 
                WHERE del_status = 'Live' AND ref_additional_title_id = '$category_id'";
        return $this->db->query($sql)->result();
    }

}
