-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Aug 29, 2020 at 12:37 PM
-- Server version: 5.7.28
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `business_profile`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

DROP TABLE IF EXISTS `tbl_about`;
CREATE TABLE IF NOT EXISTS `tbl_about` (
  `about_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `about_title` varchar(50) NOT NULL,
  `about_desc` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`about_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`about_id`, `ref_user_id`, `about_title`, `about_desc`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'About Me', 'About Description', 2, '::1', '2020-08-25 04:39:57', NULL, NULL, NULL, 'Live'),
(2, 3, 'About Me', 'About Description', 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_additional_detail`
--

DROP TABLE IF EXISTS `tbl_additional_detail`;
CREATE TABLE IF NOT EXISTS `tbl_additional_detail` (
  `additional_title_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `additional_title` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`additional_title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_additional_detail`
--

INSERT INTO `tbl_additional_detail` (`additional_title_id`, `ref_user_id`, `additional_title`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'Final Year Project', 1, 2, '::1', '2020-08-25 10:50:14', 2, '::1', '2020-08-28 05:30:51', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_additional_detail_item`
--

DROP TABLE IF EXISTS `tbl_additional_detail_item`;
CREATE TABLE IF NOT EXISTS `tbl_additional_detail_item` (
  `additional_detail_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `ref_additional_title_id` int(11) NOT NULL,
  `additional_description` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`additional_detail_description_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_additional_detail_item`
--

INSERT INTO `tbl_additional_detail_item` (`additional_detail_description_id`, `ref_user_id`, `ref_additional_title_id`, `additional_description`, `is_active`, `del_status`) VALUES
(12, 2, 1, 'E super Market', 1, 'Live'),
(13, 2, 1, '1 Year Project', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `contact_address` text NOT NULL,
  `contact_name` varchar(250) NOT NULL,
  `contact_email` varchar(250) NOT NULL,
  `ref_country_id` int(11) NOT NULL,
  `contact_phone` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `ref_user_id`, `contact_address`, `contact_name`, `contact_email`, `ref_country_id`, `contact_phone`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'Kudsad', 'Yash Agle', 'yashagle13@gmail.com', 99, '9662004451', 1, 2, '::1', '2020-08-25 07:35:26', 2, '::1', '2020-08-28 05:34:56', 'Live'),
(2, 2, 'Kim', 'Mohammed Patel', 'patelmohammed1998@gmail.com', 99, '7043411861', 1, 2, '::1', '2020-08-25 07:36:40', 2, '::1', '2020-08-28 05:35:21', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
CREATE TABLE IF NOT EXISTS `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=254 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`, `del_status`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93, 'Live'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355, 'Live'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213, 'Live'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684, 'Live'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376, 'Live'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244, 'Live'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264, 'Live'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0, 'Live'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268, 'Live'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54, 'Live'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374, 'Live'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297, 'Live'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61, 'Live'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43, 'Live'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994, 'Live'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242, 'Live'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973, 'Live'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880, 'Live'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246, 'Live'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375, 'Live'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32, 'Live'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501, 'Live'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229, 'Live'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441, 'Live'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975, 'Live'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591, 'Live'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387, 'Live'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267, 'Live'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0, 'Live'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55, 'Live'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246, 'Live'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673, 'Live'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359, 'Live'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226, 'Live'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257, 'Live'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855, 'Live'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237, 'Live'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1, 'Live'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238, 'Live'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345, 'Live'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236, 'Live'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235, 'Live'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56, 'Live'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86, 'Live'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61, 'Live'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672, 'Live'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57, 'Live'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269, 'Live'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242, 'Live'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 243, 'Live'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682, 'Live'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506, 'Live'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225, 'Live'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385, 'Live'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53, 'Live'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357, 'Live'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420, 'Live'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45, 'Live'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253, 'Live'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767, 'Live'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809, 'Live'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593, 'Live'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20, 'Live'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503, 'Live'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240, 'Live'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291, 'Live'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372, 'Live'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251, 'Live'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, 'Live'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298, 'Live'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679, 'Live'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358, 'Live'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33, 'Live'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594, 'Live'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689, 'Live'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0, 'Live'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241, 'Live'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220, 'Live'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995, 'Live'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49, 'Live'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233, 'Live'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350, 'Live'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30, 'Live'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299, 'Live'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473, 'Live'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590, 'Live'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671, 'Live'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502, 'Live'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224, 'Live'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245, 'Live'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592, 'Live'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509, 'Live'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0, 'Live'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39, 'Live'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504, 'Live'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852, 'Live'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36, 'Live'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354, 'Live'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91, 'Live'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62, 'Live'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98, 'Live'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964, 'Live'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353, 'Live'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972, 'Live'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39, 'Live'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876, 'Live'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81, 'Live'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962, 'Live'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7, 'Live'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254, 'Live'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686, 'Live'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850, 'Live'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82, 'Live'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965, 'Live'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996, 'Live'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856, 'Live'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371, 'Live'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961, 'Live'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266, 'Live'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231, 'Live'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218, 'Live'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423, 'Live'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370, 'Live'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352, 'Live'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853, 'Live'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389, 'Live'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261, 'Live'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265, 'Live'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60, 'Live'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960, 'Live'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223, 'Live'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356, 'Live'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692, 'Live'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596, 'Live'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222, 'Live'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230, 'Live'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269, 'Live'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52, 'Live'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691, 'Live'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373, 'Live'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377, 'Live'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976, 'Live'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664, 'Live'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212, 'Live'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258, 'Live'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95, 'Live'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264, 'Live'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674, 'Live'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977, 'Live'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31, 'Live'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599, 'Live'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687, 'Live'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64, 'Live'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505, 'Live'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227, 'Live'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234, 'Live'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683, 'Live'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672, 'Live'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670, 'Live'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47, 'Live'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968, 'Live'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92, 'Live'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680, 'Live'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970, 'Live'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507, 'Live'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675, 'Live'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595, 'Live'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51, 'Live'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63, 'Live'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0, 'Live'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48, 'Live'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351, 'Live'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787, 'Live'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974, 'Live'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262, 'Live'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40, 'Live'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70, 'Live'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250, 'Live'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290, 'Live'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869, 'Live'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758, 'Live'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508, 'Live'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, 'Live'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684, 'Live'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378, 'Live'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239, 'Live'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966, 'Live'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221, 'Live'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381, 'Live'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248, 'Live'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232, 'Live'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65, 'Live'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421, 'Live'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386, 'Live'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677, 'Live'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252, 'Live'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27, 'Live'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0, 'Live'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34, 'Live'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94, 'Live'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249, 'Live'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597, 'Live'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47, 'Live'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268, 'Live'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46, 'Live'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41, 'Live'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963, 'Live'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886, 'Live'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992, 'Live'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255, 'Live'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66, 'Live'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670, 'Live'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228, 'Live'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690, 'Live'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676, 'Live'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868, 'Live'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216, 'Live'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90, 'Live'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370, 'Live'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649, 'Live'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688, 'Live'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256, 'Live'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380, 'Live'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971, 'Live'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44, 'Live'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1, 'Live'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1, 'Live'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598, 'Live'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998, 'Live'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678, 'Live'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58, 'Live'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84, 'Live'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284, 'Live'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340, 'Live'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681, 'Live'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212, 'Live'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967, 'Live'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260, 'Live'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263, 'Live'),
(240, 'RS', 'SERBIA', 'Serbia', 'SRB', 688, 381, 'Live'),
(241, 'AP', 'ASIA PACIFIC REGION', 'Asia / Pacific Region', '0', 0, 0, 'Live'),
(242, 'ME', 'MONTENEGRO', 'Montenegro', 'MNE', 499, 382, 'Live'),
(243, 'AX', 'ALAND ISLANDS', 'Aland Islands', 'ALA', 248, 358, 'Live'),
(244, 'BQ', 'BONAIRE, SINT EUSTATIUS AND SABA', 'Bonaire, Sint Eustatius and Saba', 'BES', 535, 599, 'Live'),
(245, 'CW', 'CURACAO', 'Curacao', 'CUW', 531, 599, 'Live'),
(246, 'GG', 'GUERNSEY', 'Guernsey', 'GGY', 831, 44, 'Live'),
(247, 'IM', 'ISLE OF MAN', 'Isle of Man', 'IMN', 833, 44, 'Live'),
(248, 'JE', 'JERSEY', 'Jersey', 'JEY', 832, 44, 'Live'),
(249, 'XK', 'KOSOVO', 'Kosovo', '---', 0, 381, 'Live'),
(250, 'BL', 'SAINT BARTHELEMY', 'Saint Barthelemy', 'BLM', 652, 590, 'Live'),
(251, 'MF', 'SAINT MARTIN', 'Saint Martin', 'MAF', 663, 590, 'Live'),
(252, 'SX', 'SINT MAARTEN', 'Sint Maarten', 'SXM', 534, 1, 'Live'),
(253, 'SS', 'SOUTH SUDAN', 'South Sudan', 'SSD', 728, 211, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_experience_education`
--

DROP TABLE IF EXISTS `tbl_experience_education`;
CREATE TABLE IF NOT EXISTS `tbl_experience_education` (
  `exp_edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `period_from` varchar(250) NOT NULL,
  `period_to` varchar(250) DEFAULT NULL,
  `designation_course_name` varchar(250) NOT NULL,
  `company_university_name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `current_status` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_exp_or_edu` varchar(50) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`exp_edu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_experience_education`
--

INSERT INTO `tbl_experience_education` (`exp_edu_id`, `ref_user_id`, `period_from`, `period_to`, `designation_course_name`, `company_university_name`, `description`, `current_status`, `is_active`, `is_exp_or_edu`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, '2016', '2017', 'Final Year Project', 'Vision Infotech', 'Diploma Final Year project develop by my team. Project base on ecommerce website name is E-Supermarket', 0, 1, 'experience', 2, '::1', '2020-08-25 06:16:45', 2, '::1', '2020-08-28 05:45:28', 'Live'),
(2, 2, '2014', '2017', 'Engineering', 'SDCDE', 'Diploma Engineering', 0, 1, 'education', 2, '::1', '2020-08-25 06:12:47', 2, '::1', '2020-08-28 05:21:27', 'Live'),
(3, 2, '2018', '2020', 'Developer', 'Weborative', 'in Developer Post in Weborative it counsaltanct Company.', 1, 1, 'experience', 2, '::1', '2020-08-25 06:12:17', 2, '::1', '2020-08-28 05:21:22', 'Live'),
(4, 2, '2004', '2014', 'School', 'Tapovan Vidhyadham', 'Primary Education', 0, 1, 'education', 2, '::1', '2020-08-25 12:46:48', 2, '::1', '2020-08-28 05:21:17', 'Live'),
(5, 2, '2015', '2017', 'Testing', 'Testing', 'Testing Desc', 0, 0, 'education', 2, '::1', '2020-08-25 12:47:48', 2, '::1', '2020-08-25 12:48:06', 'Deleted'),
(6, 2, '2002', '2004', 'Nursery', 'Tapovan Vidhyadham', 'Nursery Study', 0, 1, 'education', 2, '::1', '2020-08-28 04:51:26', 2, '::1', '2020-08-28 05:21:10', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_footer`
--

DROP TABLE IF EXISTS `tbl_footer`;
CREATE TABLE IF NOT EXISTS `tbl_footer` (
  `footer_id` int(11) NOT NULL AUTO_INCREMENT,
  `footer_logo` text NOT NULL,
  `footer_logo_desc` text NOT NULL,
  `footer_facebook` text NOT NULL,
  `footer_twitter` text NOT NULL,
  `footer_vimeo` text NOT NULL,
  `footer_linkedin` text NOT NULL,
  `phone_number` text NOT NULL,
  `email_address` text NOT NULL,
  PRIMARY KEY (`footer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_footer`
--

INSERT INTO `tbl_footer` (`footer_id`, `footer_logo`, `footer_logo_desc`, `footer_facebook`, `footer_twitter`, `footer_vimeo`, `footer_linkedin`, `phone_number`, `email_address`) VALUES
(1, 'Weborative-1.png', 'Weborative is a trustworthy partner in building technically enhanced businesses worldwide. We take you to new heights of success with dedication and dexterity as an innate solution provider.', 'javascript:void(0)', 'javascript:void(0)', 'javascript:void(0)', 'javascript:void(0)', '+91 90333 35354', 'info@weborative.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

DROP TABLE IF EXISTS `tbl_gallery`;
CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `ref_gallery_category_id` int(11) NOT NULL,
  `gallery_name` varchar(250) NOT NULL,
  `gallery_image` text NOT NULL,
  `gallery_description` text,
  `company_name` varchar(250) DEFAULT NULL,
  `is_active_company_name` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `file_extension` varchar(250) NOT NULL,
  `InsUser` int(11) NOT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`gallery_id`, `ref_user_id`, `ref_gallery_category_id`, `gallery_name`, `gallery_image`, `gallery_description`, `company_name`, `is_active_company_name`, `is_active`, `file_extension`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 2, 'Coffee', 'assets/images/gallery/client_3.png', 'Desc', '', 0, 1, 'png', 2, '::1', NULL, 2, '::1', '2020-08-28 05:35:45', 'Live'),
(2, 2, 1, 'Name', 'assets/images/gallery/Chrysanthemum.jpg', '', '', 0, 1, 'jpg', 2, '::1', '2020-08-25 10:34:32', 2, '::1', '2020-08-28 05:35:49', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_category`
--

DROP TABLE IF EXISTS `tbl_gallery_category`;
CREATE TABLE IF NOT EXISTS `tbl_gallery_category` (
  `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `gallery_category_name` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`gallery_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery_category`
--

INSERT INTO `tbl_gallery_category` (`gallery_category_id`, `ref_user_id`, `gallery_category_name`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'Photo', 1, 2, '::1', '2020-08-25 04:39:57', NULL, NULL, NULL, 'Live'),
(2, 2, 'Video', 1, 2, '::1', '2020-08-25 04:39:57', 2, '::1', '2020-08-28 05:36:32', 'Live'),
(3, 2, 'Music', 1, 2, '::1', '2020-08-25 04:39:57', NULL, NULL, NULL, 'Live'),
(4, 2, 'Design', 1, 2, '::1', '2020-08-25 04:39:57', NULL, NULL, NULL, 'Live'),
(5, 2, 'General', 1, 2, '::1', '2020-08-25 04:39:58', 2, '::1', '2020-08-25 12:52:10', 'Live'),
(6, 3, 'Photo', 1, 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live'),
(7, 3, 'Video', 1, 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live'),
(8, 3, 'Music', 1, 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live'),
(9, 3, 'Design', 1, 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live'),
(10, 3, 'General', 1, 3, '::1', '2020-08-25 06:04:21', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_map`
--

DROP TABLE IF EXISTS `tbl_map`;
CREATE TABLE IF NOT EXISTS `tbl_map` (
  `map_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `map` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`map_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_map`
--

INSERT INTO `tbl_map` (`map_id`, `ref_user_id`, `map`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3719.781983766904!2d72.83588536493556!3d21.20081798590542!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04ef7dfac7651%3A0x3e9863c0f7b5d6d6!2sNear%20Meghani%20Tower%2C%20Surat%2C%20Gujarat%20395008!5e0!3m2!1sen!2sin!4v1598353347376!5m2!1sen!2sin', 1, 1, '::1', '2020-08-25 00:00:00', 2, '::1', '2020-08-28 05:35:33', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_plan`
--

DROP TABLE IF EXISTS `tbl_plan`;
CREATE TABLE IF NOT EXISTS `tbl_plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(250) NOT NULL,
  `plan_price` varchar(50) NOT NULL,
  `plan_sale_price` varchar(50) DEFAULT NULL,
  `is_sale` int(11) NOT NULL DEFAULT '0',
  `is_allow_profile_type` int(11) DEFAULT NULL,
  `is_allow_insert_client` int(11) NOT NULL DEFAULT '0',
  `product_limit` int(11) NOT NULL,
  `address_limit` int(11) NOT NULL,
  `service_limit` int(11) NOT NULL,
  `payment_info_limit` int(11) NOT NULL,
  `item_limit` int(11) NOT NULL,
  `client_limit` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_plan`
--

INSERT INTO `tbl_plan` (`plan_id`, `plan_name`, `plan_price`, `plan_sale_price`, `is_sale`, `is_allow_profile_type`, `is_allow_insert_client`, `product_limit`, `address_limit`, `service_limit`, `payment_info_limit`, `item_limit`, `client_limit`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Business Profile', '999', NULL, 0, 1, 0, 1, 2, 3, 2, 5, 0, 1, 1, '::1', '2020-08-17 06:01:35', 1, '::1', '2020-08-21 12:31:43', 'Live'),
(2, 'Business Profile Pro', '1499', NULL, 0, 1, 1, 10, 2, 2, 2, 3, 6, 1, 1, '::1', '2020-08-17 06:02:58', 1, '::1', '2020-08-29 05:37:16', 'Live'),
(3, 'Menu Plan ', '499', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '::1', '2020-08-17 06:03:41', NULL, NULL, NULL, 'Live'),
(4, 'Menu Pro', '999', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '::1', '2020-08-17 06:04:47', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_plan_desc`
--

DROP TABLE IF EXISTS `tbl_plan_desc`;
CREATE TABLE IF NOT EXISTS `tbl_plan_desc` (
  `plan_desc_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_plan_id` int(11) NOT NULL,
  `plan_description` text NOT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`plan_desc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_plan_desc`
--

INSERT INTO `tbl_plan_desc` (`plan_desc_id`, `ref_plan_id`, `plan_description`, `del_status`) VALUES
(19, 3, 'About', 'Live'),
(20, 3, 'Item Limit 150', 'Live'),
(21, 3, 'Contact', 'Live'),
(22, 3, 'Payment', 'Live'),
(23, 4, 'About', 'Live'),
(24, 4, 'Item Limit 300', 'Live'),
(25, 4, 'Product up to 100', 'Live'),
(26, 4, 'Gallery [Show Active Product Item ]', 'Live'),
(27, 4, 'Contact', 'Live'),
(28, 4, 'Payment', 'Live'),
(77, 1, 'About [Only info]', 'Live'),
(78, 1, 'Resume  or Service [Services up to 10]', 'Live'),
(79, 1, 'Product/Item/Portfolio [Limit 35]', 'Live'),
(80, 1, 'Gallery [Show Active Product Item ]', 'Live'),
(81, 1, 'Contact [Address limit 2]', 'Live'),
(82, 1, 'Payment Info limit 1', 'Live'),
(113, 2, 'About [With Client Profile Option]', 'Live'),
(114, 2, 'Resume or Service [Unlimited Services]', 'Live'),
(115, 2, 'Product/Item/Portfolio [Limit 100]', 'Live'),
(116, 2, 'Gallery [Show Active Product Item ]', 'Live'),
(117, 2, 'Contact [Address Unlimited]', 'Live'),
(118, 2, 'Payment Info Unlimited', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu`
--

DROP TABLE IF EXISTS `tbl_side_menu`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(250) NOT NULL,
  `menu_constant` varchar(250) NOT NULL,
  `menu_url` text NOT NULL,
  `ref_menu_id` int(11) DEFAULT NULL,
  `menu_icon` varchar(250) DEFAULT NULL,
  `menu_order_no` int(11) DEFAULT NULL,
  `menu_for` varchar(50) NOT NULL DEFAULT 'customer',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_side_menu`
--

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `menu_for`, `del_status`) VALUES
(1, 'Dashboard', 'DASHBOARD', 'admin/Dashboard', NULL, 'fa-info-circle', 1, 'admin', 'Live'),
(2, 'Master', 'MASTER', '#', NULL, 'fa-cog', 2, 'admin', 'Live'),
(3, 'About', 'ABOUT', '#', NULL, 'fa-user', 2, 'customer', 'Live'),
(4, 'About', 'ABOUT_ME', 'admin/customer/About', 3, NULL, 1, 'customer', 'Live'),
(5, 'Client', 'ABOUT_CLIENT', 'admin/customer/About/client', 3, NULL, 2, 'customer', 'Live'),
(6, 'User', 'USER', 'admin/Master/user', 2, NULL, 1, 'admin', 'Live'),
(7, 'Plan', 'PLAN', 'admin/Master/plan', 2, NULL, 2, 'admin', 'Live'),
(8, 'Service', 'SERVICE', 'admin/customer/Service', NULL, 'fa-share', 3, 'customer', 'Live'),
(9, 'Resume', 'RESUME', '#', NULL, 'fa-file-pdf\r\n', 3, 'customer', 'Live'),
(10, 'Setting', 'SETTING', 'admin/customer/Setting', NULL, 'fa-cog', 7, 'customer', 'Live'),
(11, 'Contact', 'CONTACT', '#', NULL, 'fa-user', 6, 'customer', 'Live'),
(12, 'Contact', 'CONTACT_PAGE', 'admin/customer/Contact', 11, NULL, 1, 'customer', 'Live'),
(13, 'Experience And Education', 'EXPERIENCE_EDUCATION', 'admin/customer/Resume', 9, NULL, 1, 'customer', 'Live'),
(14, 'Technical Skill', 'TECHNICAL_SKILL', 'admin/customer/Resume/technicalSkill', 9, NULL, 2, 'customer', 'Live'),
(15, 'Language', 'USER_LANGUAGE', 'admin/customer/Resume/userLanguage', 9, NULL, 3, 'customer', 'Live'),
(16, 'Additional Details', 'ADDITION_DETAILS', 'admin/customer/Resume/additionalDetails', 9, NULL, 4, 'customer', 'Live'),
(17, 'Portfolio/Products', 'PORTFOLIO_PRODUCT', '#', NULL, 'fa-images', 4, 'customer', 'Live'),
(18, 'Category', 'CATEGORY', 'admin/customer/Gallery', 17, NULL, 1, 'customer', 'Live'),
(19, 'Portfolio/Products', 'PORTFOLIO_PRODUCT_GALLERY', 'admin/customer/Gallery/portfolioGallery', 17, NULL, 2, 'customer', 'Live'),
(20, 'Payment Information', 'PAYMENT_INFORMATION', 'admin/customer/Payment', NULL, 'fa-money-bill-wave-alt', 5, 'customer', 'Live'),
(21, 'Map', 'CONTACT_MAP', 'admin/customer/Contact/map', 11, NULL, 2, 'customer', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu_rights`
--

DROP TABLE IF EXISTS `tbl_side_menu_rights`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `ref_menu_id` int(11) NOT NULL,
  `full_access` tinyint(4) NOT NULL,
  `view_right` tinyint(4) NOT NULL,
  `add_right` tinyint(4) NOT NULL,
  `edit_right` tinyint(4) NOT NULL,
  `delete_right` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_side_menu_rights`
--

INSERT INTO `tbl_side_menu_rights` (`id`, `ref_user_id`, `ref_menu_id`, `full_access`, `view_right`, `add_right`, `edit_right`, `delete_right`) VALUES
(1, 2, 1, 0, 1, 1, 1, 0),
(2, 2, 10, 0, 1, 0, 1, 0),
(3, 2, 4, 0, 1, 0, 1, 0),
(4, 2, 12, 1, 1, 1, 1, 1),
(5, 2, 18, 0, 1, 0, 1, 0),
(6, 2, 19, 1, 1, 1, 1, 1),
(7, 2, 20, 1, 1, 1, 1, 1),
(8, 2, 13, 1, 1, 1, 1, 1),
(9, 2, 14, 1, 1, 1, 1, 1),
(10, 2, 15, 1, 1, 1, 1, 1),
(11, 2, 16, 1, 1, 1, 1, 1),
(12, 2, 5, 1, 1, 1, 1, 1),
(13, 3, 1, 0, 1, 1, 1, 0),
(14, 3, 10, 0, 1, 0, 1, 0),
(15, 3, 4, 0, 1, 0, 1, 0),
(16, 3, 12, 1, 1, 1, 1, 1),
(17, 3, 18, 0, 1, 0, 1, 0),
(18, 3, 19, 1, 1, 1, 1, 1),
(19, 3, 20, 1, 1, 1, 1, 1),
(20, 3, 8, 0, 1, 0, 1, 0),
(21, 2, 21, 0, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_theme`
--

DROP TABLE IF EXISTS `tbl_theme`;
CREATE TABLE IF NOT EXISTS `tbl_theme` (
  `theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_color` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_client`
--

DROP TABLE IF EXISTS `tbl_user_client`;
CREATE TABLE IF NOT EXISTS `tbl_user_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `client_logo` text NOT NULL,
  `client_name` varchar(250) NOT NULL,
  `is_active_client` int(11) NOT NULL DEFAULT '0',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_client`
--

INSERT INTO `tbl_user_client` (`client_id`, `ref_user_id`, `client_logo`, `client_name`, `is_active_client`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'assets/images/client/client_1.png', 'Coffee', 0, 2, '::1', '2020-08-25 07:32:58', 2, '::1', '2020-08-25 10:44:11', 'Deleted'),
(2, 2, 'assets/images/client/client_2.png', 'Coffee 2', 1, 2, '::1', '2020-08-25 07:33:08', NULL, NULL, NULL, 'Deleted'),
(3, 2, 'assets/images/client/client_3.png', 'Coffee 3', 1, 2, '::1', '2020-08-25 07:33:17', NULL, NULL, NULL, 'Deleted'),
(4, 2, 'assets/images/client/MPS.png', 'MPS', 1, 2, '::1', '2020-08-28 04:46:34', 2, '::1', '2020-08-28 10:23:45', 'Live'),
(5, 2, 'assets/images/client/Advantis_India.png', 'Advantis India', 1, 2, '::1', '2020-08-28 04:46:58', 2, '::1', '2020-08-28 05:31:46', 'Live'),
(6, 2, 'assets/images/client/EASYMPS.png', 'EASY MPS', 1, 2, '::1', '2020-08-28 04:47:46', 2, '::1', '2020-08-28 05:31:49', 'Live'),
(7, 2, 'assets/images/client/MPS1.png', 'Client', 1, 2, '::1', '2020-08-29 05:28:25', 2, '::1', '2020-08-29 12:05:54', 'Live'),
(8, 2, 'assets/images/client/EASYMPS1.png', 'Client 2', 1, 2, '::1', '2020-08-29 05:37:44', 2, '::1', '2020-08-29 12:04:39', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_contact_us`
--

DROP TABLE IF EXISTS `tbl_user_contact_us`;
CREATE TABLE IF NOT EXISTS `tbl_user_contact_us` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `contact_us_name` varchar(250) NOT NULL,
  `contact_us_email` text NOT NULL,
  `contact_us_message` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL,
  PRIMARY KEY (`contact_us_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_contact_us`
--

INSERT INTO `tbl_user_contact_us` (`contact_us_id`, `ref_user_id`, `contact_us_name`, `contact_us_email`, `contact_us_message`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'Mohammed Patel', 'patelmohammed1998@gmail.com', 'Contact you', 2, '::1', '2020-08-28 06:03:54', NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_info`
--

DROP TABLE IF EXISTS `tbl_user_info`;
CREATE TABLE IF NOT EXISTS `tbl_user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(250) NOT NULL,
  `user_number` varchar(50) NOT NULL,
  `user_email` text NOT NULL,
  `user_password` text NOT NULL,
  `ref_plan_id` int(11) NOT NULL,
  `ref_payment_id` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  `plan_end_date` datetime NOT NULL,
  `ref_profile_type` int(11) DEFAULT NULL,
  `profile_photo` text,
  `raw` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `user_role` varchar(50) DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '2' COMMENT '1 = Admin, 2 = customer',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_info`
--

INSERT INTO `tbl_user_info` (`user_id`, `user_name`, `user_number`, `user_email`, `user_password`, `ref_plan_id`, `ref_payment_id`, `payment_date`, `plan_end_date`, `ref_profile_type`, `profile_photo`, `raw`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `status`, `user_role`, `user_type`, `del_status`) VALUES
(1, 'Admin', '1234567890', 'admin@weborative.com', 'e1b4755403710e0deb7aa5d45e43996d', 1, 1, '2020-08-14 00:00:00', '2020-08-14 00:00:00', 1, NULL, 'admin@123#', 1, '::1', '2020-08-14 00:00:00', NULL, NULL, NULL, 1, 'Admin', 1, 'Live'),
(2, 'Yash Agle', '9662004451', 'yashagle13@gmail.com', '69b87b9d2db71117bdc96aa0da3f5a53', 2, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 'assets/admin/img/demo/avatars/_avatar-admin1.png', 'JJQuq0y0', NULL, NULL, NULL, 2, '::1', '2020-08-29 08:30:04', 1, NULL, 2, 'Live'),
(3, 'mohammed', '7043411861', 'patelmohammed1998@gmail.com', 'ec317085fee11baf69d9187340e93782', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, 'Rsi72Oac', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 2, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_language`
--

DROP TABLE IF EXISTS `tbl_user_language`;
CREATE TABLE IF NOT EXISTS `tbl_user_language` (
  `user_language_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `language_name` varchar(250) NOT NULL,
  `language_per` int(11) DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`user_language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_language`
--

INSERT INTO `tbl_user_language` (`user_language_id`, `ref_user_id`, `language_name`, `language_per`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'English', 50, 1, 2, '::1', '2020-08-25 05:58:59', 2, '::1', '2020-08-28 05:30:25', 'Live'),
(2, 2, 'Gujarati', 100, 1, 2, '::1', '2020-08-25 05:58:59', 2, '::1', '2020-08-28 05:18:25', 'Live'),
(3, 2, 'Hindi', 80, 1, 2, '::1', '2020-08-25 05:58:59', 2, '::1', '2020-08-28 05:18:29', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_payment_info`
--

DROP TABLE IF EXISTS `tbl_user_payment_info`;
CREATE TABLE IF NOT EXISTS `tbl_user_payment_info` (
  `payment_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `bank_name` varchar(250) NOT NULL,
  `acc_holder_name` varchar(250) NOT NULL,
  `branch_name` varchar(250) DEFAULT NULL,
  `account_number` varchar(250) NOT NULL,
  `ifsc_code` varchar(250) DEFAULT NULL,
  `qr_code` text,
  `copy_detail` text,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`payment_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_payment_info`
--

INSERT INTO `tbl_user_payment_info` (`payment_info_id`, `ref_user_id`, `bank_name`, `acc_holder_name`, `branch_name`, `account_number`, `ifsc_code`, `qr_code`, `copy_detail`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'BOB', 'Weborative', 'KIMBOB', '1234567980', 'BOBKIMXXX', 'assets/images/payment/qr_code.png', '1234567980', 1, 2, '::1', '2020-08-27 11:05:49', 2, '::1', '2020-08-27 11:35:04', 'Live'),
(2, 2, 'BOB', 'Weborative', 'testing branch', '789456', 'KKBK0002852', 'assets/images/payment/payment1.jpeg', '123789', 1, 2, '::1', '2020-08-27 11:42:58', 2, '::1', '2020-08-27 11:51:08', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profile`
--

DROP TABLE IF EXISTS `tbl_user_profile`;
CREATE TABLE IF NOT EXISTS `tbl_user_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `cover_photo` text NOT NULL,
  `profile_photo` text,
  `is_active_profile_photo` int(11) NOT NULL DEFAULT '0',
  `designation` varchar(250) DEFAULT NULL,
  `is_active_designation` int(11) NOT NULL DEFAULT '0',
  `instagram` text,
  `is_active_instagram` int(11) NOT NULL DEFAULT '0',
  `twitter` text,
  `is_active_twitter` int(11) NOT NULL DEFAULT '0',
  `facebook` text,
  `is_active_facebook` int(11) NOT NULL DEFAULT '0',
  `linkedin` text,
  `is_active_linkedin` int(11) NOT NULL DEFAULT '0',
  `whatsapp` text,
  `is_active_whatsapp` int(11) NOT NULL DEFAULT '0',
  `website` text,
  `is_active_website` int(11) NOT NULL DEFAULT '0',
  `custom_link_1` text,
  `is_active_custom_link_1` int(11) NOT NULL DEFAULT '0',
  `custom_link_2` text,
  `is_active_custom_link_2` int(11) NOT NULL DEFAULT '0',
  `custom_link_3` text,
  `is_active_custom_link_3` int(11) NOT NULL DEFAULT '0',
  `custom_link_4` text,
  `is_active_custom_link_4` int(11) NOT NULL DEFAULT '0',
  `custom_link_5` text,
  `is_active_custom_link_5` int(11) NOT NULL DEFAULT '0',
  `document` text,
  `theme_color` varchar(20) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_profile`
--

INSERT INTO `tbl_user_profile` (`profile_id`, `ref_user_id`, `cover_photo`, `profile_photo`, `is_active_profile_photo`, `designation`, `is_active_designation`, `instagram`, `is_active_instagram`, `twitter`, `is_active_twitter`, `facebook`, `is_active_facebook`, `linkedin`, `is_active_linkedin`, `whatsapp`, `is_active_whatsapp`, `website`, `is_active_website`, `custom_link_1`, `is_active_custom_link_1`, `custom_link_2`, `is_active_custom_link_2`, `custom_link_3`, `is_active_custom_link_3`, `custom_link_4`, `is_active_custom_link_4`, `custom_link_5`, `is_active_custom_link_5`, `document`, `theme_color`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'assets/images/bg.jpg', 'assets/images/profile.png', 0, 'Developer', 1, 'https://www.instagram.com/mr.yash_4913/', 1, 'https://twitter.com/yashagle', 1, 'https://www.facebook.com/yashagle13', 1, '', 0, 'https://wa.link/qr3ttb', 1, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, NULL, 'purple', 2, '::1', '2020-08-25 04:39:57', 2, '::1', '2020-08-29 12:23:34', 'Live'),
(2, 3, 'assets/images/bg.jpg', 'assets/images/profile.png', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, NULL, 'orange', 3, '::1', '2020-08-25 06:04:21', 3, '::1', '2020-08-29 12:09:50', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_service`
--

DROP TABLE IF EXISTS `tbl_user_service`;
CREATE TABLE IF NOT EXISTS `tbl_user_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `service_title` varchar(250) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_service`
--

INSERT INTO `tbl_user_service` (`service_id`, `ref_user_id`, `service_title`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 3, 'My Service', 3, '::1', '2020-08-25 06:04:21', 3, '::1', '2020-08-25 07:30:48', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_service_item`
--

DROP TABLE IF EXISTS `tbl_user_service_item`;
CREATE TABLE IF NOT EXISTS `tbl_user_service_item` (
  `service_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_service_id` int(11) NOT NULL,
  `service_item_title` varchar(250) NOT NULL,
  `service_item_desc` text NOT NULL,
  `is_active_service_item` int(11) NOT NULL DEFAULT '0',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_service_item`
--

INSERT INTO `tbl_user_service_item` (`service_item_id`, `ref_service_id`, `service_item_title`, `service_item_desc`, `is_active_service_item`, `del_status`) VALUES
(1, 1, 'Title 1', 'Description 1', 1, 'Live'),
(2, 1, 'Title 2', 'Description 2', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_technical_skill`
--

DROP TABLE IF EXISTS `tbl_user_technical_skill`;
CREATE TABLE IF NOT EXISTS `tbl_user_technical_skill` (
  `user_skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `skill_name` varchar(250) NOT NULL,
  `skill_per` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`user_skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_technical_skill`
--

INSERT INTO `tbl_user_technical_skill` (`user_skill_id`, `ref_user_id`, `skill_name`, `skill_per`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 2, 'Web Design', 80, 1, 2, '::1', '2020-08-25 10:46:43', 2, '::1', '2020-08-28 05:30:09', 'Live'),
(2, 2, 'Web Development', 99, 1, 2, '::1', '2020-08-28 04:52:21', 2, '::1', '2020-08-28 05:18:47', 'Live');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
